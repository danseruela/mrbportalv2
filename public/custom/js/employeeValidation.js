$(document).ready(function() {
    var table = $('#relationsTbl1').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
    var rows = table.rows({ 'search': 'applied' }).nodes();

    //Initialize Select2 Elements
    $('.select2').select2();
    $(".select2").on("select2:open", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Select an option...");
    });

    $(".select2").on("select2:close", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });
    
    // format date
    // Getter
    var dateFormat = $( "#birthdate" ).datepicker( "option", "dateFormat" );
   
    // Setter
    $( "#birthdate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );

    //$('#birthdate').datepicker({ dateFormat: "yyyy-mm-dd" }).val();
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    });
    
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red'
    });
    
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    });

    // Disable forms when form class equal to 'disabled'
    $('form.disabled input, form.disabled radio, form.disabled select, form.disabled textarea').each(function() {
        $(this).attr('disabled', 'disabled');
    });
    $('form.disabled .iradio_flat-green').addClass('disabled');
    $('form.disabled #cancelEmployeeProfile, form.disabled #addContact, form.disabled #removeRow, form.disabled #terminateEmployee, form.disabled #addRelationBtn, form.disabled .add-salary-modal, form.disabled .edit-sal-comp-modal, form.disabled .delete-sal-comp-modal').css({
      'display': 'none',
    });
    $('.edit-modal, .delete-modal', rows).addClass('hide-item');
    

    // Cascading Select options for Organization and Location of employees
    var select2Options = { width: 'resolve' };
    var apiUrl_1 = '/employee/locations/:parentId:';
    var apiUrl_2 = '/employee/jobs/:parentId:';
    
    $('select').select2(select2Options);                 
    var cascadLoading_1 = new Select2Cascade($('#org_code'), $('#location_code'), apiUrl_1, select2Options);
    var cascadLoading_2 = new Select2Cascade($('#job_cat'), $('#job_title'), apiUrl_2, select2Options);

    cascadLoading_1.then( function(parent, child, items) {
        // Dump response data
        console.log(items);
    });

    cascadLoading_2.then( function(parent, child, items) {
        // Dump response data
        console.log(items);
    });
});

/*******************
 * Employee fields 
 *******************/
$(document).on('click', '#editEmployeeProfile', function() {
    var table = $('#relationsTbl1').DataTable();
    var rows = table.rows({ 'search': 'applied' }).nodes();
    
    // Replace edit button with save button.
    $(this).replaceWith('<button type="submit" id="updateEmployeeProfile" class="btn btn-success">' +
                            '<i class="glyphicon glyphicon-floppy-disk"></i> Save' +
                        '</button>');

    $('input, radio, select, textarea').each(function() {
        $(this).removeAttr('disabled');
    });
    $('div#personal .iradio_flat-green').removeClass('disabled');
    $('form.disabled #cancelEmployeeProfile, form.disabled #addContact, form.disabled #removeRow, form.disabled #terminateEmployee, form.disabled #addRelationBtn, form.disabled .add-salary-modal, form.disabled .edit-sal-comp-modal, form.disabled .delete-sal-comp-modal').css({
      'display': 'initial',
    });
    $('.edit-modal, .delete-modal', rows).removeClass('hide-item');
});


/*******************
 * Contact Numbers 
 *******************/

// Delete a saved contact
$(document).on('click', '#removeRow', function() {
    $('.modal-title').text('Delete Contact Information');
    $('#delete_info').text($(this).data('content'));
    $('.delete').removeClass('delete-relation');
    $('.delete').removeClass('delete-sal-component');
    $('.delete').addClass('delete-contact');
    $('#deleteModal').modal('show');
    id = $(this).data('id');
});
        
$('.modal-footer').on('click', '.delete-contact', function() {
    $.ajax({
      type: 'DELETE',
      url: "/employee/removeContact/" + id,
      data: {
        '_token': $('input[name=_token]').val(),
      },
      success: function(data) {
        toastr.success('Successfully deleted Contact!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        $('.item'+ data['id']).remove();
      },
      error: function(data) {
        console.log('Error: ' + data.status);
      }
    });
});

/*********************
 * Family Relations
 *********************/
var isRelated;
var isEmergencyContact;

// is emergency contact
$('#is_emergency_contact').on('ifChecked', function(){
  $('#emergencyContactDetails').removeClass('hide-item');
  isEmergencyContact = 1;
});
$('#is_emergency_contact').on('ifUnchecked', function(){
  $('#emergencyContactDetails').addClass('hide-item');
  isEmergencyContact = 0;
  $('#emergency_address').val('');
  $('#emergency_contact').val('');
});
// is not employee related
$('#is_not_employee').on('ifChecked', function(){
  $('#empRelativeDetails').addClass('hide-item');
  $('#nonEmpRelativeDetails').removeClass('hide-item');
  $('#rel_firstname').val('');
  $('#rel_middlename').val('');
  $('#rel_lastname').val('');
  $('#emp_related_id').val('');
  isRelated = 0;
});
$('#is_not_employee').on('ifUnchecked', function(){
  $('#empRelativeDetails').removeClass('hide-item');
  $('#nonEmpRelativeDetails').addClass('hide-item');
  isRelated = 1;
});

// add relations
$(document).on('click', '.add-modal', function() {
    $('.modal-title').text('Add Family Relations');
    $('#rel_firstname').val("");
    $('#rel_middlename').val("");
    $('#rel_lastname').val("");
    $('#is_not_employee').iCheck('check');      // Default
    // Default
    if( $('#is_emergency_contact').iCheck('uncheck') ) {
      $('#emergencyContactDetails').addClass('hide-item');
      isEmergencyContact = 0;
      $('#emergency_address').val('');
      $('#emergency_contact').val('');
    } else {
      $('#is_emergency_contact').iCheck('check');
      $('#emergencyContactDetails').removeClass('hide-item');
      isEmergencyContact = 1;
    }
    $('#emergency_address').val("");
    $('#emergency_contact').val("");
    $('#submit-relation').removeClass("edit-relation");
    $('#submit-relation').addClass("add-relation");
    $('.add-relation').text("");
    $('.add-relation').append('<span class="glyphicon glyphicon-floppy-disk"></span> Add');
    $('#addRelation').modal('show');
});

$('.modal-footer').on('click', '.add-relation', function() {
    $.ajax({
      type: 'POST',
      url: '/employee/relation',
      data: {
        '_token': $('input[name=_token]').val(),
        'rel_emp_id': $('#rel_emp_id').val(),
        'emp_related_id': $('#emp_related_id').val(),
        'rel_firstname': $('#rel_firstname').val(),
        'rel_middlename': $('#rel_middlename').val(),
        'rel_lastname': $('#rel_lastname').val(),
        'rel_type': $('#rel_type').val(),
        'rel_consanguinity': $('#rel_consanguinity').val(),
        'is_related': isRelated,
        'is_emergency_contact' : isEmergencyContact,
        'emergency_address': $('#emergency_address').val(),
        'emergency_contact': $('#emergency_contact').val(),
      },
      success: function(data) {
        if((data.errors)) {
            //$.each(data.errors, function(i, val) {
              toastr.error(data.errors, 'Error Alert', {positionClass: 'toast-bottom-right', timeOut: 9000});
            //});
        } else {
            toastr.success('Successfully added Family Relation!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
            var indicator_relative = '';
            var indicator_contact = '';
            var relativeName = '';
            
            if (data.relation.is_related > 0) {
              indicator_relative = '<i class="fa fa-star text-success"></i>';
              relativeName = data.related.getEmpRelative;
            } else {
              var middlename = (data.relation.middlename == "" || data.relation.middlename == null) ? "" : data.relation.middlename.substr(0, 1);
              relativeName = data.relation.lastname + ', ' + data.relation.firstname + ' ' + middlename;
            }

            if (data.relation.is_emergency_contact==1) {
              indicator_contact = '<i class="fa fa-address-card text-success"></i>';

              $('#incaseemergency .box-body').append(
                '<div class="info-' + data.relation.id + '">' +
                  '<p id="e_name">' +
                    '<i class="fa fa-address-card margin-r-5"></i> ' +
                    '<strong>' + relativeName + '</strong>' +
                  '</p>' +  
                  '<p id="e_address" class="text-muted">' + data.relation.address + '</p>' +
                  '<p id="e_contact" class="text-muted">' + data.relation.contact_no + '</p>' +
                  '<hr>' +
                '</div>');
            } else {
              $('#incaseemergency .box-body').append(
                '<div class="info-' + data.relation.id + '"></div>');
            }

            $('#relationsTbl1').append(                   
              '<tr class="rel-item' + data.relation.id +'">' +
                '<td>' + relativeName + '</td>' +
                '<td class="text-center">' + indicator_relative + " " + indicator_contact + '</td>' +
                '<td>' + data.related.getType + '</td>' +
                '<td>' + data.related.getConsanguinity + '</td>' +
                '<td>' +
                  '<a href="#" class="delete-modal btn btn-danger" data-id="' + data.relation.id + '" data-fullname="' + relativeName + '"><i class="fa fa-trash"></i></a> ' +
                  '<a href="#" class="edit-modal btn btn-warning" data-id="' + data.relation.id + '" data-emp-related-id="' + data.relation.emp_related_id + '" data-is-related="' + data.relation.is_related + '" data-firstname="' + data.relation.firstname + '" data-middlename="' + data.relation.middlename + '" data-lastname="' + data.relation.lastname + '" data-reltype="' + data.relation.type + '" data-relcon="' + data.relation.consanguinity + '" data-is-emergency-contact="' + data.relation.is_emergency_contact +'" data-e-address="' + data.relation.address + '" data-e-contact="' + data.relation.contact_no + '"><i class="fa fa-edit"></i></a>' +
                '</td>' +
              '</tr>');        
        }
      },
      error: function(data) {
        console.log('Error: ' + data.status);
        toastr.error(data.status, 'Server Error', { positionClass: 'toast-bottom-right', timeOut: 5000 });
      },
    });
});


// edit relations
$(document).on('click', '.edit-modal', function() {
    $('.modal-title').text('Edit Family Relations');
    $('#rel_id').remove();
    $('#rel_emp_id').after('<input id="rel_id" name="rel_id" type="hidden">');
    $('#rel_id').val($(this).data('id'));
    // Is not related to employee
    if ($(this).data('is-related') == 1) {
      $('#is_not_employee').iCheck('uncheck');
      $("#emp_related_id").val($(this).data('emp-related-id')).trigger('change');
      isRelated = 1;
    } else {
      $('#is_not_employee').iCheck('check');
      $('#rel_firstname').val($(this).data('firstname'));
      $('#rel_middlename').val($(this).data('middlename'));
      $('#rel_lastname').val($(this).data('lastname'));
      isRelated = 0;
    }
    // Is an emergency contact
    if ($(this).data('is-emergency-contact') == 1) {
      $('#is_emergency_contact').iCheck('check');
      $('#emergency_address').val($(this).data('e-address'));
      $('#emergency_contact').val($(this).data('e-contact'));
    } else {
      $('#is_emergency_contact').iCheck('uncheck');
    }

    $("#rel_type").val($(this).data('reltype')).trigger('change');
    $("#rel_consanguinity").val($(this).data('relcon')).trigger('change');

    $('#submit-relation').removeClass("add-relation");
    $('#submit-relation').addClass("edit-relation");
    $('.edit-relation').text("");
    $('.edit-relation').append('<span class="glyphicon glyphicon-floppy-disk"></span> Update');
    $('#addRelation').modal('show');
    id = $('#rel_id').val();
});

$('.modal-footer').on('click', '.edit-relation', function() {
    $.ajax({
      type: 'PUT',
      url: '/employee/relation/' + id,
      data: {
        '_token': $('input[name=_token]').val(),
        'rel_emp_id': $('#rel_emp_id').val(),
        'emp_related_id': $('#emp_related_id').val(),
        'rel_firstname': $('#rel_firstname').val(),
        'rel_middlename': $('#rel_middlename').val(),
        'rel_lastname': $('#rel_lastname').val(),
        'rel_type': $('#rel_type').val(),
        'rel_consanguinity': $('#rel_consanguinity').val(),
        'is_related': isRelated,
        'is_emergency_contact' : isEmergencyContact,
        'emergency_address': $('#emergency_address').val(),
        'emergency_contact': $('#emergency_contact').val(),
      },
      success: function(data) {
        //if ((data.errors)) {
          //$.each(data.errors, function(i, val) {
              //toastr.error(data.errors, 'Error Alert', {positionClass: 'toast-bottom-right', timeOut: 9000});
            //});
        //} else {
            toastr.success('Successfully updated Family Relation!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
            var indicator_relative = '';
            var indicator_contact = '';
            var relativeName = '';
            
            if (data.relation.is_related > 0) {
              indicator_relative = '<i class="fa fa-star text-success"></i>';
              relativeName = data.related.getEmpRelative;
            } else {
              relativeName = data.relation.lastname + ', ' + data.relation.firstname + ' ' + data.relation.middlename.substr(0, 1);
            }

            if (data.relation.is_emergency_contact==1) {
              indicator_contact = '<i class="fa fa-address-card text-success"></i>';
              $('.info-' + data.relation.id).replaceWith(
                '<div class="info-' + data.relation.id + '">' +
                  '<p id="e_name">' +
                    '<i class="fa fa-address-card margin-r-5"></i> ' +
                    '<strong>' + relativeName + '</strong>' +
                  '</p>' +  
                  '<p id="e_address" class="text-muted">' + data.relation.address + '</p>' +
                  '<p id="e_contact" class="text-muted">' + data.relation.contact_no + '</p>' +
                  '<hr>' +
                '</div>');
            }
            if (data.relation.is_emergency_contact==0) {
              $('.info-' + data.relation.id).replaceWith('<div class="info-' + data.relation.id + '"></div>');
            }
            $('.rel-item' + data.relation.id).replaceWith(
              '<tr class="rel-item' + data.relation.id +'">' +
                '<td>' + relativeName + '</td>' +
                '<td class="text-center">' + indicator_relative + " " + indicator_contact + '</td>' +
                '<td>' + data.related.getType + '</td>' +
                '<td>' + data.related.getConsanguinity + '</td>' +
                '<td>' +
                  '<a href="#" class="delete-modal btn btn-danger" data-id="' + data.relation.id + '" data-fullname="' + relativeName + '"><i class="fa fa-trash"></i></a> ' +
                  '<a href="#" class="edit-modal btn btn-warning" data-id="' + data.relation.id + '" data-emp-related-id="' + data.relation.emp_related_id + '" data-is-related="' + data.relation.is_related + '" data-firstname="' + data.relation.firstname + '" data-middlename="' + data.relation.middlename + '" data-lastname="' + data.relation.lastname + '" data-reltype="' + data.relation.type + '" data-relcon="' + data.relation.consanguinity + '" data-is-emergency-contact="' + data.relation.is_emergency_contact +'" data-e-address="' + data.relation.address + '" data-e-contact="' + data.relation.contact_no + '"><i class="fa fa-edit"></i></a>' +
                '</td>' +
              '</tr>');
        //}
      },
      error: function(data) {
        console.log('Error: ' + data.status);
        toastr.error(data.status, 'Server Error', { positionClass: 'toast-bottom-right', timeOut: 5000 });
      },
    });
});

// delete relation
$(document).on('click', '.delete-modal', function() {
    $('.modal-title').text('Delete Family Relation');
    $('#delete_question').text('Are you sure you want to delete this family relation?');
    $('#delete_info').text($(this).data('fullname'));
    $('.delete').removeClass('delete-contact');
    $('.delete').removeClass('delete-sal-component');
    $('.delete').addClass('delete-relation');
    $('#deleteModal').modal('show');
    id = $(this).data('id');
});

$('.modal-footer').on('click', '.delete-relation', function() {
    $.ajax({
      type: 'DELETE',
      url: "/employee/relation/" + id,
      data: {
        '_token': $('input[name=_token]').val(),
      },
      success: function(data) {
        toastr.success('Successfully deleted Family Relation!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        $('.rel-item'+ data['id']).remove();
        $('.info-'+data['id']).remove();

      },
      error: function(data) {
        console.log('Error: ' + data.status);
        toastr.error(data.status, 'Server Error', { positionClass: 'toast-bottom-right', timeOut: 5000 });
      }
    });
});

// Salary
$(document).on('change', '#pay_grd_code', function() {
  $.ajax({
    type: 'GET',
    url: '/job/paygrade/' + $(this).val(),
    data: {
      '_token': $('input[name=_token]').val(),
    },
    success: function(data) {
      $('.salary-range').text('Min: ' + $.number(data.paygrade.min_salary, 2, '.', ', ') + ' | Max: ' + $.number(data.paygrade.max_salary, 2, '.', ', '));
      $('.salary-range').css({'display':'block'});
    },
    error: function(data) {
      console.log('Error: ' + data.status)
    }
  });
});


// Add Salary Component
$(document).on('click', '.add-salary-modal', function() {
    $('#basicSalary .modal-title').text('Add Salary Component');
    // clear input fields for new setup.
    $('#salary_component').val("");
    $('#basic_salary').val("");
    $('#comments').val("");
    $("#pay_grd_code").val(1);
    $("#sal_component_type").val(1);
    $("#pay_freq_code").val(1);
    // add button labels
    $('#submit-basic-salary').removeClass("edit-basic-salary");
    $('#submit-basic-salary').addClass("add-basic-salary");
    $('.add-basic-salary').text("");
    $('.add-basic-salary').append('<span class="glyphicon glyphicon-floppy-disk"></span> Add');
    $('#basicSalary').modal('show');
});

$('.modal-footer').on('click', '.add-basic-salary', function() {
    $.ajax({
      type: 'POST',
      url: '/employee/salary',
      data: {
        '_token': $('input[name=_token]').val(),
        'emp_code': $('#emp_code').val(),
        'pay_grd_code': $('#pay_grd_code').val(),
        'sal_component_type': $('#sal_component_type').val(),
        'salary_component': $('#salary_component').val(),
        'pay_freq_code': $('#pay_freq_code').val(),
        'basic_salary': $('#basic_salary').val(),
        'comments': $('#comments').val()
      },
      success: function(data) {
        if((data.errors)) {
            $.each(data.errors, function(i, val) {
              toastr.error(val, 'Error Alert', {positionClass: 'toast-bottom-right', timeOut: 9000});
            });
        } else {
            toastr.success('Successfully added Salary Component!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
            
            $('#salaryTbl2').append(                   
              '<tr class="salary-item' + data.salary.id +'">' +
                '<td>' + data.salary.salary_component + '</td>' +
                '<td><span class="badge bg-green">' + data.srTbl.getSalComponentType + '</span></td>' +
                '<td>' + data.srTbl.getPayFrequency + '</td>' +
                '<td>' + $.number( data.salary.basic_salary, 2, '.', ', ' ) + '</td>' +
                '<td>' +
                  '<a href="#" class="delete-sal-comp-modal btn btn-danger" data-toggle="tooltip" title="Delete" data-id="' + data.salary.id + '" data-salary-component="' + data.salary.salary_component + '"><i class="fa fa-trash"></i></a> ' +
                  '<a href="#" class="edit-sal-comp-modal btn btn-warning" data-toggle="tooltip" title="Edit" data-id="' + data.salary.id + '" data-salary-component="' + data.salary.salary_component + '" data-pay-grade="' + data.salary.pay_grd_code + '" data-sal-component-type="' + data.salary.sal_component_type + '" data-pay-freq="' + data.salary.pay_freq_code + '" data-basic-salary="' + data.salary.basic_salary + '" data-comments="' + data.salary.comments + '"><i class="fa fa-edit"></i></a>' +
                '</td>' +
              '</tr>');        
        }
      },
      error: function(data) {
        console.log('Error: ' + data.status);
      },
    });
});

// Edit Salary Component
$(document).on('click', '.edit-sal-comp-modal', function() {
    $('#basicSalary .modal-title').text('Edit Salary Component');
    // assign data values to fields.
    $('#salary_id').remove();
    $('#emp_code').after('<input id="salary_id" name="salary_id" type="hidden">');
    $('#salary_id').val($(this).data('id'));
    $("#pay_grd_code").val($(this).data('pay-grade')).trigger('change');
    $('#salary_component').val($(this).data('salary-component'));
    $("#sal_component_type").val($(this).data('sal-component-type')).trigger('change');
    $("#pay_freq_code").val($(this).data('pay-freq')).trigger('change');
    $('#basic_salary').val($(this).data('basic-salary'));
    $('#comments').val($(this).data('comments'));
    // add button labels
    $('#submit-basic-salary').removeClass("add-basic-salary");
    $('#submit-basic-salary').addClass("edit-basic-salary");
    $('.edit-basic-salary').text("");
    $('.edit-basic-salary').append('<span class="glyphicon glyphicon-floppy-disk"></span> Save');
    $('#basicSalary').modal('show');
    id = $('#salary_id').val();
});

$('.modal-footer').on('click', '.edit-basic-salary', function() {
    $.ajax({
      type: 'PUT',
      url: '/employee/salary/' + id,
      data: {
        '_token': $('input[name=_token]').val(),
        'emp_code': $('#emp_code').val(),
        'pay_grd_code': $('#pay_grd_code').val(),
        'sal_component_type': $('#sal_component_type').val(),
        'salary_component': $('#salary_component').val(),
        'pay_freq_code': $('#pay_freq_code').val(),
        'basic_salary': $('#basic_salary').val(),
        'comments': $('#comments').val()
      },
      success: function(data) {
        if((data.errors)) {
            $.each(data.errors, function(i, val) {
              toastr.error(val, 'Error Alert', {positionClass: 'toast-bottom-right', timeOut: 9000});
            });
        } else {
            toastr.success('Successfully updated Salary Component!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
            
            $('.salary-item' + data.salary.id).replaceWith(                   
              '<tr class="salary-item' + data.salary.id +'">' +
                '<td>' + data.salary.salary_component + '</td>' +
                '<td><span class="badge bg-green">' + data.srTbl.getSalComponentType + '</span></td>' +
                '<td>' + data.srTbl.getPayFrequency + '</td>' +
                '<td>' + $.number( data.salary.basic_salary, 2, '.', ', ' ) + '</td>' +
                '<td>' +
                  '<a href="#" class="delete-sal-comp-modal btn btn-danger" data-toggle="tooltip" title="Delete" data-id="' + data.salary.id + '" data-salary-component="' + data.salary.salary_component + '"><i class="fa fa-trash"></i></a> ' +
                  '<a href="#" class="edit-sal-comp-modal btn btn-warning" data-toggle="tooltip" title="Edit" data-id="' + data.salary.id + '" data-salary-component="' + data.salary.salary_component + '" data-pay-grade="' + data.salary.pay_grd_code + '" data-sal-component-type="' + data.salary.sal_component_type + '" data-pay-freq="' + data.salary.pay_freq_code + '" data-basic-salary="' + data.salary.basic_salary + '" data-comments="' + data.salary.comments + '"><i class="fa fa-edit"></i></a>' +
                '</td>' +
              '</tr>');        
        }
      },
      error: function(data) {
        console.log('Error: ' + data.status);
      },
    });
});


// Delete Salary Component
$(document).on('click', '.delete-sal-comp-modal', function() {
    $('.modal-title').text('Delete Salary Component');
    $('#delete_question').text('Are you sure you want to delete this salary component?');
    $('#delete_info').text($(this).data('salary-component'));
    $('.delete').removeClass('delete-contact');
    $('.delete').removeClass('delete-relation');
    $('.delete').addClass('delete-sal-component');
    $('#deleteModal').modal('show');
    id = $(this).data('id');
});

$('.modal-footer').on('click', '.delete-sal-component', function() {
    $.ajax({
      type: 'DELETE',
      url: "/employee/salary/" + id,
      data: {
        '_token': $('input[name=_token]').val(),
      },
      success: function(data) {
        toastr.success('Successfully deleted Salary Component!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        $('.salary-item'+ data['id']).remove();

      },
      error: function(data) {
        console.log('Error: ' + data.status);
      }
    });
});

// Upload Profile Image
$('#image').change(function() {
  if ($(this).val() != '') {
    upload();
  }
});

function upload() {
    var id = $('#employee_id').val();
    var preview = 'user-mb-128x128.jpg';
    var fileData = $('#image').prop('files')[0];
    //var uploadForm = $('#upload_form');
    var formData = new FormData();
    formData.append('image', fileData);
    formData.append('id', id);
    
    console.log(fileData);
    console.log(formData);

    $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name=_token]').attr('content')}
    });

    $('#loading').css('display', 'block');

    $.ajax({
      type: 'POST',
      url: "/employee/photo/",
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      success: function(data){
        if ((data.errors)) {
          $.each(data.errors, function(i, val) {
              toastr.error(val, 'Error Alert', {positionClass: 'toast-bottom-right', timeOut: 9000});
            });

          $('#preview_image').attr('src', '/storage/' + preview);
        } else {
          toastr.success('Successfully updated Profile Photo!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
          
          $('#file_name').val(data.image);
          $('#preview_image').attr('src', "/storage/" + data.image);

          console.log(data.image);
        }
        $('#loading').css('display', 'none');
      },
      error: function(data) { 
        console.log('Error: ' + data.status);
        toastr.error(data.status, 'Server Error', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        $('#preview_image').attr('src', '/storage/' + preview);
        $('#loading').css('display', 'none');
      },
    });

}






