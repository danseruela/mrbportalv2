$(function(){
    var tableEduc = $('#tblEducation').DataTable({
        'ajaxSource'  : '/api/education/' + $('#employee_id').val(),
        'columns'     : [
            { 'data':  'educ_level' },
            { 'data':  'school_name' },
            { 'data':  'year_start' },
            { 'data':  'year_end' },
            //{ 'data':  'degree' },
            { 'data':   'id',
                'render': function(data, type, row){
                    var actions; 
                    var a_view = ''
                    var a_update = ''
                    var a_delete = '';

                    //if($('#amlaTable').data('can_update') == 1) {
                    a_update = '<a class="tooltipped btn btn-primary editEduc" href="#" title="Edit Details" data-id="' + row.id + '"><i class="fa fa-edit"></i></a> '; 
                    //} 
                    //if ($('#amlaTable').data('can_delete') == 1) {
                    a_delete = '<a class="tooltipped btn btn-danger deleteEduc" href="#" title="Delete Item" data-id="' + row.id + '"><i class="fa fa-trash"></i></a> ';
                    //} 
                    //if ($('#amlaTable').data('can_update') == 0 && $('#amlaTable').data('can_delete') == 0) {
                    a_view = '<a class="tooltipped btn btn-success viewEduc" href="#" title="View Details" data-id="' + row.id + '"><i class="fa fa-eye"></i></a> ';
                    //}

                    actions = a_update + a_delete;
                    
                    return actions;
            } } ],
        'order'       : [ 2, 'asc' ],
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    // Initialize tooltip on datatable redraw
    tableEduc.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    /*************************************
     *  BUTTON VISIBILITY
     * 
     */
    // Get the column API object
    var column = tableEduc.column( 4 );
    
    // initial button state
    column.visible(false);
    $('#addEducation').css('display', 'none');

    $('#editEmployeeProfile').on('click', function(e){
        e.preventDefault();
    
        // Toggle the visibility
        column.visible( ! column.visible() );
        $('#addEducation').css('display', 'inline-block');
    });
    /************************************/

    // Initialize Dropdown menus
    dropdownSelect($('#educ_level'), '/api/educlevel');
    
    // Click create button for adding Education.
    $(document).on('click', '#addEducation', function(){
        $('#educAttainmentModal .modal-title').text('Setup: Create Education');
        $('#btnPostEduc span').text(' Save');
        $('#btnPostEduc').addClass('saveEduc');
        $('#btnPostEduc').removeClass('updateEduc');
        $('#educAttainmentModal span.select2-container--default .select2-selection--single, #school_name, #year_start').removeAttr('style');
        $('#educAttainmentModal input').each(function(){
            $(this).val('');
        });
        $('#educ_level').val(null).trigger('change');
        $('#educAttainmentModal').modal('show');
    });
    // Save Education
    $(document).on('click', '.saveEduc', function(){
        // Check required fields
        if ($('#educ_level').val() == null || $('#school_name').val() == '' || $('#year_start').val() == '') {
            $('#educAttainmentModal span.select2-container--default .select2-selection--single, #school_name, #year_start').attr('style', 'border: 1px solid red !important');
            toastr.warning('Please fill in required fields.', 'Warning!', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        } else {
            $.ajax({
                type: 'POST',
                url:   '/api/education',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'emp_id': $('#employee_id').val(),
                    'level' : $('#educ_level').val(),
                    'school_name': $('#school_name').val(),
                    'year_start': $('#year_start').val(),
                    'year_end': $('#year_end').val(),
                    'degree': $('#degree').val(),
                },
                success: function(response) {
                    toastr.success('Successfully added Education!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableEduc.ajax.url( '/api/education/' + $('#employee_id').val() ).load();
                    $('#educAttainmentModal').modal('hide');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }   
    });
    // Edit Education Details
    $(document).on('click', '.editEduc', function(){
        $('#educAttainmentModal .modal-title').text('Edit: Education Details');
        $('#btnPostEduc span').text(' Update');
        $('#btnPostEduc').removeClass('saveEduc');
        $('#btnPostEduc').addClass('updateEduc');
        $('#educAttainmentModal span.select2-container--default .select2-selection--single, #school_name, #year_start').removeAttr('style');

        // retrieve remote data
        $.ajax({
            type: 'GET',
            url:   '/api/education/' + $(this).data('id') + '/show',
            success: function(response) {
                // Assign value to input field.
                $('#educ_level').val(response.data.level).trigger('change');
                $('#school_name').val(response.data.school_name);
                $('#year_start').val(response.data.year_start);
                $('#year_end').val(response.data.year_end);
                $('#degree').val(response.data.degree);
                
                $('#educAttainmentModal').modal({
                    show:  true,
                    backdrop: 'static',
                    keyboard: false,
                });
                $('#educAttainmentModal').on('shown.bs.modal', function () {
                    $('#educ_level').focus();
                });
            },
            error: function(response) {
                console.log('Error: ' + response.status);
            },
        });

        updateEduc = $(this).data('id');
        //$('#amla_id').val(updateItemId);
    });
    // Update ID Details
    $(document).on('click', '.updateEduc', function(){
        if ($('#educ_level').val() == null || $('#school_name').val() == '' || $('#year_start').val() == '') {
            $('#educAttainmentModal span.select2-container--default .select2-selection--single, #school_name, #year_start').attr('style', 'border: 1px solid red !important');
            toastr.warning('Please fill in required fields.', 'Warning!', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        } else {
            $.ajax({
                type:   'PUT',
                url:    '/api/education/' + updateEduc,
                data:   {
                    '_token': $('input[name=_token]').val(),
                    'emp_id': $('#employee_id').val(),
                    'level' : $('#educ_level').val(),
                    'school_name': $('#school_name').val(),
                    'year_start': $('#year_start').val(),
                    'year_end': $('#year_end').val(),
                    'degree': $('#degree').val(),
                },
                success: function(response){
                    toastr.success('Successfully updated Education!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableEduc.ajax.url( '/api/education/' + $('#employee_id').val() ).load();
                    $('#educAttainmentModal').modal('hide');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }
    });

    // Remove ID
    $(document).on('click', '.deleteEduc', function(){
        deleteEduc = $(this).data('id');
        $('#btnConfirmDeleteEduc').addClass('deleteEducConfirm');
        $('#deleteEducModal .modal-title').text('Delete : Education');

        $('#deleteEducModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
    });
    // Confirm Delete Item
    $(document).on('click', '.deleteEducConfirm', function(){
        $.ajax({
            type: 'DELETE',
            url:  '/api/education/' + deleteEduc,
            data: {
            '_token': $('input[name=_token]').val(),
            },
            success:  function(response){
                if(response.error==null) {
                    toastr.success('Education has been successfully deleted!', 'Success', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableEduc.ajax.url( '/api/education/' + $('#employee_id').val() ).load();
                    console.log('Education successfully deleted.');
                } else {
                    toastr.warning(response.error, "Warning!");
                    console.log(response.error);
                }
            },
            error:  function(response){
            console.log('Error: ' + response.status)
            }
        });

        $('#deleteEducModal').modal('hide');
    });
        
});

