$(document).ready(function() {
  // Date picker
  $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
  });
  // Flash message
  $("#flash-message").animate({
      opacity : '0', 
  }, "slow");

  $(".flash-message").animate({
      opacity : '0', 
    }, "5000");

  // Check auth user is_first_login
  if( $('.main-header').data('is-first-login') == 1) {
    $('h4.modal-title').text('Change Password');
    id = $('.main-header').data('user-id');
    $('#changePasswordModal').modal('show');
  }

});

// notification mark as read
function markNotificationAsRead() {
  $.get('markAsRead');
}