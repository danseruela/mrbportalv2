$(document).ready(function() {
	// submit button disabled.
	$("#save-password").prop("disabled", true);
	$("input:password").keyup(function(){
		var disable = false;
		$("input:password").each(function() {
			if ($(this).val()=="") {
				disable = true;
			} 
		});
	$("#save-password").prop("disabled", disable);	
	});

	//Check password length.
    $("#new_password").keyup(function(){
    	var input = $("#new_password").val();
    	if (input.length < 6) {
    		$("#errPassMatchMsg").html("Passwords (6) minimum characters.");
        $("#save-password").prop("disabled", true);
    	} else {
    		$("#errPassMatchMsg").html("");
    	}
    });

    //Check password matched.
    $("#confirm_password").keyup(function(){
    	if ($("#new_password").val() != $("#confirm_password").val()) {
    		$("#errPassConMatchMsg").html("Passwords do not match!");
        $("#save-password").prop("disabled", true);
    	} else {
    		$("#errPassConMatchMsg").html("");
    	}
    });
});

// Show Change Password Modal
$(document).on('click', '.change-password', function() {
  $('#changePasswordModal .modal-title').text('Change Password');
  $('#user-heading').text($(this).data("username"));
  $('#current_password').val('');
  $('#new_password').val('');
  $('#confirm_password').val('');
  $('#changePasswordModal').modal('show');
  id = $(this).data('user-id');
});

$(document).on('click', '#save-password', function() {
  	$.ajaxSetup({
  	  headers: {
  	    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
  	  }
  	});

    $.ajax({
        type: 'PUT',
        url: '/user/updatepassword/' + id,
        data: {
          'password': $('#current_password').val(),
          'new_password': $('#new_password').val(),
        },
        success: function(data) {
          $('#changePasswordModal').modal('hide');
          if (data.error) {
            toastr.error(data.message, 'Error Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
          } else {
            toastr.success(data.message, 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
            $('.callout').hide();
          }
        },
        error: function(data) {
          $('#changePasswordModal').modal('hide');
          toastr.error(data.status, 'Error Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
          console.log('Error: ' + data.status);
        },
    });

});