$(function(){
    var tableEmpHistory = $('#tblEmpHistory').DataTable({
        'ajaxSource'  : '/api/emphistory/' + $('#employee_id').val(),
        'columns'     : [
            { 'data':  'employer_name' },
            { 'data':  'designation' },
            { 'data':  'date_start' },
            { 'data':  'date_end' },
            { 'data':   'id',
              'render': function(data, type, row){
                  var actions; 
                  var a_view = ''
                  var a_update = ''
                  var a_delete = '';

                  //if($('#amlaTable').data('can_update') == 1) {
                    a_update = '<a class="tooltipped btn btn-primary editEmpHistory" href="#" title="Edit Details" data-id="' + row.id + '"><i class="fa fa-edit"></i></a> '; 
                  //} 
                  //if ($('#amlaTable').data('can_delete') == 1) {
                    a_delete = '<a class="tooltipped btn btn-danger deleteEmpHistory" href="#" title="Delete Item" data-id="' + row.id + '"><i class="fa fa-trash"></i></a> ';
                  //} 
                  //if ($('#amlaTable').data('can_update') == 0 && $('#amlaTable').data('can_delete') == 0) {
                    a_view = '<a class="tooltipped btn btn-success viewEmpHistory" href="#" title="View Details" data-id="' + row.id + '"><i class="fa fa-eye"></i></a> ';
                  //}

                  actions = a_update + a_delete;
                  
                  return actions;
            } } ],
      'order'       : [ 2, 'asc' ],
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });

    /*************************************
     *  BUTTON VISIBILITY
     * 
     */
    // Get the column API object
    var column = tableEmpHistory.column( 4 );
    
    // initial button state
    column.visible(false);
    $('#addEmpHistory').css('display', 'none');

    $('#editEmployeeProfile').on('click', function(e){
        e.preventDefault();
    
        // Toggle the visibility
        column.visible( ! column.visible() );
        $('#addEmpHistory').css('display', 'inline-block');
    });
    /************************************/

    // Initialize tooltip on datatable redraw
    tableEmpHistory.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );
    
    // Click create button for adding Employment History.
    $(document).on('click', '#addEmpHistory', function(){
        $('#empHistoryModal .modal-title').text('Setup: Create Employment History');
        $('#btnPostEmpHistory span').text(' Save');
        $('#btnPostEmpHistory').addClass('saveEmpHistory');
        $('#btnPostEmpHistory').removeClass('updateEmpHistory');
        $('#empHistoryModal #employer_name, #employer_address, #designation, #date_start, #date_end').removeAttr('style');
        $('#empHistoryModal input').each(function(){
            $(this).val('');
        });

        $('#empHistoryModal').modal('show');
    });
    // Save Employment History
    $(document).on('click', '.saveEmpHistory', function(){
        // Check required fields
        if ($('#employer_name').val() == '' || $('#employer_address').val() == '' || $('#designation').val() == '' || $('#date_start').val() == '' || $('#date_end').val() == '') {
            $('#empHistoryModal #employer_name, #employer_address, #designation, #date_start, #date_end').attr('style', 'border: 1px solid red !important');
            toastr.warning('Please fill in required fields.', 'Warning!', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        } else {
            $.ajax({
                type: 'POST',
                url:   '/api/emphistory',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'emp_id': $('#employee_id').val(),
                    'employer_name' : $('#employer_name').val(),
                    'employer_address': $('#employer_address').val(),
                    'designation': $('#designation').val(),
                    'employment_id_no': $('#employment_id_no').val(),
                    'date_start': $('#date_start').val(),
                    'date_end': $('#date_end').val(),
                    'tel_no': $('#eh_tel_no').val(),
                    'email': $('#eh_email').val(),
                },
                success: function(response) {
                    toastr.success('Successfully added Employment History!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableEmpHistory.ajax.url( '/api/emphistory/' + $('#employee_id').val() ).load();
                    $('#empHistoryModal').modal('hide');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }   
    });
    // Edit Employment History Details
    $(document).on('click', '.editEmpHistory', function(){
        $('#empHistoryModal .modal-title').text('Edit: Employment History Details');
        $('#btnPostEmpHistory span').text(' Update');
        $('#btnPostEmpHistory').removeClass('saveEmpHistory');
        $('#btnPostEmpHistory').addClass('updateEmpHistory');
        $('#empHistoryModal #employer_name, #employer_address, #designation, #date_start, #date_end').removeAttr('style');

        // retrieve remote data
        $.ajax({
            type: 'GET',
            url:   '/api/emphistory/' + $(this).data('id') + '/show',
            success: function(response) {
                // Assign value to input field.
                $('#employer_name').val(response.data.employer_name).trigger('change');
                $('#employer_address').val(response.data.employer_address);
                $('#designation').val(response.data.designation);
                $('#employment_id_no').val(response.data.employment_id_no);
                $('#date_start').val(response.data.date_start);
                $('#date_end').val(response.data.date_end);
                $('#eh_tel_no').val(response.data.tel_no);
                $('#eh_email').val(response.data.email);
                
                $('#empHistoryModal').modal({
                    show:  true,
                    backdrop: 'static',
                    keyboard: false,
                });
                $('#empHistoryModal').on('shown.bs.modal', function () {
                    $('#employer_name').focus();
                });
            },
            error: function(response) {
                console.log('Error: ' + response.status);
            },
        });

        updateEmpHistory = $(this).data('id');
    });
    // Update Employment History Details
    $(document).on('click', '.updateEmpHistory', function(){
        if ($('#employer_name').val() == '' || $('#employer_address').val() == '' || $('#designation').val() == '' || $('#date_start').val() == '' || $('#date_end').val() == '') {
            $('#empHistoryModal #employer_name, #employer_address, #designation, #date_start, #date_end').attr('style', 'border: 1px solid red !important');
            toastr.warning('Please fill in required fields.', 'Warning!', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        } else {
            $.ajax({
                type:   'PUT',
                url:    '/api/emphistory/' + updateEmpHistory,
                data:   {
                    '_token': $('input[name=_token]').val(),
                    'emp_id': $('#employee_id').val(),
                    'employer_name' : $('#employer_name').val(),
                    'employer_address': $('#employer_address').val(),
                    'designation': $('#designation').val(),
                    'employment_id_no': $('#employment_id_no').val(),
                    'date_start': $('#date_start').val(),
                    'date_end': $('#date_end').val(),
                    'tel_no': $('#eh_tel_no').val(),
                    'email': $('#eh_email').val(),
                },
                success: function(response){
                    toastr.success('Successfully updated Employment History!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableEmpHistory.ajax.url( '/api/emphistory/' + $('#employee_id').val() ).load();
                    $('#empHistoryModal').modal('hide');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }
    });

    // Remove Employment History
    $(document).on('click', '.deleteEmpHistory', function(){
        deleteEmpHistory = $(this).data('id');
        $('#btnConfirmDeleteEmpHistory').addClass('deleteEmpHistoryConfirm');
        $('#deleteEmpHistoryModal .modal-title').text('Delete : Employment History');

        $('#deleteEmpHistoryModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
    });
    // Confirm Delete Item
    $(document).on('click', '.deleteEmpHistoryConfirm', function(){
        $.ajax({
            type: 'DELETE',
            url:  '/api/emphistory/' + deleteEmpHistory,
            data: {
            '_token': $('input[name=_token]').val(),
            },
            success:  function(response){
                if(response.error==null) {
                    toastr.success('Employment History has been successfully deleted!', 'Success', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableEmpHistory.ajax.url( '/api/emphistory/' + $('#employee_id').val() ).load();
                    console.log('Employment History successfully deleted.');
                } else {
                    toastr.warning(response.error, "Warning!");
                    console.log(response.error);
                }
            },
            error:  function(response){
            console.log('Error: ' + response.status)
            }
        });

        $('#deleteEmpHistoryModal').modal('hide');
    });

});