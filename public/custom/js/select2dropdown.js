function dropdownSelect(selectId, url) {
    // Initialize select forms
    let dropdown = selectId;
    let itemName = dropdown.data('name');
    dropdown.empty();
  
    dropdown.append('<option selected="true" disabled>Choose ' + itemName + '</option>');
    dropdown.prop('selectedIndex', 0);
  
    if(url instanceof Array) {
        $.each(url, function (key, entry) {
            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.desc));
        })
    } else {
        // Populate dropdown with list of provinces
        $.getJSON(url, function (response) {
            $.each(response.data, function (key, entry) {
            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.desc));
            })
        });
    }
  
    return dropdown.select2();
  }