$(document).ready(function() {
      //Initialize Select2 Elements
      $('.select2').select2();

      //Initialize status value
      if ($('#deleted').val() != null) {
        $("#status").select2().select2('val',$('#deleted').val()); 
      }
  });

$(function () {
  $('#userTbl1').DataTable({
    'order':  [[0, 'desc']]
  });

  $('#userTbl2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  });
});

// Create a new password on page load
$('input[rel="gp"]').each(function(){
  $(this).val(randString($(this)));
});

// Create a new password
$(".getNewPass").click(function(){
  $("#save-password").attr('data-dismiss', 'modal');
  var field = $(this).closest('div').find('input[rel="gp"]');
  field.val(randString(field));
});

// Auto Select Pass On Focus
$('input[rel="gp"]').on("click", function () {
   $(this).select();
});

// Reset Password
$(document).on('click', '.reset-password', function() {
  $('#resetPasswordModal .modal-title').text('Reset Password');
  $('#user-heading').text($(this).data("reset-username"));
  $('#password').val('');
  $('#resetPasswordModal').modal('show');
  id = $(this).data('reset-id');
});

$(document).on('click', '#save-password', function() {
  if($('#password').val()=="") {
    toastr.error('Please generate a reset password', 'Error Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
  } else {
    $.ajax({
        type: 'PUT',
        url: '/user/resetpassword/' + id,
        data: {
          '_token': $('input[name=_token]').val(),
          'password': $('#password').val(),
        },
        success: function(data) {
          toastr.success(data.message, 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        },
        error: function(data) {
          console.log('Error: ' + data.status);
        },
    });
  }
});

// Edit User
$(document).on('click', '.edit-user', function() {

  $('#editUserModal .modal-title').text('Edit User');
  $('#user_id').val($(this).data('id'));
  $('#username').val($(this).data('username'));
  $("#emp_id").val($(this).data('emp-id')).trigger('change');

  var roles = $.get('/json/' + $(this).data('id'), function(data) {
    $('#user_roles').val(data).trigger('change');
    console.log('role id: ' + data);
  });


  $("#status").val($(this).data('status')).trigger('change');
  $('#editUserModal').modal('show');
  id = $('#user_id').val();

});

$(document).on('click', '#save-user', function() {
  $.ajax({
      type: 'PUT',
      url: '/user/' + id,
      data: {
        '_token': $('input[name=_token]').val(),
        'username': $('#username').val(),
        'emp_id': $('#emp_id').val(),
        'user_roles': $('#user_roles').val(),
        'status': $('#status').val(),
      },
      success: function(data) {
        if ((data.errors)) {
          $.each(data.errors, function(index, val) {
              toastr.error(val, 'Error Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
          });
        } else {
            var badge = '';
            if (data.user.deleted == 0) {
              badge = 'bg-green';
            } else {
              badge = 'bg-red';
            }

            var isOnline = '';
            if (data.related.isOnline) {
              isOnline = '<i class="fa fa-circle text-success"></i> Online';
            } else {
              isOnline = '<i class="fa fa-circle text-danger"></i> Offline';
            }

            var user_roles = '';
            $.each(data.related.getRole, function(i, role) {
              user_roles += '<span class="badge bg-green">' + role.desc + '</span> ';
            });

            toastr.success('Successfully updated User!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
            $('.item' + data.user.id).replaceWith(
              '<tr class="item' + data.user.id +'">' +
                '<td>' + isOnline + '</td>' +
                '<td><a class="text-success" href="#">' + data.related.getEmployee + '</a></td>' +
                '<td>' + data.user.username + '</td>' +
                '<td>' + data.branch + '</td>' +
                '<td><span class="badge ' + badge + '">' + data.related.getStatus + '</span></td>' +
                '<td>' +
                  '<a href="#" data-toggle="tooltip" title="Reset Password" class="reset-password btn btn-primary" data-reset-id="' + data.user.id + '" data-reset-username="' + data.user.username + '"><i class="fa fa-key"></i></a> ' +
                  '<a href="#" data-toggle="tooltip" title="Edit" class="edit-user btn btn-warning" data-id="' + data.user.id + '" data-emp-id="' + data.user.emp_id + '" data-username="' + data.user.username + '" data-status="' + data.user.deleted + '"><i class="fa fa-edit"></i></a> ' +
                  '<a href="#" data-toggle="tooltip" title="Delete" class="delete-user btn btn-danger" data-del-id="' + data.user.id + '" data-del-username="' + data.user.username + '"><i class="fa fa-trash"></i></a>' +
                '</td>' +
              '</tr>');
        }
      },
      error: function(data) {
        console.log('Error: ' + data.status);
      },
    });
});

// Delete User
$(document).on('click', '.delete-user', function() {
  $('#deleteUserModal .modal-title').text('Delete User');
  $('#deleteUserModal .user-heading').text('Are you sure you want to delete this user?');
  $('#deleteUserModal .user-details').text($(this).data('del-username'));
  $('#deleteUserModal').modal('show');
  id = $(this).data('del-id');
});

$(document).on('click', '#delete-user', function() {
  $.ajax({
      type: 'DELETE',
      url: '/user/' + id,
      data: {
        '_token': $('input[name=_token]').val(),
      },
      success: function(data) {
        toastr.success('Successfully deleted User!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        $('.item'+ data['id']).remove();
      },
      error: function(data) {
        console.log('Error: ' + data.status);
      },
    });
});