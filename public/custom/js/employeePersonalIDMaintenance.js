$(function(){
    var tableID = $('#tblID').DataTable({
        'ajaxSource'  : '/api/personalid/' + $('#employee_id').val(),
        'columns'     : [
            { 'data':  'id_type' },
            { 'data':  'id_no' },
            { 'data':  'date_issued' },
            { 'data':  'date_expiry' },
            { 'data':   'id',
              'render': function(data, type, row){
                  var actions; 
                  var a_view = ''
                  var a_update = ''
                  var a_delete = '';

                  //if($('#amlaTable').data('can_update') == 1) {
                    a_update = '<a class="tooltipped btn btn-primary editID" href="#" title="Edit Details" data-id="' + row.id + '"><i class="fa fa-edit"></i></a> '; 
                  //} 
                  //if ($('#amlaTable').data('can_delete') == 1) {
                    a_delete = '<a class="tooltipped btn btn-danger deleteID" href="#" title="Delete Item" data-id="' + row.id + '"><i class="fa fa-trash"></i></a> ';
                  //} 
                  //if ($('#amlaTable').data('can_update') == 0 && $('#amlaTable').data('can_delete') == 0) {
                    a_view = '<a class="tooltipped btn btn-success viewID" href="#" title="View Details" data-id="' + row.id + '"><i class="fa fa-eye"></i></a> ';
                  //}

                  actions = a_update + a_delete;
                  
                  return actions;
            } } ],
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });

    /*************************************
     *  BUTTON VISIBILITY
     * 
     */
    // Get the column API object
    var column = tableID.column( 4 );
    
    // initial button state
    column.visible(false);
    $('#addID').css('display', 'none');

    $('#editEmployeeProfile').on('click', function(e){
        e.preventDefault();
    
        // Toggle the visibility
        column.visible( ! column.visible() );
        $('#addID').css('display', 'inline-block');
    });
    /************************************/

    // Initialize tooltip on datatable redraw
    tableID.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    // Initialize Dropdown menus
    dropdownSelect($('#id_type'), '/api/idtype');
    
    // Click create button for adding personal IDs.
    $(document).on('click', '#addID', function(){
        $('#personalIDModal .modal-title').text('Setup: Create Personal ID');
        $('#btnPostID span').text(' Save');
        $('#btnPostID').addClass('saveID');
        $('#btnPostID').removeClass('updateID');
        $('#personalIDModal span.select2-container--default .select2-selection--single, #id_no, #date_issued').removeAttr('style');
        $('#personalIDModal input').each(function(){
            $(this).val('');
        });
        $('#id_type').val(null).trigger('change');
        $('#personalIDModal').modal('show');
    });
    // Save personal ID
    $(document).on('click', '.saveID', function(){
        // Check required fields
        if ($('#id_type').val() == null || $('#id_no').val() == '') {
            $('#personalIDModal span.select2-container--default .select2-selection--single, #id_no').attr('style', 'border: 1px solid red !important');
            toastr.warning('Please fill in required fields.', 'Warning!', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        } else {
            $.ajax({
                type: 'POST',
                url:   '/api/personalid',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'emp_id': $('#employee_id').val(),
                    'type' : $('#id_type').val(),
                    'id_no': $('#id_no').val(),
                    'date_issued': $('#date_issued').val(),
                    'date_expiry': $('#date_expiry').val(),
                },
                success: function(response) {
                    toastr.success('Successfully added ID!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableID.ajax.url( '/api/personalid/' + $('#employee_id').val() ).load();
                    $('#personalIDModal').modal('hide');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }   
    });
    // Edit ID Details
    $(document).on('click', '.editID', function(){
        $('#personalIDModal .modal-title').text('Edit: Personal ID');
        $('#btnPostID span').text(' Update');
        $('#btnPostID').removeClass('saveID');
        $('#btnPostID').addClass('updateID');
        $('#personalIDModal span.select2-container--default .select2-selection--single, #id_no, #date_issued').removeAttr('style');

        // retrieve remote data
        $.ajax({
            type: 'GET',
            url:   '/api/personalid/' + $(this).data('id') + '/show',
            success: function(response) {
                // Assign value to input field.
                $('#id_type').val(response.data.type).trigger('change');
                $('#id_no').val(response.data.id_no);
                $('#date_issued').val(response.data.date_issued);
                $('#date_expiry').val(response.data.date_expiry);
                
                $('#personalIDModal').modal({
                    show:  true,
                    backdrop: 'static',
                    keyboard: false,
                });
                $('#personalIDModal').on('shown.bs.modal', function () {
                    $('#id_type').focus();
                });
            },
            error: function(response) {
                console.log('Error: ' + response.status);
            },
        });

        updateId = $(this).data('id');
        //$('#amla_id').val(updateItemId);
    });
    // Update ID Details
    $(document).on('click', '.updateID', function(){
        if($('#id_type').val() == null || $('#id_no').val() == '' ) {
            $('#personalIDModal span.select2-container--default .select2-selection--single, #id_no').attr('style', 'border: 1px solid red !important');
            toastr.warning('Please fill in required fields.', 'Warning!', { positionClass: 'toast-bottom-right', timeOut: 5000 });
        } else {
            $.ajax({
                type:   'PUT',
                url:    '/api/personalid/' + updateId,
                data:   {
                    '_token': $('input[name=_token]').val(),
                    'type' : $('#id_type').val(),
                    'id_no': $('#id_no').val(),
                    'date_issued': $('#date_issued').val(),
                    'date_expiry': $('#date_expiry').val(),
                },
                success: function(response){
                    toastr.success('Successfully updated ID!', 'Success Alert', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableID.ajax.url( '/api/personalid/' + $('#employee_id').val() ).load();
                    $('#personalIDModal').modal('hide');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }
    });

    // Remove ID
    $(document).on('click', '.deleteID', function(){
        deleteId = $(this).data('id');
        $('#btnConfirmDeleteID').addClass('deleteItemConfirm');
        $('#deleteIDModal .modal-title').text('Delete : Personal ID');

        $('#deleteIDModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
    });
    // Confirm Delete Item
    $(document).on('click', '.deleteItemConfirm', function(){
        $.ajax({
            type: 'DELETE',
            url:  '/api/personalid/' + deleteId,
            data: {
            '_token': $('input[name=_token]').val(),
            },
            success:  function(response){
                if(response.error==null) {
                    toastr.success('ID has been successfully deleted!', 'Success', { positionClass: 'toast-bottom-right', timeOut: 5000 });
                    tableID.ajax.url( '/api/personalid/' + $('#employee_id').val() ).load();
                    console.log('ID successfully deleted.');
                } else {
                    toastr.warning(response.error, "Warning!");
                    console.log(response.error);
                }
            },
            error:  function(response){
            console.log('Error: ' + response.status)
            }
        });

        $('#deleteIDModal').modal('hide');
    });

});