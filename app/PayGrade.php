<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayGrade extends Model
{
    protected $table = 'pay_grades';

    protected $fillable = ['id', 'desc', 'min_salary', 'max_salary'];

    public $timestamps = false;
}
