<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IDType extends Model
{
    protected $table = 'id_type';

    public $timestamps = false;
}
