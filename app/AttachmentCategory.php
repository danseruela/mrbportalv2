<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttachmentCategory extends Model
{    
    protected $table = 'attachment_category';

    public $timestamps = false;
}
