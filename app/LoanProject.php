<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanProject extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'loan_project';
}
