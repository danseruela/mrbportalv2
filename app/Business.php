<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'business';

    public function project() {
        return $this->belongsTo(LoanProject::class);
    }
}
