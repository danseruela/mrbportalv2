<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalComponentType extends Model
{
    protected $table = 'sal_component_type';

    protected $fillable = ['id', 'desc'];

    public $timestamps = false;
}
