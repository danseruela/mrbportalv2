<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactCat extends Model
{
    protected $table = 'contact_cat';

    public $timestamps = false;
}
