<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelationIcbs extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'relation';

    public function type() {
        return $this->belongsTo(Lov::class);
    }

    public function status() {
        return $this->belongsTo(ConfigItemStatus::class);
    }

    public function relativeInfo() {
        return $this->hasOne(Customer::class, 'id', 'customer2_id')->select(['id', 'customer_id', 'name1', 'name2', 'name3', 'name4']);
    }

}
