<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpEduc extends Model
{
    protected $table = 'emp_educ';

    public $timestamps = false;
}
