<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresentedId extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'presented_id';

    public function type() {
        return $this->belongsTo(Lov::class);
    }

    public function status() {
        return $this->belongsTo(ConfigItemStatus::class);
    }
}
