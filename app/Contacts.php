<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';

    protected $fillable = ['id', 'emp_id', 'type', 'category', 'desc', 'created_at', 'modified_at', 'created_by', 'modified_by'];

    public $timestamps = false;

    public function employee() {
    	return $this->belongsTo(Employee::class);
    }

    public function getType() {
    	return $this->hasOne(ContactType::class, 'id', 'type');
    }

    public function getCategory() {
    	return $this->hasOne(ContactCat::class, 'id', 'category');
    }
}
