<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'contact';

    public function type() {
        return $this->belongsTo(Lov::class);
    }
}
