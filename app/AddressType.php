<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'address_type';

}
