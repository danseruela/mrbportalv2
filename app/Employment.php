<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'employment';

    public function region() {
        return $this->belongsTo(Lov::class);
    }
}
