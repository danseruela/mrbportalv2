<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class AttachmentType extends Model implements HasMedia
{
    use HasMediaTrait;
    
    // protected $connection = 'pgsql';

    protected $table = 'attachment_type';

    protected $fillable = ['id', 'cat_id', 'desc'];

    public $timestamps = false;
}
