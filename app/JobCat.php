<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCat extends Model
{
    protected $table = 'job_cat';

    protected $fillable = ['id', 'desc'];

    public $timestamps = false;
}
