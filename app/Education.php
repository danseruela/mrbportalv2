<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'education';

    public function type() {
        return $this->belongsTo(Lov::class);
    }
}
