<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AMLAType extends Model
{
    protected $table = 'amla_type';

    public $timestamps = false;
}
