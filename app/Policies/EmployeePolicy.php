<?php

namespace App\Policies;

use App\User;
use App\Employee;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole('Administrator')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the employee.
     *
     * @param  \App\User  $user
     * @param  \App\Employee  $employee
     * @return mixed
     */
    public function generateReport(User $user)
    {
        if ($user->hasRole('Reporting')) {
            return true;
        }
        
        return false;    
    }

    public function view(User $user, Employee $employee)
    {
        //return true;
        if ($user->hasRole('Content Manager')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can create employees.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //return $user->id > 0;
        if ($user->hasRole('Content Manager')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can update the employee.
     *
     * @param  \App\User  $user
     * @param  \App\Employee  $employee
     * @return mixed
     */
    public function update(User $user, Employee $employee)
    {
        if ($user->hasRole('Content Manager')) {
            return true;
        }
        
        return false;
        //return $user->id == $employee->created_by;
    }

    /**
     * Determine whether the user can delete the employee.
     *
     * @param  \App\User  $user
     * @param  \App\Employee  $employee
     * @return mixed
     */
    public function delete(User $user, Employee $employee)
    {
        //
    }
}
