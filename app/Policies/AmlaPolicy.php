<?php

namespace App\Policies;

use App\User;
use App\Amla;
use Illuminate\Auth\Access\HandlesAuthorization;

class AmlaPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole('Administrator')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the amla.
     *
     * @param  \App\User  $user
     * @param  \App\Amla  $amla
     * @return mixed
     */
    public function view(User $user, Amla $amla)
    {
        if ($user->hasAnyRole(['User', 'Content Manager', 'Content Manager - AMLA', 'Contributor - AMLA'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can create amlas.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->hasAnyRole(['Content Manager - AMLA', 'Contributor - AMLA'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can update the amla.
     *
     * @param  \App\User  $user
     * @param  \App\Amla  $amla
     * @return mixed
     */
    public function update(User $user, Amla $amla)
    {
        if ($user->hasAnyRole(['Content Manager - AMLA', 'Contributor - AMLA'])) {
            return true;
        }
        
        return false;
    }

    /**
     * Determine whether the user can delete the amla.
     *
     * @param  \App\User  $user
     * @param  \App\Amla  $amla
     * @return mixed
     */
    public function delete(User $user, Amla $amla)
    {
        if ($user->hasAnyRole(['Content Manager - AMLA'])) {
            return true;
        }
        
        return false;
    }
}
