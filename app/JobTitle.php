<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{
    protected $table = 'job_title';

    protected $fillable = ['id', 'cat_id', 'desc'];

    public $timestamps = false;

    public function employee() {
		return $this->belongsTo('App\Employee');
	}
}

