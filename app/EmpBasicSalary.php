<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpBasicSalary extends Model
{
    protected $table = 'emp_basic_salary';

    protected $fillable = ['emp_code', 'pay_grd_code', 'sal_component_type', 'basic_salary', 'pay_freq_code', 'salary_component', 'comments'];

    public $timestamps = false;

    public function getSalaryGrade() {
    	return $this->hasOne(PayGrade::class, 'id', 'pay_grd_code');
    }

    public function getPayFrequency() {
    	return $this->hasOne(PayFrequency::class, 'id', 'pay_freq_code');
    }

    public function getSalComponentType() {
    	return $this->hasOne(SalComponentType::class, 'id', 'sal_component_type');
    }
}
