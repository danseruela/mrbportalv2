<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmlaGroup extends Model
{
    protected $table = 'amla_group';

    protected $fillable = ['desc', 'amla_type', 'date_issued', 'created_at', 'created_by', 'modified_at', 'modified_by'];

    public $timestamps = false;

    public function getAmlaType() {
        return $this->hasOne(AMLAType::class, 'id', 'amla_type');
    }
}
