<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelConsanguinity extends Model
{
    protected $table = 'rel_consanguinity';

    protected $fillable = ['id', 'desc1', 'desc2'];

    public $timestamps = false;

    public function relation() {
        return $this->belongsTo(Relations::class);
    }

}
