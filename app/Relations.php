<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relations extends Model
{
    protected $table = 'relations';

    protected $fillable = ['id', 'emp_id', 'emp_related_id', 'firstname', 'middlename', 'lastname', 'type', 'consanguinity', 'is_related', 'is_emergency_contact', 'address', 'contact_no'];

    public $timestamps = false;

    public function getType() {
        return $this->hasOne(RelType::class, 'id', 'type');
    }

    public function getConsanguinity() {
        return $this->hasOne(RelConsanguinity::class, 'id', 'consanguinity');
    }

    public function empRelatives() {
    	return $this->belongsTo(Employee::class, 'emp_related_id');
    }

    public function employee() {
        return $this->hasOne(Employee::class, 'id', 'emp_id');
    }

    public function empRelated() {
        return $this->hasOne(Employee::class, 'id', 'emp_related_id');
    }
}
