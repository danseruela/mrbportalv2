<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmlaRelation extends Model
{
    protected $table = 'amla_relations';

    protected $fillable = ['id', 'amla_id', 'firstname', 'middlename', 'lastname', 'type', 'consanguinity'];

    public $timestamps = false;

    public function getType() {
        return $this->hasOne(RelType::class, 'id', 'type');
    }

    public function getConsanguinity() {
        return $this->hasOne(RelConsanguinity::class, 'id', 'consanguinity');
    }
}
