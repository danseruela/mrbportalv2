<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigItemStatus extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'config_item_status';
}
