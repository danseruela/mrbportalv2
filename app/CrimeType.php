<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrimeType extends Model
{
    protected $table = 'crime_type';

    public $timestamps = false;

    public function amla()
	{
		return $this->belongsToMany(Amla::class, 'amla_crimes', 'crime_id', 'amla_id');
	}
}
