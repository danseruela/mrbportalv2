<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {   
        /*if(Auth::user()->role_id == 1) {
            return true;
        }*/

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emp_no'            =>  'required|min:6',
            'firstname'         =>  'required|min:2',
            //'middlename'        =>  'required|min:2',
            'lastname'          =>  'required|min:2',
            'birthdate'         =>  'required',
            'gender'            =>  'required',
            'civil_status'      =>  'required',
            'address1'          =>  'required',
            'city'              =>  'required',
            'province'          =>  'required',
            'country'           =>  'required',
            'emp_status'        =>  'required',
            'emp_start_date'    =>  'required',
            'org_code'          =>  'required',
            'location_code'     =>  'required',
            'job_title'         =>  'required',
            'job_cat'           =>  'required',
        ];
    }

}
