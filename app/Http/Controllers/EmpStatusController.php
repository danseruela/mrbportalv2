<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmpStatus;
use DB;

class EmpStatusController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    protected $user;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_status_list')->with([ 
                'emp_statuses' => EmpStatus::all(),
            ]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }    
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_status_create')->with([
                'action'        =>  'create',
                'available_id'  =>  DB::table('emp_status')->max('id') + 1,
            ]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store
        $job_status = EmpStatus::create([
            'id'        =>  $request->input('job_stat_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        // flash message - created employee status successfully.
        $request->session()->flash('message', 'Successfully created Employment Status!');
        
        // redirect
        return redirect('job/status');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job_status = EmpStatus::find($id);

        return view('job_status_create')->with([
            'job_status'    =>  $job_status,
            'action'        =>  'edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        EmpStatus::where('id', '=', $id )
        ->update([
            'id'        =>  $request->input('job_stat_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        
        // flash message - updated employee status successfully.
        $request->session()->flash('message', 'Successfully updated Employment Status!');
        
        // redirect
        return redirect('job/status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        
    }
}
