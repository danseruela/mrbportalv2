<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;
use Validator;
use App\EmpBasicSalary;
use App\PayGrade;
use App\SalComponentType;
use App\PayFrequency;

class EmpBasicSalaryController extends Controller
{
    protected $rules = 
    [
        'salary_component'  =>  'required',
        'pay_freq_code'     =>  'required',
        'basic_salary'      =>  'required|numeric',
    ];

    protected $messages = 
    [
        'salary_component.required'    =>  'Salary Component field is required.',
        'pay_freq_code.required'       =>  'Pay Frequency field is required.',
        'basic_salary.required'        =>  'Amount field is required.',
        'basic_salary.numeric'         =>  'Amount field should be numeric'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            // Store
            $salary = new EmpBasicSalary;
            $salary->emp_code           =  $request->emp_code;
            $salary->pay_grd_code       =  $request->pay_grd_code;
            $salary->sal_component_type =  $request->sal_component_type;
            $salary->basic_salary       =  $request->basic_salary;
            $salary->pay_freq_code      =  $request->pay_freq_code;
            $salary->salary_component   =  $request->salary_component;
            $salary->comments           =  $request->comments;
            $salary->save();

            $srTbl = [
                'getSalComponentType'   =>  SalComponentType::where('id', $request->sal_component_type)->pluck('desc')->all(),
                'getSalaryGrade'        =>  PayGrade::where('id', $request->pay_grd_code)->pluck('desc')->all(),
                'getPayFrequency'       =>  PayFrequency::where('id', $request->pay_freq_code)->pluck('desc')->all(),
            ];

            // Response
            return response()->json(['salary' => $salary, 'srTbl' => $srTbl]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            // Store
            $salary = EmpBasicSalary::findOrFail($id);
            $salary->emp_code           =  $request->emp_code;
            $salary->pay_grd_code       =  $request->pay_grd_code;
            $salary->sal_component_type =  $request->sal_component_type;
            $salary->basic_salary       =  $request->basic_salary;
            $salary->pay_freq_code      =  $request->pay_freq_code;
            $salary->salary_component   =  $request->salary_component;
            $salary->comments           =  $request->comments;
            $salary->save();

            $srTbl = [
                'getSalComponentType'   =>  SalComponentType::where('id', $request->sal_component_type)->pluck('desc')->all(),
                'getSalaryGrade'        =>  PayGrade::where('id', $request->pay_grd_code)->pluck('desc')->all(),
                'getPayFrequency'       =>  PayFrequency::where('id', $request->pay_freq_code)->pluck('desc')->all(),
            ];

            // Response
            return response()->json(['salary' => $salary, 'srTbl' => $srTbl]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete
        $salary = EmpBasicSalary::findOrFail($id);
        $salary->delete();

        return response()->json($salary);
    }
}
