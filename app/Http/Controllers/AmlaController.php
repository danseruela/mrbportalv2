<?php

namespace App\Http\Controllers;

use App\Amla;
use App\CrimeType;
use App\AmlaRelation;
use App\User;
use Illuminate\Http\Request;
use Exception;
use DB;

class AmlaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => Amla::select(
                        'amla.*',
                        DB::raw("CONCAT(IFNULL(`amla`.lastname, ''), ', ', IFNULL(`amla`.firstname, ''), ' ', IFNULL(`amla`.middlename, '')) AS fullname"),
                        'amla_type.desc AS type',
                        'organization.desc AS organization_desc',
                        'region.desc AS region',
                        'c.username AS created_by',
                        'm.username AS modified_by'
                        )
                        ->leftJoin('amla_type', 'amla.amla_type', '=', 'amla_type.id')
                        ->leftJoin('organization', 'amla.organization', '=', 'organization.id')
                        ->leftJoin('region', 'amla.region', '=', 'region.id')
                        ->leftJoin('users AS c', 'amla.created_by', '=', 'c.id')
                        ->leftJoin('users AS m', 'amla.modified_by', '=', 'm.id')
                        ->orderBy('fullname')
                        ->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $amla                   = new Amla;
        $amla->firstname        = $request->firstname;
        $amla->middlename       = $request->middlename;
        $amla->lastname         = $request->lastname;
        $amla->amla_type        = $request->amla_type;
        $amla->organization     = $request->organization;
        $amla->position         = $request->position;
        $amla->barangay         = $request->barangay;
        $amla->city             = $request->city;
        $amla->province         = $request->province;
        $amla->postal_code      = $request->postal_code;
        $amla->region           = $request->region;
        $amla->info_source      = $request->info_source;
        $amla->info_source_type = $request->info_source_type;
        $amla->remarks          = $request->remarks;
        $amla->created_at       = date('Y-m-d h:i:s');
        $amla->created_by       = $request->user;
        $amla->save();

        // Remove existing crimes.
        $amla->crimes()->detach();

        // Check if amla has multiple crimes
        if (is_array($request->crimes)) {
            $num = count($request->crimes);
                            
            foreach ($request->crimes as $crime) {
                // Add new crimes to amla.
                $amla->crimes()->attach(CrimeType::where('id', $crime)->first());    
            }
        }

        return response()->json([
            'data'  =>  $amla
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $amla = Amla::find($id);
        $crimes = !empty($amla) ? $amla->crimes($id)->get() : null;

        return response()->json([
            'data'          =>  $amla,
            'crimes'        =>  $crimes,
        ]);
    }

    /**
     * Display the specified resource by amla_type
     * 
     * @param int $amlatype
     * @return json response
     */
    public function grpByType($amlatype)
    {
        if($amlatype == 0) {
            return $this->index();
        } else {
            return response()->json([
                'data'  =>  Amla::select(
                            'amla.*',
                            DB::raw("CONCAT(IFNULL(`amla`.lastname, ''), ', ', IFNULL(`amla`.firstname, ''), ' ', IFNULL(`amla`.middlename, '')) AS fullname"),
                            'amla_type.desc AS type',
                            'organization.desc AS organization_desc',
                            'region.desc AS region',
                            'c.username AS created_by',
                            'm.username AS modified_by'
                            )
                            ->leftJoin('amla_type', 'amla.amla_type', '=', 'amla_type.id')
                            ->leftJoin('organization', 'amla.organization', '=', 'organization.id')
                            ->leftJoin('region', 'amla.region', '=', 'region.id')
                            ->leftJoin('users AS c', 'amla.created_by', '=', 'c.id')
                            ->leftJoin('users AS m', 'amla.modified_by', '=', 'm.id')
                            ->where('amla.amla_type', '=', $amlatype)
                            ->orderBy('fullname')
                            ->get(),
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $amla = Amla::findOrFail($id);
        $amla->firstname        = $request->firstname;
        $amla->middlename       = $request->middlename;
        $amla->lastname         = $request->lastname;
        $amla->amla_type        = $request->amla_type;
        $amla->organization     = $request->organization;
        $amla->position         = $request->position;
        $amla->barangay         = $request->barangay;
        $amla->city             = $request->city;
        $amla->province         = $request->province;
        $amla->postal_code      = $request->postal_code;
        $amla->region           = $request->region;
        $amla->info_source      = $request->info_source;
        $amla->info_source_type = $request->info_source_type;
        $amla->remarks          = $request->remarks;
        $amla->modified_at       = date('Y-m-d h:i:s');
        $amla->modified_by       = $request->user;
        $amla->save();

        // Remove existing crimes.
        $amla->crimes()->detach();

        // Check if amla has multiple crimes
        if (is_array($request->crimes)) {
            $num = count($request->crimes);
                            
            foreach ($request->crimes as $crime) {
                // Add new crimes to amla.
                $amla->crimes()->attach(CrimeType::where('id', $crime)->first());    
            }
        }

        return response()->json([
            'data'  =>  $amla
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $amla = Amla::find($id);
            // delete values on related relations table
            $relations = AmlaRelation::where('amla_id', '=', $id);
            if(!empty($relations)) {
                $relations->delete();
            }
            // delete values on related amla_crimes table
            $amla->crimes()->detach();
            // delete amla
            $amla->delete();
            $error = null;

        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $amla,
            'error' =>  $error,
        ]);
    }
}
