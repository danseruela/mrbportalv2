<?php

namespace App\Http\Controllers;

use App\BloodType;
use Illuminate\Http\Request;
use Exception;

class BloodTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => BloodType::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blood_type       = new BloodType;
        $blood_type->desc = $request->desc;
        $blood_type->save();

        return response()->json([
            'data'  =>  $blood_type
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blood_type = BloodType::findOrFail($id);
        $blood_type->desc = $request->desc;
        $blood_type->save();

        return response()->json([
            'data'  =>  $blood_type
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $blood_type = BloodType::find($id);
            $blood_type->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $blood_type,
            'error' =>  $error,
        ]);
    }
}
