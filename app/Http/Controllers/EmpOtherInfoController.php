<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Exception;

class EmpOtherInfoController extends Controller
{
    /**
     * Return object personal ids
     * 
     */
    public function getIDs($emp_id) 
    {
        return response()->json([
            'data'  =>  DB::table('personal_ids')->select('personal_ids.*', 'id_type.desc AS id_type')
                        ->leftJoin('id_type', 'id_type.id', '=', 'personal_ids.type')
                        ->where('emp_id', $emp_id)
                        ->get()
        ]);
    }
    /**
     * Retrieve ID Details
     * 
     */
    public function showID($id)
    {
        return response()->json([
            'data'  =>  DB::table('personal_ids')->select('personal_ids.*')
                        ->where('id', $id)
                        ->first()
        ]);
    }
    /**
     * Store ID Details
     * 
     */
    public function saveID(Request $request)
    {
        return response()->json([
            'data'  =>  DB::table('personal_ids')->insert([
                            'emp_id'        =>  $request->emp_id,
                            'type'          =>  $request->type,
                            'id_no'         =>  $request->id_no,
                            'date_issued'   =>  $request->date_issued,
                            'date_expiry'   =>  $request->date_expiry,
                        ])
        ]);
    }
    /**
     * Update ID Details
     * 
     */
    public function updateID(Request $request, $id)
    {
        return response()->json([
            'data'  =>  DB::table('personal_ids')->where('id', $id)->update([
                            'type'          =>  $request->type,
                            'id_no'         =>  $request->id_no,
                            'date_issued'   =>  $request->date_issued,
                            'date_expiry'   =>  $request->date_expiry,
                        ])
        ]);
    }
    /**
     * Delete ID
     * 
     */
    public function removeID($id)
    {
        try {
            $personal_id = DB::table('personal_ids')->where('id', $id);
            $personal_id->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $personal_id,
            'error' =>  $error,
        ]);
    }

    /**
     * Return object educational attainment
     * 
     */
    public function getEducations($emp_id) 
    {
        return response()->json([
            'data'  =>  DB::table('educational_attainment')->select('educational_attainment.*', 'education_level.desc AS educ_level')
                        ->leftJoin('education_level', 'education_level.id', '=', 'educational_attainment.level')
                        ->where('emp_id', $emp_id)
                        ->get()
        ]);
    }
    /**
     * Retrieve Education Details
     * 
     */
    public function showEducation($id)
    {
        return response()->json([
            'data'  =>  DB::table('educational_attainment')->select('educational_attainment.*')
                        ->where('id', $id)
                        ->first()
        ]);
    }
    /**
     * Store Education Details
     * 
     */
    public function saveEducation(Request $request)
    {
        return response()->json([
            'data'  =>  DB::table('educational_attainment')->insert([
                            'emp_id'       =>  $request->emp_id,
                            'level'        =>  $request->level,
                            'school_name'  =>  $request->school_name,
                            'year_start'   =>  $request->year_start,
                            'year_end'     =>  $request->year_end,
                            'degree'       =>  $request->degree,
                        ])
        ]);
    }
    /**
     * Update Education Details
     * 
     */
    public function updateEducation(Request $request, $id)
    {
        return response()->json([
            'data'  =>  DB::table('educational_attainment')->where('id', $id)->update([
                            'level'        =>  $request->level,
                            'school_name'  =>  $request->school_name,
                            'year_start'   =>  $request->year_start,
                            'year_end'     =>  $request->year_end,
                            'degree'       =>  $request->degree,
                        ])
        ]);
    }
    /**
     * Delete Education
     * 
     */
    public function removeEducation($id)
    {
        try {
            $education = DB::table('educational_attainment')->where('id', $id);
            $education->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $education,
            'error' =>  $error,
        ]);
    }

    /**
     * Return object employment history
     * 
     */
    public function getEmpHistories($emp_id) 
    {
        return response()->json([
            'data'  =>  DB::table('employment_history')->select('employment_history.*')
                        ->where('emp_id', $emp_id)
                        ->get()
        ]);
    }
    /**
     * Retrieve Employment History Details
     * 
     */
    public function showEmpHistory($id)
    {
        return response()->json([
            'data'  =>  DB::table('employment_history')->select('employment_history.*')
                        ->where('id', $id)
                        ->first()
        ]);
    }
    /**
     * Store Employment History Details
     * 
     */
    public function saveEmpHistory(Request $request)
    {
        return response()->json([
            'data'  =>  DB::table('employment_history')->insert([
                            'emp_id'            =>  $request->emp_id,
                            'employer_name'     =>  $request->employer_name,
                            'employer_address'  =>  $request->employer_address,
                            'designation'       =>  $request->designation,
                            'employment_id_no'  =>  $request->employment_id_no,
                            'date_start'        =>  $request->date_start,
                            'date_end'          =>  $request->date_end,
                            'tel_no'            =>  $request->tel_no,
                            'email'             =>  $request->email,
                        ])
        ]);
    }
    /**
     * Update Employment History Details
     * 
     */
    public function updateEmpHistory(Request $request, $id)
    {
        return response()->json([
            'data'  =>  DB::table('employment_history')->where('id', $id)->update([
                            'employer_name'     =>  $request->employer_name,
                            'employer_address'  =>  $request->employer_address,
                            'designation'       =>  $request->designation,
                            'employment_id_no'  =>  $request->employment_id_no,
                            'date_start'        =>  $request->date_start,
                            'date_end'          =>  $request->date_end,
                            'tel_no'            =>  $request->tel_no,
                            'email'             =>  $request->email,
                        ])
        ]);
    }
    /**
     * Delete Employment History
     * 
     */
    public function removeEmpHistory($id)
    {
        try {
            $emp_history = DB::table('employment_history')->where('id', $id);
            $emp_history->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $emp_history,
            'error' =>  $error,
        ]);
    }

}
