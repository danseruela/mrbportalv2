<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobCat;
use DB;

class JobCategoryController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    protected $user;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_cat_list')->with([ 
                'job_categories' => JobCat::all() 
            ]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_cat_create')->with([
                'action'        =>  'create',
                'available_id'  =>  DB::table('job_cat')->max('id') + 1,
            ]);
        } else {
            
            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Store
        $job_category = JobCat::create([
            'id'        =>  $request->input('job_cat_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        // flash message - created job category successfully.
        $request->session()->flash('message', 'Successfully created Job Category!');
        
        // redirect
        return redirect('job/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job_category = JobCat::find($id);

        return view('job_cat_create')->with([
            'job_category'  =>  $job_category,
            'action'        =>  'edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        JobCat::where('id', '=', $id )
        ->update([
            'id'        =>  $request->input('job_cat_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        
        // flash message - updated job category successfully.
        $request->session()->flash('message', 'Successfully updated Job Category!');
        
        // redirect
        return redirect('job/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
