<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\QuickEmailMail;
use Auth;

class QuickEmailController extends Controller
{
    /**
     * Send Quick Email
     */
    public function send(Request $request)
    {
        $data = request()->validate([
            'emailto'   =>  'required|email',
            'subject'   =>  'required',
            'message'   =>  'required'
        ]);

        $data = $request->merge(['sender' => Auth::user()->employee->firstname . ' ' . Auth::user()->employee->lastname]);

        Mail::to($request['emailto'])->send(new QuickEmailMail($data));
        
        return redirect('dashboard')->with('message', 'Message successfully sent.');
    }
}
