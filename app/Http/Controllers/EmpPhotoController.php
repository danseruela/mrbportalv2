<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\Storage;
use Response;
use Validator;
use Auth;
use App\Employee;
use Storage;

class EmpPhotoController extends Controller
{
    /**
     * Store employee photo.
     */
    public function empImage(Request $request)
    {
    	$id = $request->id;
        $rules = ['image' => 'image|mimes:jpeg,png,jpg,gif,svg'];
        $messages = ['image.mimes' => 'Only jpg, jpeg, png, gif, svg files are accepted.'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->getMessageBag()->toArray()]);
        } else {

            if (Input::hasFile('image')) {
            	// Delete old photo.
            	$old_photo = Employee::where('id', $id)->whereNotNull('emp_photo')->pluck('emp_photo')->all();
                Storage::delete($old_photo);

                $file = $request->file('image');
                $filename = date('Ymdhis') . '.' . $file->getClientOriginalExtension();
                //$file->move(public_path('storage'), $filename);
                $path = Storage::putFileAs('public', $file, $filename);
                    
                $image = Employee::findOrFail($id);
                $image->emp_photo   = $filename;
                $image->modified_at = date('Y-m-d');
                $image->modified_by = Auth::id();
                $image->save();
             } else {
                $filename = 'No image uploaded!';
            }
            
        return response()->json(['image' => $filename, 'old' => $old_photo]);
        }
    }
}
