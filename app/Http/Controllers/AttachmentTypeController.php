<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AttachmentType;
use App\Customer;
use App\User;
use Spatie\MediaLibrary\Media;
use Exception;
use DB;
use Hash;
use Auth;

class AttachmentTypeController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  AttachmentType::select(
                        'attachment_type.*',
                        'attachment_category.desc AS category')
                        ->leftJoin('attachment_category', 'attachment_category.id', '=', 'attachment_type.cat_id')
                        ->get(),
        ]);
    }

    /**
     * Display a listing of the resource per customer
     *
     * @return \Illuminate\Http\Response
     */
    public function byCustomer($cid, $category)
    {
        return response()->json([
            'data'  =>  AttachmentType::select(
                        'attachment_type.*',
                        'attachment_category.desc AS category',
                        DB::raw("(SELECT COUNT(id) FROM media WHERE model_id = attachment_type.id AND collection_name = '" . $cid . "') AS total_files"))
                        ->leftJoin('attachment_category', 'attachment_category.id', '=', 'attachment_type.cat_id')
                        ->where('cat_id', $category)
                        ->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attachment_type            = new AttachmentType;
        $attachment_type->cat_id    = $request->cat_id;
        $attachment_type->desc      = $request->desc;
        $attachment_type->save();

        return response()->json([
            'data'  =>  $attachment_type
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'data'  =>  AttachmentType::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attachment_type            = AttachmentType::findOrFail($id);
        $attachment_type->cat_id    = $request->cat_id;
        $attachment_type->desc      = $request->desc;
        $attachment_type->save();

        return response()->json([
            'data'  =>  $attachment_type
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $attachment_type = AttachmentType::find($id);
            $attachment_type->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $attachment_type,
            'error' =>  $error,
        ]);
    }

    /**
     * Upload Files - Customer Attachments
     * 
     */
    public function upload(Request $request)
    {
        try {
            // $collection_name = 'customer_files_' . $request->file_cid;
            $collection_name = $request->file_cid;
            $uploadFiles = AttachmentType::find($request->file_id);
            $uploadFiles->addMedia($request->file)->toMediaCollection($collection_name);
            $error = null;

        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  => $uploadFiles,
            'error' =>  $error,
        ]);
    }

    /**
     * Retrieve Files - Customer Attachments
     * 
     */
    public function view($id, $file_cid)
    {
        // $collection_name = 'customer_files_' . $file_cid;
        $collection_name = $file_cid;
        $files = AttachmentType::find($id);
        //$files->getMedia($collection_name);

        $files = Media::select(
                        'media.id', 
                        'media.file_name', 
                        'attachment_category.desc AS category',
                        DB::raw("CONCAT(ROUND(media.size/1048576, 2), ' mb') AS file_size"))
                       ->leftJoin('attachment_type', 'attachment_type.id', '=', 'media.model_id')
                       ->leftJoin('attachment_category', 'attachment_category.id', '=', 'attachment_type.cat_id')
                       ->where('media.collection_name', '=', $collection_name)
                       ->where('media.model_id', '=', $id)
                       ->get(); 

        return response()->json([
            'data'            => $files,
            'collection_name' => $collection_name,
        ]);
    }

    /**
     * Remove Files - Customer Attachments
     * 
     */ 
    public function remove($id)
    {
        $file = Media::find($id);
        $file->forceDelete();

        return response()->json([
            'data' => $file,
        ]);
    }
    /**
     * Download attachment
     * 
     */
    public function download($id)
    {
        $downloadFile = Media::find($id);

        return response()->download($downloadFile->getPath(), $downloadFile->file_name);
    }
    /**
     * Customer Photo
     *  
     */
    public function customerPhoto($cid, $mid)
    {
        $photo = Media::where('collection_name', $cid)
                      ->where('model_id', $mid)
                      ->orderBy('created_at', 'desc')
                      ->first();

        return response()->json([
            'data'  =>  $photo,
        ]);
    }

    /**
     * Report - List of Customers already have attachments.
     * 
     */
    public function rptCustomerAttachments($date_from, $date_to)
    {
        // Using icbs - connection on attachment_type table
        $media = Media::select(
                        'media.collection_name', 
                        DB::raw("(SELECT CASE WHEN COUNT(m.id) = 0 THEN 0 ELSE COUNT(m.id) END FROM media AS m LEFT JOIN attachment_type AS at ON at.id=m.model_id LEFT JOIN attachment_category AS ac ON ac.id=at.cat_id WHERE m.collection_name=media.collection_name AND ac.id=1) AS actual_cif_files"), // actual attachments
                        DB::raw("(SELECT CASE WHEN COUNT(m.id) = 0 THEN 0 ELSE 1 END FROM media AS m LEFT JOIN attachment_type AS at ON at.id=m.model_id LEFT JOIN attachment_category AS ac ON ac.id=at.cat_id WHERE m.collection_name=media.collection_name AND ac.id=1) AS cif_files"),
                        DB::raw("(SELECT CASE WHEN COUNT(m.id) = 0 THEN 0 ELSE COUNT(m.id) END FROM media AS m LEFT JOIN attachment_type AS at ON at.id=m.model_id LEFT JOIN attachment_category AS ac ON ac.id=at.cat_id WHERE m.collection_name=media.collection_name AND ac.id=2) AS loan_files"),
                        DB::raw("(SELECT CASE WHEN COUNT(m.id) = 0 THEN 0 ELSE COUNT(m.id) END FROM media AS m LEFT JOIN attachment_type AS at ON at.id=m.model_id LEFT JOIN attachment_category AS ac ON ac.id=at.cat_id WHERE m.collection_name=media.collection_name AND ac.id=3) AS deposit_files"),
                        // DB::raw("(SELECT COUNT(m.id) FROM media AS m WHERE m.collection_name=media.collection_name) AS uploaded_files")) // old script
                        
                        // Re-count attachments with CIF attachments counted as 1
                        DB::raw("(SELECT ((SELECT CASE WHEN COUNT(m.id) = 0 THEN 0 ELSE 1 END FROM media AS m LEFT JOIN attachment_type AS at ON at.id=m.model_id LEFT JOIN attachment_category AS ac ON ac.id=at.cat_id WHERE m.collection_name=media.collection_name AND ac.id=1) +
                                          (SELECT CASE WHEN COUNT(m.id) = 0 THEN 0 ELSE COUNT(m.id) END FROM media AS m LEFT JOIN attachment_type AS at ON at.id=m.model_id LEFT JOIN attachment_category AS ac ON ac.id=at.cat_id WHERE m.collection_name=media.collection_name AND ac.id=2) + 
                                          (SELECT CASE WHEN COUNT(m.id) = 0 THEN 0 ELSE COUNT(m.id) END FROM media AS m LEFT JOIN attachment_type AS at ON at.id=m.model_id LEFT JOIN attachment_category AS ac ON ac.id=at.cat_id WHERE m.collection_name=media.collection_name AND ac.id=3))) AS uploaded_files"))
                        ->whereBetween('media.created_at', [$date_from, $date_to])
                        ->groupBy('media.collection_name')
                        ->get();
        
        $records = Media::select('collection_name')->whereBetween('media.created_at', [$date_from, $date_to])->groupBy('collection_name')->count();
        
        // Extract customers ids in collection_name
        foreach ($media as $m) {
            // $temp = explode('_', $m->collection_name);
            // $customer_ids[] = $temp[2];
            $customer_ids[] = $m->collection_name;
        }

        $customers = Customer::select('customer.id', 'branch.name AS branch', 'customer.customer_id', 'customer.display_name')
                            ->leftJoin('branch', 'branch.id', '=', 'customer.branch_id')
                            ->whereIn('customer.id', !empty($customer_ids) ? $customer_ids : [])
                            ->get();

        // Construct customers with uploaded_files
        foreach ($customers as $customer) {
            foreach ($media as $m) {
                // $temp = explode('_', $m->collection_name);
                // if($temp[2] == $customer->id) {
                if($m->collection_name == $customer->id) {
                    $customer_data[] = array(
                        'id'                =>  $customer->id,
                        'branch'            =>  $customer->branch,
                        'customer_id'       =>  $customer->customer_id,
                        'display_name'      =>  $customer->display_name,
                        'actual_cif_files'  =>  $m->actual_cif_files,
                        'cif_files'         =>  $m->cif_files,
                        'loan_files'        =>  $m->loan_files,
                        'deposit_files'     =>  $m->deposit_files,
                        'uploaded_files'    =>  $m->uploaded_files,
                    );
                }   
            }
        }

        return response()->json([
            'draw'              => 3,
            'recordsTotal'      => $records,
            'recordsFiltered'   => $records,
            'data'              => !empty($customer_data) ? $customer_data : [],
        ]);
    }
    // FUNCTION TO BE DELETED
    public function rptBranchAttachments()
    {
        // Using icbs - connection on attachment_type table
        $media = Media::select('media.collection_name', DB::raw("COUNT(media.id) AS uploaded_files"))
                        ->groupBy('media.collection_name')
                        ->get();
        
        // Extract customers ids in collection_name
        foreach ($media as $m) {
            $customer_ids[] = $m->collection_name;
        }

        $customers = Customer::select('customer.id', 'branch.name AS branch', 'customer.customer_id', 'customer.display_name')
                            ->leftJoin('branch', 'branch.id', '=', 'customer.branch_id')
                            ->whereIn('customer.id', !empty($customer_ids) ? $customer_ids : [])
                            ->get();

        // Construct customers with uploaded_files
        foreach ($customers as $customer) {
            foreach ($media as $m) {
                // $temp = explode('_', $m->collection_name);
                // if($temp[2] == $customer->id) {
                if($m->collection_name == $customer->id) {
                    $customer_data[] = array(
                        'id'             =>  $customer->id,
                        'branch'         =>  $customer->branch,
                        'customer_id'    =>  $customer->customer_id,
                        'display_name'   =>  $customer->display_name,
                        'uploaded_files' =>  $m->uploaded_files,
                    );
                }   
            }
        }

        return response()->json([
            'data'  => !empty($customer_data) ? $customer_data : [],
        ]);
    }
    // END OF FUNCTION TO BE DELETED

    /**
     * Override request
     */
    public function override(Request $request)
    {
        $message = 'Override request successful!.';
        $status = 'Success';
        $overrideuser = User::where('username', trim($request->username))->first();

        // check override username exist
        if(is_null($overrideuser)) {
            $message = 'Username does not exist!';
            $status = 'Error';
        }
        // check override username and password match
        if($overrideuser['username'] != trim($request->username) || !Hash::check(trim($request->password), $overrideuser['password'])) {
            $message = 'Username/Password does not match.';
            $status = 'Error';
        }
        // check user has override access
        if($overrideuser['username'] == trim($request->username) && Hash::check(trim($request->password), $overrideuser['password']) && 
         !$overrideuser->hasAnyRole(['Administrator', 'ICBS - Manager'])) {
            $message = 'You are not autorized to override.';
            $status = 'Error';
        }
        // check override user branch == customer's branch
        if($overrideuser['username'] == trim($request->username) && Hash::check(trim($request->password), $overrideuser['password']) && 
           $overrideuser->hasAnyRole(['Administrator', 'ICBS - Manager']) &&
           $overrideuser->employee->getLocation->id != $request->customerBrID) {
            $message = 'You are not authorized on this branch.';
            $status = 'Error';
        }
        // check override user is Administrator
        if($overrideuser['username'] == trim($request->username) && Hash::check(trim($request->password), $overrideuser['password']) && 
           $overrideuser->hasRole('Administrator')) {
            $message = 'Override request successful!.';
            $status = 'Success';
        }

        return response()->json([
            'status'        =>  $status,
            'message'       =>  $message,
            'overrideuser'  =>  $overrideuser,
        ]);
    }

}
