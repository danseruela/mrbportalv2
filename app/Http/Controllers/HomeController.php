<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Hash;

class HomeController extends Controller
{
	// validate the info, create rules for the inputs
	protected $rules = [
	    'username'  => 'required', // make sure the username is an actual username
	    'password' 	=> 'required|min:3' // password has to be greater than 3 characters
	];

    // show login form
    public function showLogin() 
    {
    	if (auth()->user()) {
    		return redirect('dashboard');
    	} else {
    		return view('login');
    	}
    }

    // process login
    public function doLogin(Request $request) 
    {
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $this->rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
		    return redirect('/')
		        ->withErrors($validator) // send back all errors to the login form
		        ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
		    // attempt to do the login
		    if (Auth::attempt([ 
					'username' 	=> $request->username, 
					'password' 	=> trim($request->password), 
					'deleted'	=> 0 
			], $request->remember_token)) {

		        // validation successful!
		        // redirect them to the secure section or whatever
		        // return redirect('secure');
		        // for now we'll just echo success (even though echoing in a controller is bad)
		        return redirect()->intended('dashboard');

		    } else {        
		    	
		        // validation not successful, send back to form 
		        return back()->withErrors([
		        	'message' => 'Username or Password does not match.'
		        ]);

		    }

		}

    }

    public function doLogout()
    {
    	Auth::logout(); // log the user out of our application
    	return redirect('/'); // redirect the user to the login screen
    }

}
