<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\UserRole;
use App\Employee;
use App\Notifications\UpdateUserRole;
use App\Notifications\UpdatePassword;
use Response;
use Validator;
use Auth;
use Hash;
use DB;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $rules = 
    [
        'username'          =>  'required|string|max:20|unique:users',
        'password'          =>  'required|string|min:6',
        'emp_id'            =>  'required|unique:users',
        'user_roles'        =>  'required',
        'status'            =>  'required',
    ];

    protected $messages = 
    [
        'emp_id.unique'     =>  'Selected employee already have assigned user!',
    ];


    protected $user;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = auth()->user();

        if ($this->user->can('admin-only', $this->user)) {
            return view('user_list')->with([ 
                'users'         =>  User::all(), 
                'employees'     =>  Employee::select(DB::raw("CONCAT(lastname, ', ', firstname, ' ', middlename) as fullname"), 'id')
                                    ->orderby('fullname')
                                    ->get()
                                    ->pluck('fullname', 'id'),
                'roles'         =>  UserRole::pluck('name', 'id'),
            ]);
        } else {
            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = Auth::user();

        if ($this->user->can('admin-only', $this->user)) {

            return view('user_create')->with([
                'action'        =>  'create',
                'available_id'  =>  User::max('id') + 1,
                'employees'     =>  Employee::leftJoin('users', 'users.emp_id', '=', 'employees.id')
                                    ->select(DB::raw("CONCAT(employees.lastname, ', ', employees.firstname, ' ', employees.middlename) as fullname"), 'employees.id')
                                    ->where('users.emp_id', '=', null)
                                    ->orderby('fullname')
                                    ->get()
                                    ->pluck('fullname', 'id'),
                'roles'         =>  UserRole::pluck('name', 'id'),
            ]);
        } else {
            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store
        $validator = Validator::make(Input::all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
            //return redirect('user/create')->with(['errors' => $validator->getMessageBag()->toArray()])->withInput();
        } else {
            // Store
            $user = new User;
            $user->emp_id       =  $request->emp_id;
            //$user->user_role_id =  $request->user_role_id;
            $user->username         =  $request->username;
            $user->password         =  bcrypt($request->password);
            $user->deleted          =  $request->status;
            $user->is_first_login   =  1;
            $user->created_at       =  date('Y-m-d h:i:s');
            $user->created_by       =  Auth::id();
            $user->save();

            // Remove existing roles.
            $user->roles()->detach();

            // Check if user has multiple roles
            if (is_array($request->user_roles)) {
                $roles = $request->user_roles;
                $num = count($roles);
                                
                foreach ($roles as $role) {
                    // Add new roles to user.
                    $user->roles()->attach(UserRole::where('id', $role)->first());    
                }
                
            }

            // flash message - updated employee successfully.
            $request->session()->flash('message', 'Successfully added user!');
            
            return redirect('user');   
            
            //return response()->json($user);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('user_create')->with([
            'user'          =>  $user,
            'action'        =>  'edit',
            'employees'     =>  Employee::select(DB::raw("CONCAT(lastname, ', ', firstname, ' ', middlename) as fullname"), 'id')
                                ->orderby('fullname')
                                ->get()
                                ->pluck('fullname', 'id'),
            'roles'         =>  UserRole::pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        $user = User::findOrFail($id);
        $user->emp_id = $request->emp_id;
        //$user->user_role_id = $request->user_role_id;
        $user->username = $request->username;
        $user->deleted = $request->status;
        $user->modified_at = date('Y-m-d h:i:s');
        $user->modified_by = Auth::id();
        $user->save();

        // Remove existing roles.
        $user->roles()->detach();

        // Check if user has multiple roles
        if (is_array($request->user_roles)) {
            $roles = $request->user_roles;
            $num = count($roles);
                                
            foreach ($roles as $role) {
                // Add new roles to user.
                $user->roles()->attach(UserRole::where('id', $role)->first());    
            }
                
        }

        $related = [
            'getEmployee'   =>  Employee::select(DB::raw("CONCAT(employees.lastname, ', ', employees.firstname, ' ', employees.middlename) as fullname"), 'employees.id')
                                ->where('id', $request->emp_id)
                                ->pluck('fullname')
                                ->all(),
            //'getRole'       =>  UserRole::where('id', $request->user_role_id)->pluck('name')->all(),
            'getRole'       =>  DB::table('users_roles')
                                ->join('users_role', 'users_roles.role_id', '=', 'users_role.id')
                                ->where('user_id', $id)->get([
                                    'role_id as id', 
                                    'users_role.name as desc'
                                ])->toArray(),
            'getStatus'     =>  $request->status == 0 ? 'Active' : 'Inactive',
            'isOnline'      =>  $user->isOnline(),
        ];

        // create notification for updated user role - DB only.
        $user->notify(new UpdateUserRole($related['getRole']));

        // flash message - updated employee successfully.
        //$request->session()->flash('message', 'Successfully added user!');
               
        return response()->json([
            'user'      =>  $user, 
            'related'   =>  $related,
            'branch'    =>  $user->employee->getLocation->desc, 
        ]);            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json($user);
    }

    /**
     * Reset user password.
     */
    public function resetPassword(Request $request, $id) 
    {
        // reset password
        $user = User::findOrFail($id);
        $user->password = bcrypt($request->password);
        $user->is_first_login = 1;
        $user->modified_at = date('Y-m-d h:i:s');
        $user->modified_by = Auth::id();
        $user->save();

        // create notification for updated password - DB only.
        $user->notify(new UpdatePassword($user));
               
        return response()->json(['user' => $user, 'message' => 'Successfully Reset User Password!']);
    }

    /**
     * Update user password from user profile.
     */
    public function updatePassword(Request $request, $id)
    {
        $user = User::findOrFail($id);

        /*$validator = Validator::make($request, [
                'password'      =>  'replace_with:new_password|password|min:6',
                'new_password'  =>  'confirmed|min:6',
        ]);*/

        // if validator fails

        // Check current password is correct.
        if (Hash::check($request->password, $user->password)) {
            $user->is_first_login = 0;
            $user->fill([
                'password'  =>  bcrypt($request->new_password)
            ])->save();

            return response()->json(['user' => $user, 'message' => 'Successfully Updated User Password!']);

        } else {
            return response()->json(['error' => true, 'message' => 'Current Password does not match!']);
        }
    }


    public function getRoles($id) 
    {
        $roles = DB::table('users_roles')
         ->join('users_role', 'users_roles.role_id', '=', 'users_role.id')
         ->where('user_id', $id)->get([
            'role_id as id', 
            //'users_role.name as rolename'
        ]);

            foreach ($roles as $role) {
                $role_val[] = $role->id; 
            }
       
        return $role_val;
    }

}
