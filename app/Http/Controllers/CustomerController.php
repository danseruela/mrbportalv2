<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  Customer::select(
                        'customer.id',
                        'customer.customer_id',
                        'branch.name AS branch',
                        'customer.display_name AS customer_name',
                        'customer_type.description AS customer_type',
                        'customer.birth_date',
                        'customer_status.description AS status',
                        'createdby.username AS created_by',
                        'updatedby.username AS updated_by'
                        )
                        ->leftJoin('branch', 'branch.id', '=', 'customer.branch_id')
                        ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.type_id')
                        ->leftJoin('customer_status', 'customer_status.id', '=', 'customer.status_id')
                        ->leftJoin('user_master AS createdby', 'createdby.id', '=', 'customer.created_by_id')
                        ->leftJoin('user_master AS updatedby', 'updatedby.id', '=', 'customer.last_updated_by_id')
                        ->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::select(
                    'customer.id',
                    'customer.customer_id',
                    'customer.branch_id',
                    'branch.name AS branch',
                    'title.item_value as title',
                    'customer.display_name AS customer_name',
                    'customer.name1 AS firstname',
                    'customer.name2 AS middlename',
                    'customer.name3 AS lastname',
                    'customer.name4 AS initials',
                    'customer_type.description AS customer_type',
                    'customer_group.name AS customer_group',
                    'customer_dosri_code.description AS dosri',
                    'customer.birth_date',
                    'customer.birth_place',
                    'gender.description AS gender',
                    'civil.item_value AS civil_status',
                    'customer_status.description AS status',
                    'nationality.item_value AS nationality',
                    'customer.pep_description',
                    'customer.passport_no',
                    'customer.remarks',
                    'customer.short_address',
                    'customer.source_of_income',
                    'customer.sss_no',
                    'customer.tin_no',
                    'createdby.username AS created_by',
                    'updatedby.username AS updated_by'
                    )
                    ->leftJoin('branch', 'branch.id', '=', 'customer.branch_id')
                    ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.type_id')
                    ->leftJoin('customer_status', 'customer_status.id', '=', 'customer.status_id')
                    ->leftJoin('customer_group', 'customer_group.id', '=', 'customer.group_id')
                    ->leftJoin('customer_dosri_code', 'customer_dosri_code.id', '=', 'customer.dosri_code_id')
                    ->leftJoin('gender', 'gender.id', '=', 'customer.gender_id')
                    ->leftJoin('lov AS civil', 'civil.id', '=', 'customer.civil_status_id')
                    ->leftJoin('lov AS title', 'title.id', '=', 'customer.title_id')
                    ->leftJoin('lov AS nationality', 'nationality.id', '=', 'customer.nationality_id')
                    ->leftJoin('user_master AS createdby', 'createdby.id', '=', 'customer.created_by_id')
                    ->leftJoin('user_master AS updatedby', 'updatedby.id', '=', 'customer.last_updated_by_id')
                    ->where('customer.id', '=', $id)
                    ->first();

            $address = $customer->address()->where('customer_id', $id)->with(['type', 'town'])->get();
            $contact = $customer->contact()->where('customer_id', $id)->with(['type'])->get();
            $education = $customer->education()->where('customer_id', $id)->with(['type'])->get();
            $employment = $customer->employment()->where('customer_id', $id)->with(['region'])->get();
            $business = $customer->business()->where('customer_id', $id)->with(['project'])->get();
            $presented_id = $customer->presentedId()->where('customer_id', $id)->with(['type', 'status'])->get();
            $relation = $customer->relationIcbs()->where('customer_id', $id)->with(['type', 'status', 'relativeInfo'])->get();

        return response()->json([
            'data'          =>  $customer,
            'address'       =>  $address,    
            'contact'       =>  $contact,
            'education'     =>  $education,
            'employment'    =>  $employment,
            'business'      =>  $business,
            'presented_id'  =>  $presented_id,    
            'relation'      =>  $relation,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
