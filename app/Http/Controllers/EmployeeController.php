<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use Response;
use Validator;
use Auth;
use DB;
use App\ContactType;
use App\ContactCat;
use App\Organization;
use App\Location;
use App\JobTitle;
use App\JobCat;
use App\JobLevel;
use App\EmpStatus;
use App\Country;
use App\CivilStatus;
use App\Employee;
use App\Gender;
use App\Contacts;
use App\RelType;
use App\RelConsanguinity;
use App\Relations;
use App\EmpBasicSalary;
use App\SalComponentType;
use App\PayGrade;
use App\PayFrequency;
use App\BloodType;


class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        
        if ($user->can('generateReport', Employee::class)) {
            if ($user->hasRole('Administrator')) {
                $employees = Employee::all();
            } else {
                $employees = Employee::where('emp_no', 'NOT LIKE', '%SysAd%')->get();
            }
            return view('employee_list')->with([ 'employees' => $employees ]);
        } else {
            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        
        if ($user->can('create', Employee::class)) {
            return view('employee_create')->with([
                'action'        =>  'create',
                'contact_type'  =>  ContactType::all(), 
                'contact_cat'   =>  ContactCat::all(), 
                'emp_status'    =>  EmpStatus::pluck('desc', 'id'), 
                'organizations' =>  Organization::pluck('desc', 'id'),
                'locations'     =>  Location::pluck('desc', 'id'),
                'job_title'     =>  JobTitle::pluck('desc', 'id'),
                'job_cat'       =>  JobCat::pluck('desc', 'id'),
                'job_level'     =>  JobLevel::pluck('desc', 'id'),
                'countries'     =>  Country::pluck('desc', 'id'),
                'civil_status'  =>  CivilStatus::pluck('desc', 'id'),
                'blood_type'    =>  BloodType::pluck('desc', 'id'),
                'gender'        =>  Gender::all(),
                'pay_grade'     =>  PayGrade::pluck('desc', 'id'),
            ]);
        } else {
            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {

        // Store
        $employee = Employee::create([
            'emp_no'        =>  $request->input('emp_no'),
            'lastname'      =>  $request->input('lastname'),
            'firstname'     =>  $request->input('firstname'),
            'middlename'    =>  $request->input('middlename'),
            'nickname'      =>  $request->input('nickname'),
            'birthdate'     =>  $request->input('birthdate'),
            'gender'        =>  $request->input('gender'),
            'blood_type'    =>  $request->input('blood_type'),
            'civil_status'  =>  $request->input('civil_status'),
            'emp_status'    =>  $request->input('emp_status'),
            'emp_start_date'=>  $request->input('emp_start_date'),
            'emp_end_date'  =>  $request->input('emp_end_date'),
            'org_code'      =>  $request->input('org_code'),
            'location_code' =>  $request->input('location_code'),
            'job_title'     =>  $request->input('job_title'),
            'job_cat'       =>  $request->input('job_cat'),
            'job_level'     =>  $request->input('job_level'),
            'job_specifics' =>  $request->input('job_specifics'),
            'address1'      =>  $request->input('address1'),
            'address2'      =>  $request->input('address2'),
            'city'          =>  $request->input('city'),
            'province'      =>  $request->input('province'),
            'postal_code'   =>  $request->input('postal_code'),
            'country_id'    =>  $request->input('country'),
            'created_at'    =>  date('Y-m-d h:i:s'),
            'created_by'    =>  Auth::id(),
            'modified_at'   =>  date('Y-m-d h:i:s'),
            'modified_by'   =>  Auth::id()
        ]);
        
        // flash message - created employee successfully.
        $request->session()->flash('message', 'Successfully created employee!');
        
        // redirect
        return redirect('employee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $employee = Employee::find($id);

        if ($user->can('view', $employee)) {

            return view('employee_create')->with([
                'employee'          =>  $employee,
                'isEnabled'         =>  'disabled',
                'action'            =>  'edit',
                'contact_type'      =>  ContactType::pluck('desc', 'id'), 
                'contact_cat'       =>  ContactCat::pluck('desc', 'id'), 
                'emp_status'        =>  EmpStatus::pluck('desc', 'id'), 
                'organizations'     =>  Organization::pluck('desc', 'id'),
                'locations'         =>  Location::pluck('desc', 'id'),
                'job_title'         =>  JobTitle::pluck('desc', 'id'),
                'job_cat'           =>  JobCat::pluck('desc', 'id'),
                'job_level'         =>  JobLevel::pluck('desc', 'id'),
                'countries'         =>  Country::pluck('desc', 'id'),
                'civil_status'      =>  CivilStatus::pluck('desc', 'id'),
                'gender'            =>  Gender::all(),
                'blood_type'        =>  BloodType::pluck('desc', 'id'),
                'contacts'          =>  Contacts::where('emp_id', '=', $id)->get(),
                'relations'         =>  Relations::where('emp_id', '=', $id)->get(),
                'relation_types'    =>  RelType::pluck('desc', 'id'),
                'consanguinities'   =>  RelConsanguinity::pluck('desc1', 'id'),
                'salaries'          =>  EmpBasicSalary::where('emp_code', '=', $id)->get(),
                'sal_component_type'=>  SalComponentType::pluck('desc', 'id'),
                'pay_grade'         =>  PayGrade::pluck('desc', 'id'),
                'pay_frequency'     =>  PayFrequency::pluck('desc', 'id'),
                'emp_relations'     =>  Employee::select(DB::raw("CONCAT(lastname, ', ', firstname, ' ', middlename) as fullname"), 'id')
                                        ->orderby('fullname')
                                        ->get()
                                        ->pluck('fullname', 'id'),
            ]);
        } else {
            return view('error')->with([
                'error'       =>  '404',
                'message'     =>  'Page Not Found',
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $employee = Employee::find($id);

        return view('employee_create')->with([
            'employee'          =>  $employee,
            'action'            =>  'edit',
            'contact_type'      =>  ContactType::pluck('desc', 'id'), 
            'contact_cat'       =>  ContactCat::pluck('desc', 'id'), 
            'emp_status'        =>  EmpStatus::pluck('desc', 'id'), 
            'organizations'     =>  Organization::pluck('desc', 'id'),
            'locations'         =>  Location::pluck('desc', 'id'),
            'job_title'         =>  JobTitle::pluck('desc', 'id'),
            'job_cat'           =>  JobCat::pluck('desc', 'id'),
            'job_level'         =>  JobLevel::pluck('desc', 'id'),
            'countries'         =>  Country::pluck('desc', 'id'),
            'civil_status'      =>  CivilStatus::pluck('desc', 'id'),
            'blood_type'        =>  BloodType::pluck('desc', 'id'),
            'gender'            =>  Gender::all(),
            'contacts'          =>  Contacts::where('emp_id', '=', $id)->get(),
            'relations'         =>  Relations::where('emp_id', '=', $id)->get(),
            'relation_types'    =>  RelType::pluck('desc', 'id'),
            'consanguinities'   =>  RelConsanguinity::pluck('desc1', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        // Update
        Employee::where('id', '=', $id )
        ->update([
            'emp_no'        =>  $request->input('emp_no'),
            'lastname'      =>  $request->input('lastname'),
            'firstname'     =>  $request->input('firstname'),
            'middlename'    =>  $request->input('middlename'),
            'nickname'      =>  $request->input('nickname'),
            'birthdate'     =>  $request->input('birthdate'),
            'gender'        =>  $request->input('gender'),
            'civil_status'  =>  $request->input('civil_status'),
            'blood_type'    =>  $request->input('blood_type'),
            'emp_status'    =>  $request->input('emp_status'),
            'emp_start_date'=>  $request->input('emp_start_date'),
            'emp_end_date'  =>  $request->input('emp_end_date'),
            'org_code'      =>  $request->input('org_code'),
            'location_code' =>  $request->input('location_code'),
            'job_title'     =>  $request->input('job_title'),
            'job_cat'       =>  $request->input('job_cat'),
            'job_level'     =>  $request->input('job_level'),
            'job_specifics' =>  $request->input('job_specifics'),
            'address1'      =>  $request->input('address1'),
            'address2'      =>  $request->input('address2'),
            'city'          =>  $request->input('city'),
            'province'      =>  $request->input('province'),
            'postal_code'   =>  $request->input('postal_code'),
            'country_id'    =>  $request->input('country'),
            'modified_at'   =>  date('Y-m-d h:i:s'),
            'modified_by'   =>  Auth::id()
        ]);
        
        // Store Contact Information
        if ( !empty($request->input('contact_info')) ) {
            foreach ($request->contact_info as $key => $contact) {

                $contacts = Contacts::firstOrNew(
                    ['id'               =>  $request->contact_ids[$key]],
                    ['type'             =>  $request->contact_type[$key]],
                    ['category'         =>  $request->contact_cat[$key]]
                );
                
                $contacts->emp_id        =  $id;
                $contacts->type          =  $request->contact_type[$key];
                $contacts->category      =  $request->contact_cat[$key];
                $contacts->desc          =  $contact; 
                $contacts->created_at    =  date('Y-m-d h:i:s');
                $contacts->modified_at   =  date('Y-m-d h:i:s');
                $contacts->created_by    =  Auth::id();
                $contacts->modified_by   =  Auth::id();

                $contacts->save();
            }
        }
        
        // flash message - updated employee successfully.
        $request->session()->flash('message', 'Successfully updated employee!');
        
        // redirect
        return redirect()->route('employee.show', [$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeContact($id)
    {
        // delete
        $contact = Contacts::findOrFail($id);
        $contact->delete();

        return response()->json($contact);
    }

    public function orgLocationList($id)
    {
        $locations = Location::where('org_code', $id)->pluck('desc', 'id');

        return response()->json($locations);
    }

    public function catJobList($id)
    {
        $job_title = JobTitle::where('cat_id', $id)->pluck('desc', 'id');

        return response()->json($job_title);
    }


}
