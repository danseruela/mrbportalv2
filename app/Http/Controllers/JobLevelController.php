<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobLevel;
use DB;

class JobLevelController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    protected $user;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_level_list')->with([ 
                'job_levels' => JobLevel::all() 
            ]);
        } else {
            
            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_level_create')->with([
                'action'        =>  'create',
                'available_id'  =>  DB::table('job_level')->max('id') + 1,
            ]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store
        $job_level = JobLevel::create([
            'id'        =>  $request->input('job_level_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        // flash message - created job level successfully.
        $request->session()->flash('message', 'Successfully created Job Level!');
        
        // redirect
        return redirect('job/level');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job_level = JobLevel::find($id);

        return view('job_level_create')->with([
            'job_level'      =>  $job_level,
            'action'        =>  'edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        JobLevel::where('id', '=', $id )
        ->update([
            'id'        =>  $request->input('job_level_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        
        // flash message - updated job level successfully.
        $request->session()->flash('message', 'Successfully updated Job Level!');
        
        // redirect
        return redirect('job/level');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
