<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PayGrade;
use DB;

class PayGradeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pay_grades_list')->with([
            'pay_grades' => PayGrade::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $available_id = DB::table('pay_grades')->max('id') + 1;

        return view('pay_grades_create')->with([
            'action'        =>  'create',
            'available_id'  =>  $available_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store
        $job_title = PayGrade::create([
            'id'            =>  $request->input('Pay_grade_id'),
            'desc'          =>  $request->input('desc'),
            'min_salary'    =>  $request->input('min_salary'),
            'max_salary'    =>  $request->input('max_salary'),
        ]);
        
        // flash message - created pay grade successfully.
        $request->session()->flash('message', 'Successfully created Pay Grade!');
        
        // redirect
        return redirect('job/paygrade');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paygrade = PayGrade::find($id);
        
        return response()->json(['paygrade' => $paygrade]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pay_grade = PayGrade::find($id);

        return view('pay_grades_create')->with([
            'pay_grade'     =>  $pay_grade,
            'action'        =>  'edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        PayGrade::where('id', '=', $id )
        ->update([
            'id'              =>  $request->input('pay_grade_id'),
            'desc'            =>  $request->input('desc'),
            'min_salary'      =>  $request->input('min_salary'),
            'max_salary'      =>  $request->input('max_salary'),
        ]);
        
        
        // flash message - updated pay grade successfully.
        $request->session()->flash('message', 'Successfully updated Pay Grade!');
        
        // redirect
        return redirect('job/paygrade');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
