<?php

namespace App\Http\Controllers;

use App\AmlaGroup;
use Illuminate\Http\Request;
use Exception;
use DB;

class AmlaGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => AmlaGroup::select(
                        'amla_group.*',
                        'amla_type.desc AS type',
                        'c.username AS created_by',
                        'm.username AS modified_by'
                        )
                        ->leftJoin('amla_type', 'amla_group.amla_type', '=', 'amla_type.id')
                        ->leftJoin('users AS c', 'amla_group.created_by', '=', 'c.id')
                        ->leftJoin('users AS m', 'amla_group.modified_by', '=', 'm.id')
                        ->orderBy('amla_group.desc')
                        ->get(),
        ]);
    }

    /**
     * Display a listing of the resource
     * param string $amlatype
     * 
     * @return \Illuminate\Http\Response
     */
    public function getGroupWatchlist($amlatype) {
        $type = ($amlatype == 'countries') ? 'Watchlist Countries' : 'Watchlist Corporation';

        return response()->json([
            'data' => AmlaGroup::select(
                        'amla_group.*',
                        'amla_type.desc AS type',
                        'c.username AS created_by',
                        'm.username AS modified_by'
                        )
                        ->leftJoin('amla_type', 'amla_group.amla_type', '=', 'amla_type.id')
                        ->leftJoin('users AS c', 'amla_group.created_by', '=', 'c.id')
                        ->leftJoin('users AS m', 'amla_group.modified_by', '=', 'm.id')
                        ->where('amla_type.desc', '=', $type)
                        ->orderBy('amla_group.desc')
                        ->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $amla_group              = new AmlaGroup;
        $amla_group->desc        = $request->desc;
        $amla_group->date_issued = $request->date_issued;
        $amla_group->amla_type   = ($request->type == 'countries') ? 2 : 3; // check amla_type id for this.
        $amla_group->remarks     = $request->remarks;
        $amla_group->created_at  = date('Y-m-d h:i:s');
        $amla_group->created_by  = $request->user;
        $amla_group->save();

        return response()->json([
            'data'  =>  $amla_group
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $amla_group = AmlaGroup::find($id);

        return response()->json([
            'data'          =>  $amla_group,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $amla_group               = AmlaGroup::findOrFail($id);
        $amla_group->desc         = $request->desc;
        $amla_group->date_issued  = $request->date_issued;
        $amla_group->remarks      = $request->remarks;
        $amla_group->modified_at  = date('Y-m-d h:i:s');
        $amla_group->modified_by  = $request->user;
        $amla_group->save();

        return response()->json([
            'data'  =>  $amla_group
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $amla_group = AmlaGroup::find($id);
            $amla_group->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $amla_group,
            'error' =>  $error,
        ]);
    }
}
