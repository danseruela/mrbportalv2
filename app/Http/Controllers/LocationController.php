<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use App\Location;

class LocationController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    protected $user;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {
            
            return view('setup_location_list')->with(['locations' => Location::all()]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('setup_location_create')->with([
                'action'        =>  'create',
                'available_id'  =>  Location::max('id') + 1,
                'organizations' =>  Organization::pluck('desc', 'id'),
            ]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store
        $job_title = Location::create([
            'id'            =>  $request->input('location_id'),
            'org_code'      =>  $request->input('org_code'),
            'desc'          =>  $request->input('desc'),
            'address'       =>  $request->input('address'),
        ]);
        
        // flash message - created location successfully.
        $request->session()->flash('message', 'Successfully created Location!');
        
        // redirect
        return redirect('setup/location');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::find($id);

        return view('setup_location_create')->with([
            'action'        =>  'edit',
            'location'      =>  $location,
            'organizations' =>  Organization::pluck('desc', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        Location::where('id', '=', $id )
        ->update([
            'id'          =>  $request->input('location_id'),
            'org_code'    =>  $request->input('org_code'),
            'desc'        =>  $request->input('desc'),
            'address'     =>  $request->input('address'),
        ]);
        
        
        // flash message - updated location successfully.
        $request->session()->flash('message', 'Successfully updated Location!');
        
        // redirect
        return redirect('setup/location');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
