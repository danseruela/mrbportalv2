<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobCat;
use App\JobTitle;
use DB;

class JobTitleController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    protected $user;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_list')->with([ 
                'job_titles' => JobTitle::all() 
            ]);
        } else {
            
            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('job_create')->with([
                'action'        =>  'create',
                'available_id'  =>  DB::table('job_title')->max('id') + 1,
                'job_cat'       =>  JobCat::pluck('desc', 'id'),
            ]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store
        $job_title = JobTitle::create([
            'id'        =>  $request->input('job_id'),
            'cat_id'    =>  $request->input('cat_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        // flash message - created job title successfully.
        $request->session()->flash('message', 'Successfully created Job Title!');
        
        // redirect
        return redirect('job/title');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job_title = Jobtitle::find($id);

        return view('job_create')->with([
            'job_title'      =>  $job_title,
            'action'        =>  'edit',
            'job_cat'       =>  JobCat::pluck('desc', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        Jobtitle::where('id', '=', $id )
        ->update([
            'id'        =>  $request->input('job_id'),
            'cat_id'    =>  $request->input('cat_id'),
            'desc'      =>  $request->input('desc'),
        ]);
        
        
        // flash message - updated job title successfully.
        $request->session()->flash('message', 'Successfully updated Job Title!');
        
        // redirect
        return redirect('job/title');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
