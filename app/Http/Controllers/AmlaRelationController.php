<?php

namespace App\Http\Controllers;

use App\AmlaRelation;
use App\RelType;
use App\RelConsanguinity;
use DB;
use Exception;
use Illuminate\Http\Request;

class AmlaRelationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $amla_relations                   = new AmlaRelation;
        $amla_relations->amla_id          = $request->amla_id;
        $amla_relations->firstname        = $request->firstname;
        $amla_relations->middlename       = $request->middlename;
        $amla_relations->lastname         = $request->lastname;
        $amla_relations->type             = $request->type;
        $amla_relations->consanguinity    = $request->consanguinity;
        $amla_relations->save();

        return response()->json([
            'data'  =>  $amla_relations
        ]);
    }

    /**
     * Display individual relation
     * @param int $id
     */
    public function getRelation($id)
    {
        return response()->json([
            'data'  =>  AmlaRelation::find($id),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'data'  =>  AmlaRelation::select(
                        'amla_relations.id',
                        DB::raw("CONCAT(amla_relations.lastname, ', ', amla_relations.firstname, ' ', substr(amla_relations.middlename, 1, 1), '.') as fullname"),
                        'rel_type.desc AS type',
                        'rel_consanguinity.desc1 AS consanguinity'
                        )
                        ->leftJoin('rel_type', 'rel_type.id', '=', 'amla_relations.type')
                        ->leftJoin('rel_consanguinity', 'rel_consanguinity.id', '=', 'amla_relations.consanguinity')
                        ->where('amla_relations.amla_id', '=', $id)
                        ->get(),
        ]);
    }

    public function relTypes()
    {
        return response()->json([
            'data'  =>  RelType::all(),
        ]);
    }

    public function relConsanguinity()
    {
        return response()->json([
            'data'  =>  RelConsanguinity::select('rel_consanguinity.id', 'rel_consanguinity.desc1 AS desc')->get(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $amla_relations                   = AmlaRelation::findOrFail($id);
        $amla_relations->amla_id          = $request->amla_id;
        $amla_relations->firstname        = $request->firstname;
        $amla_relations->middlename       = $request->middlename;
        $amla_relations->lastname         = $request->lastname;
        $amla_relations->type             = $request->type;
        $amla_relations->consanguinity    = $request->consanguinity;
        $amla_relations->save();

        return response()->json([
            'data'  =>  $amla_relations
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $amla_relations = AmlaRelation::find($id);
            $amla_relations->delete();

            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $amla_relations,
            'error' =>  $error,
        ]);
    }
}
