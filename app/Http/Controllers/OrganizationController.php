<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use DB;

class OrganizationController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth', ['except' => ['list']]);
    // }

    protected $user;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {
            
            return view('setup_org_list')->with(['organizations' => Organization::all()]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }    
    }

    public function list()
    {
        return response()->json([
            'data' => DB::table('organization')->orderBy('desc')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->user = auth()->user();

        if ($this->user->can('manage-setup', $this->user)) {

            return view('setup_org_create')->with([
                'action'        =>  'create',
                'available_id'  =>  Organization::max('id') + 1,
            ]);
        } else {

            return view('error')->with([
                'error'         =>  '603',
                'message'       =>  'Access denied.',
            ]);
        }    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store
        $job_title = Organization::create([
            'id'            =>  $request->input('org_id'),
            'desc'          =>  $request->input('desc'),
        ]);
        
        // flash message - created organization successfully.
        $request->session()->flash('message', 'Successfully created Organization!');
        
        // redirect
        return redirect('setup/organization');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organization = Organization::find($id);

        return view('setup_org_create')->with([
            'action'           =>  'edit',
            'organization'     =>  $organization,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update
        Organization::where('id', '=', $id )
        ->update([
            'id'              =>  $request->input('org_id'),
            'desc'            =>  $request->input('desc'),
        ]);
        
        
        // flash message - updated organization successfully.
        $request->session()->flash('message', 'Successfully updated Organization!');
        
        // redirect
        return redirect('setup/organization');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
