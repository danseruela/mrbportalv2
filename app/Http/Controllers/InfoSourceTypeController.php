<?php

namespace App\Http\Controllers;

use App\InfoSourceType;
use Illuminate\Http\Request;
use Exception;

class InfoSourceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => InfoSourceType::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $info_source_type       = new InfoSourceType;
        $info_source_type->desc = $request->desc;
        $info_source_type->save();

        return response()->json([
            'data'  =>  $info_source_type
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $info_source_type = InfoSourceType::findOrFail($id);
        $info_source_type->desc = $request->desc;
        $info_source_type->save();

        return response()->json([
            'data'  =>  $info_source_type
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $info_source_type = InfoSourceType::find($id);
            $info_source_type->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $info_source_type,
            'error' =>  $error,
        ]);
    }
}
