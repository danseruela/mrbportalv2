<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Response;
use Validator;
use App\Employee;
use App\Relations;
use App\RelType;
use App\RelConsanguinity;
use DB;

class RelationController extends Controller
{
    protected $rules = 
    [
        'rel_firstname'  =>  'min:2',
        'rel_middlename' =>  'min:2',
        'rel_lastname'   =>  'min:2',
    ];

    protected $messages = 
    [
        'rel_firstname.min'    =>  'First Name field must have (2) min characters.',
        'rel_middlename.min'   =>  'Middle Name field must have (2) min characters.',
        'rel_lastname.min'     =>  'Last Name field must have (2) min characters.',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$validator = Validator::make(Input::all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {*/
        // Check if added relation already exist.
        $relatives = Relations::where([
                    'emp_id'          =>  $request->rel_emp_id,
                ])->whereNotNull('emp_related_id')->get();

        foreach ($relatives as $relative) {
            if ($relative->emp_related_id == $request->emp_related_id) {
                $error = 'Employee Related Account already exist!';
            }
        }

        if (!empty($error)) {
            return Response::json(['errors' => $error]);
        } else {

            // Store
            $relation = new Relations;
            $relation->emp_id               =  $request->rel_emp_id;
            $relation->emp_related_id       =  $request->emp_related_id;
            $relation->firstname            =  $request->rel_firstname;
            $relation->middlename           =  $request->rel_middlename;
            $relation->lastname             =  $request->rel_lastname;
            $relation->type                 =  $request->rel_type;
            $relation->consanguinity        =  $request->rel_consanguinity;
            $relation->is_related           =  $request->is_related;
            $relation->is_emergency_contact =  $request->is_emergency_contact;
            $relation->address              =  $request->emergency_address;
            $relation->contact_no           =  $request->emergency_contact;
            $relation->save();
            

            $related = [
                'getType'           =>  RelType::where('id', $request->rel_type)->pluck('desc')->all(),
                'getConsanguinity'  =>  RelConsanguinity::where('id', $request->rel_consanguinity)->pluck('desc1')->all(),
                'getEmpRelative'    =>  Employee::select(DB::raw("CONCAT(lastname, ', ', firstname, ' ', substr(middlename, 1, 1), '.') as fullname"))->where('id', $request->emp_related_id)->get()->pluck('fullname'),
            ];

            // Response
            return response()->json(['relation' => $relation, 'related' => $related]);
        }    
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$validator = Validator::make(Input::all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {*/
 
            // Store
            $relation = Relations::findOrFail($id);
            $relation->emp_id               = $request->rel_emp_id;
            $relation->emp_related_id       = $request->emp_related_id;
            $relation->firstname            = $request->rel_firstname;
            $relation->middlename           = $request->rel_middlename;
            $relation->lastname             = $request->rel_lastname;
            $relation->type                 = $request->rel_type;
            $relation->consanguinity        = $request->rel_consanguinity;
            $relation->is_related           = $request->is_related;
            $relation->is_emergency_contact = $request->is_emergency_contact;
            $relation->address              = $request->emergency_address;
            $relation->contact_no           = $request->emergency_contact;
            $relation->save();

            $related = [
                'getType'           =>  RelType::where('id', $request->rel_type)->pluck('desc')->all(),
                'getConsanguinity'  =>  RelConsanguinity::where('id', $request->rel_consanguinity)->pluck('desc1')->all(),
                'getEmpRelative'    =>  Employee::select(DB::raw("CONCAT(lastname, ', ', firstname, ' ', substr(middlename, 1, 1), '.') as fullname"))->where('id', $request->emp_related_id)->get()->pluck('fullname'),
            ];

            // Response
            return response()->json(['relation' => $relation, 'related' => $related]);
        //}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete
        $relation = Relations::findOrFail($id);
        $relation->delete();

        return response()->json($relation);
    }
}
