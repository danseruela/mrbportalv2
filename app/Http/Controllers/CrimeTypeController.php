<?php

namespace App\Http\Controllers;

use App\CrimeType;
use Illuminate\Http\Request;
use Exception;

class CrimeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data' => CrimeType::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $crime_type       = new CrimeType;
        $crime_type->desc = $request->desc;
        $crime_type->save();

        return response()->json([
            'data'  =>  $crime_type
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'data'  =>  CrimeType::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $crime_type = CrimeType::findOrFail($id);
        $crime_type->desc = $request->desc;
        $crime_type->save();

        return response()->json([
            'data'  =>  $crime_type
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $crime_type = CrimeType::find($id);
            $crime_type->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $crime_type,
            'error' =>  $error,
        ]);
    }
}
