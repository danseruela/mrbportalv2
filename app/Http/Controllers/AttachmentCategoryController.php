<?php

namespace App\Http\Controllers;

use App\AttachmentCategory;
use Illuminate\Http\Request;
use Exception;
use DB;

class AttachmentCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'data'  =>  AttachmentCategory::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attachment_category       = new AttachmentCategory;
        $attachment_category->desc = $request->desc;
        $attachment_category->save();

        return response()->json([
            'data'  =>  $attachment_category
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'data'  =>  AttachmentCategory::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attachment_category = AttachmentCategory::findOrFail($id);
        $attachment_category->desc = $request->desc;
        $attachment_category->save();

        return response()->json([
            'data'  =>  $attachment_category
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $attachment_category = AttachmentCategory::find($id);
            $attachment_category->delete();
            $error = null;
        } catch(Exception $e) {
            $error = $e->getMessage();
        }

        return response()->json([
            'data'  =>  $attachment_category,
            'error' =>  $error,
        ]);
    }
}
