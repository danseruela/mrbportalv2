<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Employee;
use App\Relations;
use App\RelType;
use App\RelConsanguinity;
use App\Organization;

class ReportController extends Controller
{

	public function __construct()
	{
	    $this->middleware('auth');
	}

    public function employeeRelatedAccountsAll()
    {
        $all_er_accounts = Relations::all();

        return view('report_era')->with([
            'report_title'      =>  'Employee Related Accounts Report',
            'all_er_accounts'   =>  $all_er_accounts,
        ]);
    }

    public function employeeRelatedAllAPI()
    {
        $all_er_accounts = Relations::select(
                            DB::raw("IF(`relations`.emp_related_id IS NULL, 
                                        CONCAT(`relations`.lastname, ', ', `relations`.firstname, ' ', IFNULL(`relations`.middlename, '')),        
                                        CONCAT(`emp`.lastname, ', ', `emp`.firstname, ' ', IFNULL(`emp`.middlename, ''))) as relative_name"),  
                            DB::raw("CONCAT(`employees`.lastname, ', ', `employees`.firstname, ' ', IFNULL(`employees`.middlename, '')) as related_to"),
                            'rel_type.desc as relation',
                            'rel_consanguinity.desc1 as consanguinity',
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, organization.desc, '') as company"),  
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, location.desc, '') as location"), 
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, job_title.desc, '') as designation")
                        )
                        ->leftJoin('employees', 'relations.emp_id', '=', 'employees.id')
                        ->leftJoin('employees as emp', 'relations.emp_related_id', '=', 'emp.id')
                        ->leftJoin('rel_type', 'relations.type', '=', 'rel_type.id')
                        ->leftJoin('rel_consanguinity', 'relations.consanguinity', '=', 'rel_consanguinity.id')
                        ->leftJoin('organization', 'emp.org_code', '=', 'organization.id')
                        ->leftJoin('location', 'emp.location_code', '=', 'location.id')
                        ->leftJoin('job_title', 'emp.job_title', '=', 'job_title.id')
                        ->get();

        return response()->json(['data' => $all_er_accounts]);
    }

    public function employeeRelatedDosriAPI()
    {
        $er_dosri = Relations::select(
                            DB::raw("IF(`relations`.emp_related_id IS NULL, 
                                        CONCAT(`relations`.lastname, ', ', `relations`.firstname, ' ', IFNULL(`relations`.middlename, '')),        
                                        CONCAT(`emp`.lastname, ', ', `emp`.firstname, ' ', IFNULL(`emp`.middlename, ''))) as relative_name"),  
                            DB::raw("CONCAT(`employees`.lastname, ', ', `employees`.firstname, ' ', IFNULL(`employees`.middlename, '')) as related_to"),
                            'rel_type.desc as relation',
                            'rel_consanguinity.desc1 as consanguinity',
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, organization.desc, '') as company"),  
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, location.desc, '') as location"), 
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, job_title.desc, '') as designation")
                        )
                        ->leftJoin('employees', 'relations.emp_id', '=', 'employees.id')
                        ->leftJoin('employees as emp', 'relations.emp_related_id', '=', 'emp.id')
                        ->leftJoin('rel_type', 'relations.type', '=', 'rel_type.id')
                        ->leftJoin('rel_consanguinity', 'relations.consanguinity', '=', 'rel_consanguinity.id')
                        ->leftJoin('organization', 'emp.org_code', '=', 'organization.id')
                        ->leftJoin('location', 'emp.location_code', '=', 'location.id')
                        ->leftJoin('job_title', 'emp.job_title', '=', 'job_title.id')
                        ->whereIn('employees.job_level', [1,2,3,4,5,6])
                        ->get();

        return response()->json(['data' => $er_dosri]);
    }

    public function employeeRelatedAccountsAPI()
    {
        $is_related = Relations::select(
                            DB::raw("IF(`relations`.emp_related_id IS NULL, 
                                        CONCAT(`relations`.lastname, ', ', `relations`.firstname, ' ', IFNULL(`relations`.middlename, '')),        
                                        CONCAT(`emp`.lastname, ', ', `emp`.firstname, ' ', IFNULL(`emp`.middlename, ''))) as relative_name"),  
                            DB::raw("CONCAT(`employees`.lastname, ', ', `employees`.firstname, ' ', IFNULL(`employees`.middlename, '')) as related_to"),
                            'rel_type.desc as relation',
                            'rel_consanguinity.desc1 as consanguinity',
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, organization.desc, '') as company"),  
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, location.desc, '') as location"), 
                            DB::raw("IF(`relations`.emp_related_id IS NOT NULL, job_title.desc, '') as designation")
                        )
                        ->leftJoin('employees', 'relations.emp_id', '=', 'employees.id')
                        ->leftJoin('employees as emp', 'relations.emp_related_id', '=', 'emp.id')
                        ->leftJoin('rel_type', 'relations.type', '=', 'rel_type.id')
                        ->leftJoin('rel_consanguinity', 'relations.consanguinity', '=', 'rel_consanguinity.id')
                        ->leftJoin('organization', 'emp.org_code', '=', 'organization.id')
                        ->leftJoin('location', 'emp.location_code', '=', 'location.id')
                        ->leftJoin('job_title', 'emp.job_title', '=', 'job_title.id')
                        ->where('employees.job_level', '=', 7)
                        ->get();

        return response()->json(['data' => $is_related]);
    }

    public function employeeRelatedAccounts() 
    {
    	$er_accounts = Relations::where('is_related', 1)->get();

    	return view('report_era')->with([ 
            'report_title'  => 'Employee Related Accounts Report',
            'er_accounts'   => $er_accounts 
        ]);
    }

    public function employeesAPI($id)
    {
        if($id==0) { // Display All
            $ref_col = 'employees.id';
            $opr = '!=';
            $id = NULL;
        } else {    // Select Org Code
            $ref_col = 'employees.org_code';
            $opr = '=';
            $id = $id;
        }
        
        $employees = Employee::select(
                        'employees.id',
                        'employees.emp_no',
                        DB::raw("CONCAT(`employees`.lastname, ', ', `employees`.firstname, ' ', IFNULL(`employees`.middlename, '')) as employee_name"),
                        'organization.desc as company',  
                        'employees.emp_start_date',
                        DB::raw("job_title.desc as designation"),
                        DB::raw("job_level.desc as job_level"),
                        'emp_status.desc as emp_status'
                    )
                    ->leftJoin('organization', 'employees.org_code', '=', 'organization.id')
                    ->leftJoin('emp_status', 'employees.emp_status', '=', 'emp_status.id')
                    ->leftJoin('job_title', 'employees.job_title', '=', 'job_title.id')
                    ->leftJoin('job_level', 'employees.job_level', '=', 'job_level.id')
                    ->where($ref_col, $opr, $id)
                    ->orderby('employees.emp_no')
                    ->get();

        return response()->json(['data' =>  $employees]);
    }

    public function genderChart()
    {
        $male       = Employee::where('gender', '=', 1)->count();
        $female     = Employee::where('gender', '=', 2)->count();

        $data   =   array(
            ['data' => $male*100/Employee::count(),   'label' =>  'Male', 'color' => 'rgb(60, 141, 188)' ],
            ['data' => $female*100/Employee::count(),   'label' =>  'Female', 'color' => 'rgb(245, 105, 84)' ],
        );

        return response()->json($data);
    }

    public function empStatusChart()
    {
        return response()->json(
            [
                [ 'data' =>Employee::where('emp_status', '=', 5)->count()*100/Employee::count(), 'label' => 'Regular', 'color' => '#00a65a' ],
                [ 'data' =>Employee::where('emp_status', '=', 1)->count()*100/Employee::count(), 'label' => 'Probationary', 'color' => '#337ab7' ],
                [ 'data' =>Employee::where('emp_status', '=', 4)->count()*100/Employee::count(), 'label' => 'Casual', 'color' => '#ff851b' ],
                [ 'data' =>Employee::where('emp_status', '=', 2)->count()*100/Employee::count(), 'label' => 'Part-time', 'color' => '#dd4b39' ],
                [ 'data' =>Employee::where('emp_status', '=', 3)->count()*100/Employee::count(), 'label' => 'Fixed-term', 'color' => '#605ca8' ],
            ]
        );
    }

    public function employees() 
    {   
        $user = Auth::user();

        if ($user->hasRole('Administrator')) {
            $employees = Employee::orderby('emp_no')->get();
        } else {
            $employees = Employee::where('emp_no', 'NOT LIKE', '%SysAd%')->orderby('emp_no')->get();
        }

    	return view('report_emp')->with([ 
            'report_title'  => 'Employee Master List Report',
            'employees'     =>  $employees, 
            'companies'     =>  Organization::pluck('desc', 'id') 
        ]);
    }

    public function showEmpRelatives($id)
    {
        $employee   = Employee::find($id);
        $relatives  = Relations::where('emp_id', $id)->get();

        return view('report_emp_relatives')->with([
            'report_title'  =>  'Employee Relations',
            'employee'      =>  $employee,
            'relatives'     =>  $relatives,
        ]);
    }

    public function getResignedEmployees($date_from, $date_to)
    {
        $employees = Employee::select(
                                'employees.id',
                                'employees.emp_no',
                                DB::raw("CONCAT(`employees`.lastname, ', ', `employees`.firstname, ' ', IFNULL(`employees`.middlename, '')) as employee_name"),
                                'organization.desc as company',  
                                'employees.emp_start_date',
                                'employees.emp_end_date',
                                DB::raw("job_title.desc as designation"),
                                DB::raw("job_level.desc as job_level"),
                                'emp_status.desc as emp_status'
                            )
                            ->leftJoin('organization', 'employees.org_code', '=', 'organization.id')
                            ->leftJoin('emp_status', 'employees.emp_status', '=', 'emp_status.id')
                            ->leftJoin('job_title', 'employees.job_title', '=', 'job_title.id')
                            ->leftJoin('job_level', 'employees.job_level', '=', 'job_level.id')
                            ->whereBetween('employees.emp_end_date', [$date_from, $date_to])
                            ->where('employees.org_code', 1)
                            ->orderby('employees.emp_end_date', 'asc')
                            ->get();

        return response()->json(['data' =>  $employees]);
    }

}
