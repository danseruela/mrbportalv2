<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpSkills extends Model
{
    protected $table = 'emp_skills';

    public $timestamps = false;
}
