<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelType extends Model
{
    protected $table = 'rel_type';

    protected $fillable = ['id', 'desc'];

    public $timestamps = false;

    public function relation() {
        return $this->belongsTo(Relations::class);
    }
}
