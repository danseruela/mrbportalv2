<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'location';

    protected $fillable = ['id', 'org_code', 'desc', 'address'];

    public $timestamps = false;

    public function getOrg() {
    	return $this->hasOne(Organization::class, 'id', 'org_code');
    }
}
