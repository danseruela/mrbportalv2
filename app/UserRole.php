<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'users_role';

    protected $fillable = ['id', 'name'];

    public $timestamps = false;

    /*public function user() {
    	return $this->belongsTo(User::class);
    }*/

	public function users()
	{
		return $this->belongsToMany(User::class, 'users_roles', 'role_id', 'user_id');
	}    
}
