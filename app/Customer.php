<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Customer extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $connection = 'pgsql';

    protected $table = 'customer';

    public function address() {
        return $this->hasMany(Address::class);
    }

    public function contact() {
        return $this->hasMany(Contact::class);
    }

    public function education() {
        return $this->hasMany(Education::class);
    }

    public function employment() {
        return $this->hasOne(Employment::class, 'customer_id', 'id');
    }

    public function business() {
        return $this->hasOne(Business::class, 'customer_id', 'id');
    }

    public function presentedId() {
        return $this->hasMany(PresentedId::class);
    }

    public function relationIcbs() {
        return $this->hasMany(RelationIcbs::class);
    }
}
