<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpStatus extends Model
{
    protected $table = 'emp_status';

    protected $fillable = ['id', 'desc'];

    public $timestamps = false;
}
