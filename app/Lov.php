<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lov extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'lov';
}
