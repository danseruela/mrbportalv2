<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    /*
	 * Attributes that are mass assignable.
     */
    protected $fillable = ['emp_no', 'lastname', 'firstname', 'middlename', 'nickname', 'emp_photo', 'birthdate', 'gender', 'civil_status', 'blood_type', 'emp_status', 'emp_start_date', 'emp_end_date', 'org_code', 'location_code', 'job_title', 'job_cat', 'job_level', 'job_specifics', 'address1', 'address2', 'city', 'province', 'postal_code', 'country_id', 'created_at', 'created_by', 'modified_at', 'modified_by'];

    public $timestamps = false;

    public function getJob() {
        return $this->hasOne(JobTitle::class, 'id', 'job_title');
    }

    public function getJobLevel() {
        return $this->hasOne(JobLevel::class, 'id', 'job_level');
    }

    public function getStatus() {
        return $this->hasOne(EmpStatus::class, 'id', 'emp_status');
    }

    public function getGender() {
        return $this->hasOne(Gender::class, 'id', 'gender');
    }

    public function getCivilStatus() {
        return $this->hasOne(CivilStatus::class, 'id', 'civil_status');
    }

    public function getBloodType() {
        return $this->hasOne(BloodType::class, 'id', 'blood_type');
    }

    public function getOrg() {
        return $this->hasOne(Organization::class, 'id', 'org_code');
    }

    public function getLocation() {
        return $this->hasOne(Location::class, 'id', 'location_code');
    }

    public function getCountry() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
    /*
	 *
	 * Get the contacts of the employee.
     */
    public function contacts() {
    	return $this->hasMany(Contacts::class);
    }
}
