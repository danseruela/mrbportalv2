<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amla extends Model
{
    protected $table = 'amla';

    protected $fillable = [
                'firstname',
                'middlename', 
                'lastname', 
                'amla_type', 
                'organization', 
                'position', 
                'barangay', 
                'city', 
                'province', 
                'postal_code', 
                'region', 
                'crime_type', 
                'info_source', 
                'info_source_type', 
                'date_issued', 
                'remarks',
                'created_at', 
                'created_by', 
                'modified_at', 
                'modified_by'
    ];

    public $timestamps = false;

    public function getAmlaType() {
        return $this->hasOne(AMLAType::class, 'id', 'amla_type');
    }

    public function getOrg() {
        return $this->hasOne(Organization::class, 'id', 'organization');
    }

    public function getPosition() {
        return $this->hasOne(JobTitle::class, 'id', 'position');
    }

    public function getPostalCode() {
        return $this->hasOne(PostalCode::class, 'id', 'postal_code');
    }

    public function getRegion() {
        return $this->hasOne(Region::class, 'id', 'org_code');
    }

    public function getCrimeType() {
        return $this->hasOne(CrimeType::class, 'id', 'crime_type');
    }

    public function getInfoSource() {
        return $this->hasOne(InfoSource::class, 'id', 'info_source');
    }

    public function getInfoSourceType() {
        return $this->hasOne(InfoSourceType::class, 'id', 'info_source_type');
    }

    public function crimes() {
        return $this->belongsToMany(CrimeType::class, 'amla_crimes', 'amla_id', 'crime_id');
    }

}
