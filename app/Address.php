<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $connection = 'pgsql';

    protected $table = 'address';

    public function type() {
        return $this->belongsTo(AddressType::class);
    }

    public function town() {
        return $this->belongsTo(Town::class);
    }
}
