<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoSource extends Model
{
    protected $table = 'info_source';

    public $timestamps = false;
}
