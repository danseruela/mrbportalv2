<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayFrequency extends Model
{
    protected $table = 'pay_frequency';

    protected $fillable = ['id', 'desc'];

    public $timestamps = false;
}
