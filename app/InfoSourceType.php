<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoSourceType extends Model
{
    protected $table = 'info_source_type';

    public $timestamps = false;
}
