<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\Employee;
use App\JobCat;

use App\Policies\UserPolicy;
use App\Policies\EmployeePolicy;



class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Employee::class => EmployeePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Define Gates.
        Gate::define('admin-only', function($user){
            if($user->hasRole('Administrator')) {
                return true;
            }
            return false;
        });

        Gate::define('manage-setup', function($user){
            if ($user->hasAnyRole(['Administrator', 'Content Manager'])) {
                return true;
            }
            return false;
        });
        // Employee Management
        Gate::define('employee.create', 'App\Policies\EmployeePolicy@create');
        Gate::define('report.view', 'App\Policies\EmployeePolicy@generateReport');
        Gate::resource('employee', 'App\Policies\EmployeePolicy');

        // AMLA Management
        Gate::define('amla.create', 'App\Policies\AmlaPolicy@create');
        Gate::resource('amla', 'App\Policies\AmlaPolicy');       

    }
}
