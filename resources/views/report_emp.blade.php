@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net/css/buttons.dataTables.min.css') }}">
  <!-- Media Print -->
  <link rel="stylesheet" media="print" href="{{ asset('custom/css/print.css') }}">
  <!-- custom -->
  <style>
    .report-filter { margin-bottom: 20px; }
  </style>
@endsection

@section('title', $report_title)

@section('page_title', 'Reports')

@section('breadcrumb_title', 'Reports')

@section('content')

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <h2 class="page-header text-center">
        <!-- <i class="fa fa-file"></i> -->
        <span id="report_title">{{ $report_title }}</span>
        <small>as of </small>
        <small>{{ Carbon\Carbon::now()->format('d-m-Y') }}</small>
      </h2>
      <!-- <a href="#" class="btn btn-success print pull-right" onclick="window.print()">
        <i class="fa fa-print"></i> 
        Print
      </a> -->
      <!-- <a href="#" class="btn btn-success">Create Organization</a> -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">  
    <div class="row report-filter">
      <div class="col-xs-2">
        <strong>Group By Company:</strong>
      </div>
      <div class="col-xs-4">
        <p> 
          @if (count($companies) > 0)
            {!! Form::select('company', $companies, null, ['id' => 'company', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
          @endif
        </p>
      </div>
    </div>
        @php
          $group = !empty($request->id) ? $request->id : 0;
        @endphp
        <table id="mbTbl2" class="table table-striped dt-responsive" data-group="{{$group}}">
          <thead>
            <tr>
              <th>Employee No.</th>
              <th>Employee Name</th>
              <th>Company</th>
              <th>Date Employed</th>
              <th>Designation</th>
              <th>Job Level</th>
              <th>Employment Status</th>
            </tr>
          </thead>
        </table>  <!-- / table -->  
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- DataTables Buttons -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake_vfs_fonts.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.print.min.js') }}"></script>
<!-- page script -->
<script>
$(document).ready(function(){
    //Initialize Select2 Elements
    $('.select2').select2();
    $(".select2").on("select2:open", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Select an option...");
    });

    $(".select2").on("select2:close", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });
    // Set new data to append
    var newOption = new Option('All', 0, true, true);
    // Append it to the select
    $('#company').append(newOption).trigger('change');
    // Check if there is a request id
    var groupBy = $('#mbTbl2').data('group');
    if(groupBy!=null) {
      $('#company').val(groupBy).trigger('change');
      var title = 'Employee Master List Report (' + $('.select2').find(':selected').text() + ')';
      $('title').text(title);
      $('#report_title').text(title);
    }
    
    // Initialize DataTables
    $('#mbTbl1').DataTable();
    var table = $('#mbTbl2').DataTable({
      'ajaxSource'  : '/reports/emp-master/' + $('#company').val(),
      'columns'     : [
        { 
          'data'    :  'emp_no',
          'render'  :  function(data, type, row, meta) {
            if(type==='display') {
              data = '<a href="/reports/employee/' + row.id + '" class="text-success">' + data + '</a>';
            }
            return data;
          }   
        },
        { 'data':  'employee_name' },
        { 'data':  'company' },
        { 'data':  'emp_start_date' },
        { 'data':  'designation' },
        { 'data':  'job_level' },
        { 'data':  'emp_status' },
      ],
      'dom'         : 'Bfrtip',
      'buttons'     : [
                        {
                          extend: 'pdf',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-file"></i> Export PDF',
                        }, 
                        {
                          extend: 'csv',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-table"></i> Export CSV',
                        },
                        {
                          extend: 'print', 
                          text: '<i class="fa fa-print"></i> <u>P</u>rint', 
                          autoPrint: true,
                          customize: function(win) {
                            $(win.document.body).css({margin: '0 20px'});
                          },
                        }
                      ],
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });

    $(document).on('change', '#company', function(){
      table.ajax.url( '/reports/emp-master/' + $(this).val() ).load();
      var title = 'Employee Master List Report (' + $('.select2').find(':selected').text() + ')';
      $('title').text(title);
      $('#report_title').text(title);
    });
});
</script>
@endsection