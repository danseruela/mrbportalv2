@extends('layouts.master')

@php 
  $page = request()->route()->getName() == 'category.create' ? 'Create Job Category' : 'Edit Job Category';
@endphp

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')
<div class="row">
<div class="col-xs-6">  
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
      <div class="box-body">
      @if ($action == 'create')
          {!! Form::open(['route' => 'category.store', 'role' => 'form', 'method' => 'POST']) !!}
      @else 
          {!! Form::model($job_category, ['route' => ['category.update', $job_category->id], 'role' => 'form', 'method' => 'PUT']) !!}
      @endif
          {{ csrf_field() }}   
        <div class="form-group">
          <label for="id">ID</label>
          {!! Form::text('job_cat_id', !empty($available_id) ? $available_id : $job_category->id, ['id' => 'job_cat_id', 'class' => 'form-control', 'placeholder' => 'ID', 'readonly', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Job Category Description</label>
          {!! Form::text('desc', null, ['desc' => 'desc', 'class' => 'form-control', 'placeholder' => 'Description', 'required']) !!}
        </div>
        <hr>
        <div class="text-right">
          <input type="submit" class="btn btn-success" style="font-weight: 700;" value="Save">
        </div> 
        {!! Form::close() !!}
      </div>  <!-- / .box-body -->   
    </div>  <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection