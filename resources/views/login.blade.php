<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ env('APP_NAME') }} | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/square/green.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- Custom Style -->
  <link rel="stylesheet" href="{{ asset('custom/css/global.css') }}">
  <!-- Favicon Logo -->
  <link rel="icon" href="{{ asset('img/mactanbank-icon-32x32.png') }}">

  <style>
    body { height: auto; }
  </style>
</head>
<body class="hold-transition skin-green login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="{{ asset('img/it-portal-icon.png') }}" alt="mactanbank logo" style="width: 100px;"><br />
      @php $appname = explode(' ', env('APP_NAME')); @endphp
    <a href="{{ url('/') }}"><strong>{{ $appname[0] }}</strong> {{ $appname[1] }}</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body custom-box-success">
    <p class="login-box-msg">Sign in to start your session</p>

    {!! Form::open(['url' => '/', 'method' => 'POST']) !!}
      <div class="form-group has-feedback">
        {!! Form::text('username', old('username'), ['id' => 'username', 'class' => 'form-control', 'placeholder' => 'Username', 'required', 'autocomplete' => 'off']) !!}
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'required']) !!}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              {!! Form::checkbox('remember_token', null, false) !!} Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          {!! Form::submit('Sign In', ['class' => 'btn btn-success btn-block btn-flat']) !!}
        </div>
        <!-- /.col -->
      </div>
    {!! Form::close() !!}
    <!-- <hr> -->
    <!-- <a href="#" class="text-success">Forgot Password?</a><br> -->

  </div>
  <!-- /.login-box-body -->
  <div class="text-muted" style="text-align: center; margin-top: 10px;">
  Maintained by Mactanbank IT Department
  </div>

  @if ($errors->any())
  <div class="alert alert-danger alert-dismissible" style="margin-top: 10px;">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
      <ul class="list-unstyled">
          @foreach($errors->all() as $error)
            <li>{{ $error }} </li>
          @endforeach  
      </ul>    
  </div>
  @endif

</div>
<!-- /.login-box -->
  
<!-- jQuery 3 -->
<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(document).ready(function(){
    $('#username').focus();
  });

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
