@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@section('title', 'Job Levels')

@section('page_title', 'Job Levels')

@section('breadcrumb_title', 'Job Levels')

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="{{ route('level.create') }}" class="btn btn-success">Create Job Level</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    @if (count($job_levels) > 0)  
        <table id="mbTbl1" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>ID</th>
              <th>Job Title</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($job_levels as $job_level)  
            <tr>
              <td><a class="text-success" href="{{ route('level.edit', ['id' => $job_level->id]) }}">{{ $job_level->id }}</a></td>
              <td><a class="text-success" href="{{ route('level.edit', ['id' => $job_level->id]) }}">{{ $job_level->desc }}</a></td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Job Level</th>
            </tr>
          </tfoot>
        </table>  <!-- / table -->  
    @endif      
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#mbTbl1').DataTable()
    $('#mbTbl2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection