@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net/css/buttons.dataTables.min.css') }}">
  <!-- Media Print -->
  <link rel="stylesheet" media="print" href="{{ asset('custom/css/print.css') }}">
  <!-- custom -->
  <style>
    .report-filter { margin-bottom: 20px; }
    .report-filter .iradio_square-green {
      margin-left: 20px !important;
    }
  </style>
  
@endsection

@section('title', $report_title)

@section('page_title', 'Reports')

@section('breadcrumb_title', 'Reports')

@section('content')

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <h2 class="page-header text-center">
        <!-- <i class="fa fa-file"></i> -->
        <span id="report_title">{{ $report_title }}</span>
        <small>as of </small>
        <small>{{ Carbon\Carbon::now()->format('d-m-Y') }}</small>
      </h2>
      <!-- <a href="#" class="btn btn-success print pull-right" onclick="window.print()">
        <i class="fa fa-print"></i> 
        Print
      </a> -->
      <!-- <a href="#" class="btn btn-success">Create Organization</a> -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">  
    <div class="row">
      <div class="col-xs-12 report-filter">
        <p>
          <strong>Report Filter:</strong> 
          <input type="radio" id="all" name="filter" class="flat-red filter"> All 
          <input type="radio" id="dosri" name="filter" class="flat-red filter"> DOSRI 
          <input type="radio" id="era" name="filter" class="flat-red filter"> ERA
        </p>
      </div>
    </div>
        @php
          $group = !empty($request->group) ? $request->group : "all";
        @endphp
        <table id="mbTbl2" class="table table-striped dt-responsive" data-group="{{$group}}">
          <thead>
            <tr>
              <th>Relative Name</th>
              <th>Related To</th>
              <th>Relation</th>
              <th>Consanguinity</th>
              <th>Company</th>
              <th>Location</th>
              <th>Designation</th>
            </tr>
          </thead>
        </table>  <!-- / table -->    
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- DataTables Buttons -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake_vfs_fonts.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.print.min.js') }}"></script>
<!-- page script -->
<script>
$(document).ready(function() {
    // initialize iCheck
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
      increaseArea: '20%' // optional
    });
    
    // check by default. dispaly all data
    $('#all').iCheck('check');   

    // check if there are request item
    if($('#mbTbl2').data('group')=='all') {
      $('#all').iCheck('check');   
      var title = 'Employee Related Accounts Report (All)';
      $('title').text(title);
      $('#report_title').text(title);
    }
    if($('#mbTbl2').data('group')=='dosri') {
      $('#dosri').iCheck('check');   
      var title = 'Employee Related Accounts Report (DOSRI)';
      $('title').text(title);
      $('#report_title').text(title);
    }
    if($('#mbTbl2').data('group')=='accounts') {
      $('#era').iCheck('check');   
      var title = 'Employee Related Accounts Report (ERA)';
      $('title').text(title);
      $('#report_title').text(title);
    }

    // Initialize datatable.
    $('#mbTbl1').DataTable();
    var table = $('#mbTbl2').DataTable({
      'ajaxSource'  : '/reports/emp-related-' + $('#mbTbl2').data('group'),
      'columns'     : [
        { 'data':  'relative_name' },
        { 'data':  'related_to' },
        { 'data':  'relation' },
        { 'data':  'consanguinity' },
        { 'data':  'company' },
        { 'data':  'location' },
        { 'data':  'designation' },
      ],
      'dom'         : 'Bfrtip',
      'buttons'     : [
                        {
                          extend: 'pdf',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-file"></i> Export PDF',
                        }, 
                        {
                          extend: 'csv',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-table"></i> Export CSV',
                        }, 
                        {
                          extend: 'print', 
                          text: '<i class="fa fa-print"></i> <u>P</u>rint', 
                          autoPrint: true,
                          customize: function(win) {
                            $(win.document.body).css({margin: '0 20px'});
                          },
                        }
                      ],
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
    
    // Report Filter
    $('#all').on('ifChecked', function(){
      table.ajax.url( '/reports/emp-related-all' ).load();
      var title = 'Employee Related Accounts Report (All)';
      $('title').text(title);
      $('#report_title').text(title);
    });

    $('#dosri').on('ifChecked', function(){
      table.ajax.url( '/reports/emp-related-dosri' ).load();
      var title = 'Employee Related Accounts Report (DOSRI)';
      $('title').text(title);
      $('#report_title').text(title);
    });

    $('#era').on('ifChecked', function(){
      table.ajax.url( '/reports/emp-related-accounts' ).load();
      var title = 'Employee Related Accounts Report (ERA)';
      $('title').text(title);
      $('#report_title').text(title);
    });
});

</script>
@endsection