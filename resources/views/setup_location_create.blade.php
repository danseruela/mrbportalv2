@extends('layouts.master')

@php 
  $page = request()->route()->getName() == 'organization.create' ? 'Create Location' : 'Edit Location';
@endphp

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')
<div class="row">
<div class="col-xs-6">  
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
      <div class="box-body">
      @if ($action == 'create')
          {!! Form::open(['route' => 'location.store', 'role' => 'form', 'method' => 'POST']) !!}
      @else 
          {!! Form::model($location, ['route' => ['location.update', $location->id], 'role' => 'form', 'method' => 'PUT']) !!}
      @endif
          {{ csrf_field() }}   
        <div class="form-group">
          <label for="id">ID</label>
          {!! Form::text('location_id', !empty($available_id) ? $available_id : $location->id, ['id' => 'location_id', 'class' => 'form-control', 'placeholder' => 'ID', 'readonly', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Organization</label>
          @php
             $org_code = !empty($location->org_code) ? $location->org_code : null;
          @endphp

          {!! Form::select('org_code', $organizations, $org_code, ['id' => 'org_code', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Location Name</label>
          {!! Form::text('desc', null, ['id' => 'desc', 'class' => 'form-control', 'placeholder' => 'Location Name', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Address</label>
          {!! Form::text('address', null, ['id' => 'address', 'class' => 'form-control', 'placeholder' => 'Address', 'required']) !!}
        </div>
        <hr>
        <div class="text-right">
          <input type="submit" class="btn btn-success" style="font-weight: 700;" value="Save">
        </div> 
        {!! Form::close() !!}
      </div>  <!-- / .box-body -->   
    </div>  <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Page Scripts -->
<script>
  //Initialize Select2 Elements
    $('.select2').select2();
    $(".select2").on("select2:open", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Select an option...");
    });

    $(".select2").on("select2:close", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });
</script>
@endsection