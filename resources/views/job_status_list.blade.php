@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@section('title', 'Job Employment Status')

@section('page_title', 'Job Employment Status')

@section('breadcrumb_title', 'Job Employment Status')

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="{{ route('status.create') }}" class="btn btn-success">Create Employment Status</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    @if (count($emp_statuses) > 0)  
        <table id="mbTbl1" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>ID</th>
              <th>Employment Status</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($emp_statuses as $emp_status)  
            <tr>
              <td><a class="text-success" href="{{ route('status.edit', ['id' => $emp_status->id]) }}">{{ $emp_status->id }}</a></td>
              <td><a class="text-success" href="{{ route('status.edit', ['id' => $emp_status->id]) }}">{{ $emp_status->desc }}</a></td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Employment Status</th>
            </tr>
          </tfoot>
        </table>  <!-- / table -->  
    @endif      
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#mbTbl1').DataTable()
    $('#mbTbl2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection