@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net/css/buttons.dataTables.min.css') }}">
  <!-- Media Print -->
  <link rel="stylesheet" media="print" href="{{ asset('custom/css/print.css') }}">
  
@endsection

@section('title', $report_title)

@section('page_title', 'Reports')

@section('breadcrumb_title', 'Reports')

@section('content')

@if($employee)
<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="row">
    <div class="col-xs-12">
      <div class="box-header">
        <h2>
          {{ $employee->firstname }} {{ substr($employee->middlename,0,1)  }}. {{ $employee->lastname }}
          <a href="#" class="btn btn-success print pull-right" onclick="window.print()"><i class="fa fa-print"></i> Print</a>
        </h2>  
      </div>
    </div>  
    </div>
    <div class="row">
    <!-- Personal Details -->
    <div class="col-xs-6">
    <div class="box-header">
        <h2 class="page-header">
          <i class="fa fa-user-circle"></i> 
          Personal Details  
        </h2>
        <p><strong>Address:</strong> {{ $employee->address1 }}, {{ $employee->address2 }}, {{ $employee->city }}, {{ $employee->province }}, {{ $employee->getCountry->desc }} {{ $employee->postal_code }}</p>
        <p><strong>Date Of Birth:</strong> {{ $employee->birthdate }}</p>
        <p><strong>Gender:</strong> {{ $employee->getGender->desc }}</p>
        <p><strong>Civil Status:</strong> {{ $employee->getCivilStatus->desc }}</p>
    </div> <!-- /.box-header --> 
    </div>
    <!-- Job Details -->
    <div class="col-xs-6">
    <div class="box-header">
        <h2 class="page-header">
          <i class="fa fa-building"></i> 
          Job Details  
        </h2>
        <p><strong>Company:</strong> {{ $employee->getOrg->desc }} ({{ $employee->getLocation->desc }})</p>
        <p><strong>Position:</strong> {{ $employee->getJob->desc }}</p>
        <p><strong>Date Hired:</strong> {{ $employee->emp_start_date }}</p>
        <p><strong>Status:</strong> {{ $employee->getStatus->desc }}</p>
    </div> <!-- /.box-header --> 
    </div> 
    </div>

    <div class="row">
      <div class="col-xs-12"> 
      <div class="box-body">  
          <h2 class="page-header"><i class="fa fa-users"></i> Relatives ({{count($relatives)}})</h2>
          @if(count($relatives) > 0)
            <table id="mbTbl2" class="table table-striped dt-responsive">
            <thead>
              <tr>
                <th>Relative Name</th>
                <th>Relation</th>
                <th>Consanguinity</th>
              </tr>
            </thead>
            <tbody>
              @foreach($relatives as $relative)
                <tr>
                  @if(empty($relative->emp_related_id))
                    <td>{{ $relative->firstname }} {{ $relative->middlename }} {{ $relative->lastname }}</td>
                  @else
                    <td>{{ $relative->empRelated['firstname'] }} {{ $relative->empRelated['middlename'] }} {{ $relative->empRelated['lastname'] }}</td>
                  @endif  
                  <td>{{ $relative->getType->desc }}</td>
                  <td>{{ $relative->getConsanguinity->desc1 }}</td>
                </tr>
              @endforeach  
            </tbody>
          </table>  <!-- / table -->
          @endif 
      </div> <!-- / .box -->
      </div>
    </div> <!-- / .row -->
  </div> <!-- / .box -->
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endif
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- DataTables Buttons -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake_vfs_fonts.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.print.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#mbTbl1').DataTable()
    $('#mbTbl2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
</script>
@endsection