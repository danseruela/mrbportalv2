<!-- MODAL - Setup Tables -->
<div id="personalIDModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <br />
                    <form class="form-horizontal" role="form" onsubmit="return false;">
                        <div class="form-group">
                            <label for="id_type" class="col-sm-3 control-label">ID Type</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="id_type" data-name="ID Type" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="id_no" class="col-sm-3 control-label">ID Number</label>
                            <div class="col-sm-9">
                                <input type="id_no" id="id_no" name="id_no" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_issued" class="col-sm-3 control-label">Date Issued</label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                <input type="date_issued" id="date_issued" name="date_issued" class="form-control pull-right datepicker" autocomplete="off" required>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_expiry" class="col-sm-3 control-label">Date Expiry</label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                <input type="date_expiry" id="date_expiry" name="date_expiry" class="form-control pull-right datepicker" autocomplete="off">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnPostID" class="btn btn-success">
                        <i class='glyphicon glyphicon-floppy-disk'></i> 
                        <span> Save</span>
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i> 
                        <span> Close</span>
                    </button>
                </div>
            </div>
        </div>
</div>

<div id="deleteIDModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <br />
                <div class="text-center">
                    <h3>Are you sure you want to delete this ID?</h3>
                    <h4 class="itemName"></h4>
                </div>
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" id="btnConfirmDeleteID" class="btn btn-danger">
                    <i class='glyphicon glyphicon-trash'></i> 
                    <span> Delete</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>