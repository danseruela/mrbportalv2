<!-- MODAL - Setup Tables -->
<div id="setupAmlaGroupModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 id="modal-header-title" class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h1 id="user-heading" class="text-center text-success"></h1>
                    <br />
                    <form class="form-horizontal" role="form" onsubmit="return false;">
                        <div class="form-group">
                            <label for="desc" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <input type="text" id="desc" name="desc" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_issued" class="col-sm-3 control-label">Date Issued</label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <input type="text" id="date_issued" class="form-control pull-right datepicker" required>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="remarks" class="col-sm-3 control-label">Remarks</label>
                            <div class="col-sm-9">
                            <textarea name="remarks" id="remarks" class="form-control" style="resize: none;" autocomplete="off" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnPost" class="btn btn-success" data-user="{{ $user_id }}">
                        <i class='glyphicon glyphicon-floppy-disk'></i> 
                        <span> Save</span>
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i> 
                        <span> Close</span>
                    </button>
                </div>
            </div>
        </div>
</div>

<div id="deleteItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 id="deleteModal-header-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h1 id="user-heading" class="text-center text-success"></h1>
                <br />
                <div class="text-center">
                    <h3>Are you sure you want to delete this item?</h3>
                    <h4 id="itemName"></h4>
                </div>
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" id="btnConfirmDelete" class="btn btn-danger">
                    <i class='glyphicon glyphicon-trash'></i> 
                    <span> Delete</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>