<!-- MODAL - Setup Tables -->
<div id="setupAmlaModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 id="modal-header-title" class="modal-title"></h4>
                </div>
                <div class="modal-body" style="background-color: #ecf0f5;">
                <form class="form-horizontal" role="form" onsubmit="return false;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active" id="defaultTab"><a href="#personal" data-toggle="tab" aria-expanded="true">Personal Details</a></li>
                                    <li><a href="#contact" data-toggle="tab" aria-expanded="false">Location</a></li>
                                    <li><a href="#information" data-toggle="tab" aria-expanded="false">AMLA Information</a></li>
                                    <li><a href="#relations" data-toggle="tab" aria-expanded="false">Family Relations</a></li>
                                    <li><a href="#remarksTab" data-toggle="tab" aria-expanded="false">Remarks</a></li>
                                </ul>
                                <!-- tab contents -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="personal">
                                    <h1 class="text-center text-success"></h1>
                                    <div class="form-group">
                                        <label for="firstname" class="col-sm-3 control-label">First Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="firstname" name="firstname" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middlename" class="col-sm-3 control-label">Middle Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="middlename" name="middlename" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname" class="col-sm-3 control-label">Last Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="lastname" name="lastname" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="organization" class="col-sm-3 control-label">Organization</label>
                                        <div class="col-sm-9">
                                            <select id="organization" data-name="Organization" style="width:100%;">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="position" class="col-sm-3 control-label">Position</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="position" name="position" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="contact">
                                    <h1 class="text-center text-success"></h1>
                                    <div class="form-group">
                                        <label for="barangay" class="col-sm-3 control-label">Barangay</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="barangay" name="barangay" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="col-sm-3 control-label">City</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="city" name="city" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="province" class="col-sm-3 control-label">Province</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="province" name="province" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="postal_code" class="col-sm-3 control-label">Postal Code</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="postal_code" name="postal_code" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="region" class="col-sm-3 control-label">Region</label>
                                        <div class="col-sm-9">
                                            <select id="region" data-name="Region" style="width:100%;">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="information">
                                    <h1 class="text-center text-success"></h1>
                                    <div class="form-group">
                                        <label for="amla_type" class="col-sm-3 control-label">AMLA Type</label>
                                        <div class="col-sm-9">
                                            <select id="amla_type" data-name="AMLA Type" style="width:100%;">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="crimes" class="col-sm-3 control-label">Alleged Predicate Crimes</label>
                                        <div class="col-sm-9">
                                            <!-- <input type="text" id="crimes" name="crimes" class="form-control" autocomplete="off" required> -->
                                            {!! Form::select('crimes[]', $crime_types, null, ['id' => 'crimes', 'multiple' => 'multiple', 'style' => 'width: 100%;']) !!}
                                        </div>
                                    </div>    
                                    <div class="form-group">
                                        <label for="info_source" class="col-sm-3 control-label">Info Source</label>
                                        <div class="col-sm-9">
                                            <select id="info_source" data-name="Information Source" style="width:100%;">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="info_source_type" class="col-sm-3 control-label">Info Source Type</label>
                                        <div class="col-sm-9">
                                            <select id="info_source_type" data-name="Information Source Type" style="width:100%;">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="relations">
                                        <a href="#" id="addRelation" class="add-modal btn btn-success"><i class="fa fa-plus"></i> Add Family Relation</a>
                                        <table id="relationTable" data-api="/api/amlarelations/" class="table table-bordered table-hover dt-responsive">
                                            <thead>
                                                <tr>
                                                <th>Name</th>
                                                <th>Relation</th>
                                                <th>Degree of Consanguinity</th>
                                                <th>Actions</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="remarksTab">
                                    <h1 class="text-center text-success"></h1>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <textarea name="remarks" id="remarks" class="form-control" style="resize: none;" autocomplete="off" cols="30" rows="12"></textarea>
                                        </div>
                                    </div>
                                    </div> <!-- end of remarks -->
                                </div><!-- end of tab-content -->
                            </div><!-- end of nav-tabs-custom -->
                        </div><!-- end of col-sm-12 -->
                    </div><!-- end of row -->
                </form>                   
                </div> <!-- end of modal-body -->
                <div class="modal-footer">
                    <button type="button" id="btnPost" class="btn btn-success" data-user="{{ $user_id }}">
                        <i class='glyphicon glyphicon-floppy-disk'></i> 
                        <span> Save</span>
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i> 
                        <span> Close</span>
                    </button>
                </div>
            </div>
        </div>
</div>

<!-- Modal form Relations -->
<div id="setupRelationModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" onsubmit="return false;">    
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="rel_firstname">First Name:</label>
                            <div class="col-sm-9">  
                                <input type="text" id="rel_firstname" class="form-control" placeholder="First Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="rel_middlename">Middle Name:</label>
                            <div class="col-sm-9">
                                <input type="text" id="rel_middlename" class="form-control" placeholder="Middle Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="rel_lastname">Last Name:</label>
                            <div class="col-sm-9">
                                <input type="text" id="rel_lastname" class="form-control" placeholder="Last Name">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="rel_type">Relation:</label>
                            <div class="col-sm-9"> 
                                <select id="rel_type" data-name="Relation" style="width:100%;">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="rel_consanguinity">Consanguinity:</label>
                            <div class="col-sm-9">
                                <select id="rel_consanguinity" data-name="Consanguinity" style="width:100%;">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="amla_id">
                    <button type="button" id="submit-relation" class="btn btn-success" data-user="{{ $user_id }}">
                        <i class='glyphicon glyphicon-floppy-disk'></i>
                        <span> Save</span>
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i>
                        <span> Close</span>
                    </button>
                </div>
        </div>
    </div>
</div>
    

<!-- Delete item Modal -->
<div id="deleteItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 id="deleteModal-header-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h1 id="user-heading" class="text-center text-success"></h1>
                <br />
                <div class="text-center">
                    <h3>Are you sure you want to delete this item?</h3>
                    <h4 id="itemName"></h4>
                </div>
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" id="btnConfirmDelete" class="btn btn-danger">
                    <i class='glyphicon glyphicon-trash'></i> 
                    <span> Delete</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>