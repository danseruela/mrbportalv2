<!-- MODAL - Educational Attainment -->
<div id="educAttainmentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <br />
                    <form class="form-horizontal" role="form" onsubmit="return false;">
                        <div class="form-group">
                            <label for="educ_level" class="col-sm-3 control-label">Education Level</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="educ_level" data-name="Education Level" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="school_name" class="col-sm-3 control-label">School Name</label>
                            <div class="col-sm-9">
                                <input type="school_name" id="school_name" name="school_name" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="year_start" class="col-sm-3 control-label">Year Started</label>
                            <div class="col-sm-9">
                                <input type="year_start" id="year_start" name="year_start" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="year_end" class="col-sm-3 control-label">Year Ended</label>
                            <div class="col-sm-9">
                                <input type="year_end" id="year_end" name="year_end" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="degree" class="col-sm-3 control-label">Degree</label>
                            <div class="col-sm-9">
                                <input type="degree" id="degree" name="degree" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnPostEduc" class="btn btn-success">
                        <i class='glyphicon glyphicon-floppy-disk'></i> 
                        <span> Save</span>
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i> 
                        <span> Close</span>
                    </button>
                </div>
            </div>
        </div>
</div>

<div id="deleteEducModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <br />
                <div class="text-center">
                    <h3>Are you sure you want to delete this item?</h3>
                    <h4 class="itemName"></h4>
                </div>
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" id="btnConfirmDeleteEduc" class="btn btn-danger">
                    <i class='glyphicon glyphicon-trash'></i> 
                    <span> Delete</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>