<!-- MODAL - Setup Tables -->
<div id="viewAttachmentType" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 id="modal-header-title" class="modal-title"></h4>
                </div>
                <div class="modal-body">
                <div class="container">
                
                </div>
                    <!-- Dropzone Area -->
                    <div class="dropzone" id="attachedFilesDropZone">
                        {{ csrf_field() }}
                        <input type="hidden" id="file_id" name="file_id">
                        <input type="hidden" id="file_cid" name="file_cid">
                        <input type="hidden" id="file_category" name="file_category">
                        <div class="row">
                            <div class="dz-message">
                                <div class="drag-icon-cph">
                                    <i class="fa fa-cloud-upload dp-icon"></i>
                                </div>
                                <h5 class="dp-message-custom">Drop files here or click to upload.</h5>
                            </div>
                            <div class="fallback">
                                <input type="file" name="file" multiple />
                            </div>
                        </div>
                    </div> <!-- end of dropzone -->
                    <!-- Attachment files Table -->
                    <table class="table table-hover dt-responsive tblAttachments" 
                           style="margin-top: 20px;"
                           data-is_admin="{{ $user->hasRole('Administrator') ? 1 : 0 }}"
                           data-is_newacct="{{ $user->hasRole('ICBS - New Accounts') ? 1 : 0 }}"
                           data-is_loan="{{ $user->hasRole('ICBS - Loans') ? 1 : 0 }}"
                           data-can_create="{{ $user->hasAnyRole(['Administrator', 'ICBS - New Accounts', 'ICBS - Loans']) ? 1 : 0 }}"
                           data-can_delete="{{ $user->hasAnyRole(['Administrator', 'ICBS - New Accounts', 'ICBS - Loans']) ? 1 : 0 }}"
                    >
                        <thead>
                            <tr>
                                <th>Filename</th>
                                <th>Size</th>
                                <th width="25%">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table> <!-- end of table -->
                </div>
                <div class="modal-footer">
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i> 
                        <span> Close</span>
                    </button>
                </div>
            </div>
        </div>
</div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 id="confirmModal-header-title" class="modal-title">Information</h4>
            </div>
            <div class="modal-body">
                <h1 id="user-heading" class="text-center text-success"></h1>
                <br />
                <div class="text-center">
                    <i class="fa fa-check-circle text-success" style="font-size: 100px;"></i><br/>
                    <h3 style="margin: 30px 10px;">Files successfully uploaded!</h3>
                    <!-- <h4 id="confirmFileName"></h4> -->
                    <div class="spacer"></div>
                    <a href="#" id="confirmSuccess" class="btn btn-success">OK</a>
                </div>
                <br />
            </div>
        </div>
    </div>
</div>

<div id="deleteItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 id="deleteModal-header-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h1 id="user-heading" class="text-center text-success"></h1>
                <br />
                <div class="text-center">
                    <h3>Are you sure you want to delete this file?</h3>
                    <h4 id="itemName"></h4>
                </div>
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btnConfirmDeleteFile">
                    <i class='glyphicon glyphicon-trash'></i> 
                    <span> Delete</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="overrideModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 id="overrideModal-header-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                    <br />
                    <form class="form-horizontal" role="form" onsubmit="return false;">
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
                            <input type="username" id="username" name="username" class="form-control" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" id="password" name="password" class="form-control" autocomplete="off" required>
                        </div>
                    </div>
                    <br />
                    </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btnOverride">
                    <i class='glyphicon glyphicon-send'></i> 
                    <span> Submit</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>