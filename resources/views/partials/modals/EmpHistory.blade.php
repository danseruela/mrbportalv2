<!-- MODAL - Employment History -->
<div id="empHistoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <br />
                    <form class="form-horizontal" role="form" onsubmit="return false;">
                        <div class="form-group">
                            <label for="employer_name" class="col-sm-3 control-label">Employer Name</label>
                            <div class="col-sm-9">
                                <input type="employer_name" id="employer_name" name="employer_name" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="employer_address" class="col-sm-3 control-label">Employer Address</label>
                            <div class="col-sm-9">
                                <input type="employer_address" id="employer_address" name="employer_address" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="designation" class="col-sm-3 control-label">Designation</label>
                            <div class="col-sm-9">
                                <input type="designation" id="designation" name="designation" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="employment_id_no" class="col-sm-3 control-label">Employment ID No.</label>
                            <div class="col-sm-9">
                                <input type="employment_id_no" id="employment_id_no" name="employment_id_no" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_start" class="col-sm-3 control-label">Date Started</label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                <input type="date_start" id="date_start" name="date_start" class="form-control pull-right datepicker" autocomplete="off" required>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_end" class="col-sm-3 control-label">Date Ended</label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                <input type="date_end" id="date_end" name="date_end" class="form-control pull-right datepicker" autocomplete="off" required>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="eh_tel_no" class="col-sm-3 control-label">Telephone No.</label>
                            <div class="col-sm-9">
                                <input type="eh_tel_no" id="eh_tel_no" name="eh_tel_no" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="eh_email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="eh_email" id="eh_email" name="eh_email" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnPostEmpHistory" class="btn btn-success">
                        <i class='glyphicon glyphicon-floppy-disk'></i> 
                        <span> Save</span>
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i> 
                        <span> Close</span>
                    </button>
                </div>
            </div>
        </div>
</div>

<div id="deleteEmpHistoryModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <br />
                <div class="text-center">
                    <h3>Are you sure you want to delete this item?</h3>
                    <h4 class="itemName"></h4>
                </div>
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" id="btnConfirmDeleteEmpHistory" class="btn btn-danger">
                    <i class='glyphicon glyphicon-trash'></i> 
                    <span> Delete</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>