<!-- MODAL - Setup Tables -->
<div id="setupModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 id="modal-header-title" class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h1 id="user-heading" class="text-center text-success"></h1>
                    <br />
                    <form class="form-horizontal" role="form" onsubmit="return false;">
                        <div class="form-group">
                            <label for="desc" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <input type="desc" id="desc" name="desc" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div id="category-wrapper" class="form-group hidden">
                            <label for="category" class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="category" data-name="Category" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnPost" class="btn btn-success">
                        <i class='glyphicon glyphicon-floppy-disk'></i> 
                        <span> Save</span>
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <i class='glyphicon glyphicon-remove'></i> 
                        <span> Close</span>
                    </button>
                </div>
            </div>
        </div>
</div>

<div id="deleteItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 id="deleteModal-header-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h1 id="user-heading" class="text-center text-success"></h1>
                <br />
                <div class="text-center">
                    <h3>Are you sure you want to delete this item?</h3>
                    <h4 id="itemName"></h4>
                </div>
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" id="btnConfirmDelete" class="btn btn-danger">
                    <i class='glyphicon glyphicon-trash'></i> 
                    <span> Delete</span>
                </button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <i class='glyphicon glyphicon-remove'></i> 
                    <span> Close</span>
                </button>
            </div>
        </div>
    </div>
</div>