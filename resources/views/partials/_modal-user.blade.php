<!-- MODALS -->
<!-- Modal form Reset Password -->
    <div id="resetPasswordModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h1 id="user-heading" class="text-center text-success"></h1>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                              <div class="input-group">
                                <input type="text" id="password" name="password" class="form-control" rel="gp" data-size="8" data-character-set="a-z,A-Z,0-9" autocomplete="off" required>
                                <span class="input-group-btn">
                                  <button type="button" class="getNewPass btn btn-success">
                                    <i class="glyphicon glyphicon-refresh"></i> 
                                    Generate
                                  </button>
                                </span> 
                              </div>  
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="save-password" class="btn btn-success">
                        <span class='glyphicon glyphicon-floppy-disk'></span> Reset
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>

<!-- Modal form edit User -->
    <div id="editUserModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}  
                        <div class="form-group">
                          <label for="employee" class="control-label col-sm-3">Employee Name</label>
                          <div class="col-sm-9">
                            <input type="hidden" id="user_id" name="user_id">
                            @php
                                $emp_id = !empty($user->emp_id) ? $user->emp_id : null;
                            @endphp
                            {!! Form::select('emp_id', $employees, $emp_id, ['id' => 'emp_id', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'disabled' => 'disabled', 'required']) !!}
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="username" class="control-label col-sm-3">Username</label>
                          <div class="col-sm-9">
                            {!! Form::text('username', null, ['id' => 'username', 'class' => 'form-control', 'placeholder' => 'Username', 'disabled' => 'disabled', 'required']) !!}
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="user_role" class="control-label col-sm-3">Role</label>
                          @php
                              $user_role = $user->roles()->where('user_id', $user->id)->get();
                          @endphp
                          <div class="col-sm-9">
                            {!! Form::select('user_roles[]', $roles, null, ['id' => 'user_roles', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'multiple' => 'multiple', 'required']) !!}
                          </div>  
                        </div>
                        <div class="form-group">
                          <label for="status" class="control-label col-sm-3">Status</label>
                          <div class="col-sm-9">
                            {!! Form::hidden('deleted', !empty($user->deleted) ? $user->deleted : '', ['id' => 'deleted']) !!}
                            <select name="status" id="status" class="select2" style="width: 100%;">
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                          </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" id="save-user" class="btn btn-success" data-dismiss="modal">
                        <span class='glyphicon glyphicon-floppy-disk'></span> Save
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>        

<!-- Modal form delete User -->
    <div id="deleteUserModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h4 class="user-heading text-center"></h4>
                    <h1 class="user-details text-center text-success"></h1>
                </div>
                <div class="modal-footer">
                    <button type="button" id="delete-user" class="btn btn-danger" data-dismiss="modal">
                        <span class='glyphicon glyphicon-trash'></span> Delete
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>