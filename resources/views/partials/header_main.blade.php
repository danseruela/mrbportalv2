  <header class="main-header" data-user-id="{{ Auth::user()->id }}" data-is-first-login="{{ Auth::user()->is_first_login }}">
    <!-- Logo -->
    <a href="{{ url('dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">{{ env('ACRONYM') }}</span>
      <!-- logo for regular state and mobile devices -->
      @php $appname = explode(' ', env('APP_NAME')); @endphp
      <span class="logo-lg"><strong>{{ $appname[0] }}</strong> {{ $appname[1] }}</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu" id="markasread" onclick="markNotificationAsRead()">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger">{{count(Auth::user()->unreadNotifications)}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have {{count(Auth::user()->unreadNotifications)}} notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu"> 
                  @foreach(Auth::user()->unreadNotifications as $notification)
                    <li>
                      @include('partials.notifications.'.snake_case(class_basename($notification->type)))
                    </li>
                  @endforeach
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          @php
            $photo = !empty(Auth::user()->employee->emp_photo) ? Storage::url(Auth::user()->employee->emp_photo) : asset('/img/user-mb-128x128.jpg');
          @endphp
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ $photo }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->employee->firstname }} {{ Auth::user()->employee->lastname }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ $photo }}" class="img-circle" alt="User Image">

                <p>
                  {{ Auth::user()->employee->firstname }} {{ Auth::user()->employee->lastname }}
                  <small>Member since {{ Carbon\Carbon::parse( Auth::user()->created_at )->format('M. Y') }}</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url('profile') }}" class="btn btn-success btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('logout') }}" class="btn btn-success btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button 
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
          -->
        </ul>
      </div>
    </nav>
  </header>