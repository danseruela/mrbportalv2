  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        @php
          $photo = !empty(Auth::user()->employee->emp_photo) ? Storage::url(Auth::user()->employee->emp_photo) : asset('/img/user-mb-128x128.jpg');
        @endphp
        <div class="pull-left image">
          <img src="{{ $photo }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->employee->firstname }} {{ Auth::user()->employee->lastname }}</p>
          <!-- @foreach (Auth::user()->roles as $role)
            <a href="#"><i class="fa fa-circle text-success"></i> {{$role->name}}</a><br>
          @endforeach -->
          <a href="#"><i class="fa fa-circle text-success"></i> {{ Auth::user()->deleted == 0 ? "Active" : "Inactive" }}</a><br>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li> <!-- main navigation -->
        <li class="@php echo Request::path() == 'dashboard' || Request::is('dashboard/*') ? 'active' : null; @endphp">
          <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="@php echo Request::path() == 'profile' || Request::is('profile/*') ? 'active' : null; @endphp">
          <a href="{{ url('profile') }}">
            <i class="fa fa-user-circle"></i> <span>Profile</span>
          </a>
        </li>
 <!--       <li class="@php echo Request::path() == 'leave' || Request::is('leave/*') ? 'active' : null; @endphp treeview">  
          <a href="#">
            <i class="fa fa-user"></i> <span>Leave</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@php echo Request::path() == 'leave' ? 'active' : null; @endphp"><a href="{{ url('leave-apply') }}"><i class="fa fa-users"></i> Apply Leave</a></li>
            <li class="@php echo Request::path() == 'leave' ? 'active' : null; @endphp"><a href="{{ url('leave-list') }}"><i class="fa fa-users"></i> Leave List</a></li>
            <li class="@php echo Request::path() == 'leave' ? 'active' : null; @endphp"><a href="{{ url('entitlements') }}"><i class="fa fa-users"></i> Entitlements</a></li>
          </ul>
        </li>
        <li class="@php echo Request::path() == 'time' || Request::is('time/*') ? 'active' : null; @endphp treeview">  
          <a href="#">
            <i class="fa fa-user"></i> <span>Time</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@php echo Request::path() == 'time' ? 'active' : null; @endphp"><a href="{{ url('overtime') }}"><i class="fa fa-users"></i> Apply Overtime</a></li>
            <li class="@php echo Request::path() == 'time' ? 'active' : null; @endphp"><a href="{{ url('undertime') }}"><i class="fa fa-users"></i> Apply Undertime</a></li>
            <li class="@php echo Request::path() == 'time' ? 'active' : null; @endphp"><a href="{{ url('timesheet') }}"><i class="fa fa-users"></i> Timesheet</a></li>
          </ul>
        </li>  
        <li class="@php echo Request::path() == 'payslip' || Request::is('payslip/*') ? 'active' : null; @endphp">
          <a href="{{ url('payslip') }}">
            <i class="fa fa-dashboard"></i> <span>Payslip</span>
          </a>
        </li>  -->
        @can('employee.create', auth()->user())
        <li class="header">ADMINISTRATION</li> <!-- ADMINISTRATION -->
        <li class="@php echo Request::path() == 'employee' || Request::is('employee/*') ? 'active' : null; @endphp treeview">  
          <a href="#">
            <i class="fa fa-user"></i> <span>ERA Monitoring</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@php echo Request::path() == 'employee' ? 'active' : null; @endphp"><a href="{{ url('employee') }}"><i class="fa fa-users"></i> Employee List</a></li>
            <li class="@php echo Request::path() == 'employee/create' ? 'active' : null; @endphp"><a href="{{ url('employee/create') }}"><i class="fa fa-user-plus"></i> Create Employee</a></li>
          </ul>
        </li>
        @endcan
        <li class="@php echo Request::path() == 'customers' || Request::is('customers/*') ? 'active' : null; @endphp treeview">  
          <a href="#">
            <i class="fa fa-user"></i> <span>CIF Digitization</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@php echo Request::path() == 'customers' ? 'active' : null; @endphp"><a href="{{ url('customers') }}"><i class="fa fa-users"></i> Customer List</a></li>
          </ul>
        </li>
        <li class="@php echo Request::path() == 'amla' || Request::path() == 'amlagroup' || Request::is('amla/*')  ? 'active' : null; @endphp treeview">  
          <a href="#">
            <i class="fa fa-eye"></i> <span>AMLA Monitoring</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@php echo Request::path() == 'amla' ? 'active' : null; @endphp"><a href="{{ url('amla') }}"><i class="fa fa-user"></i> AMLA Watchlist</a></li>
            <li class="@php echo Request::path() == 'amlagroup' ? 'active' : null; @endphp"><a href="{{ url('amlagroup') }}"><i class="fa fa-users"></i> AMLA Group Watchlist</a></li>
          </ul>
        </li>
        <!--
        <li class="@php echo Request::path() == 'attendance' || Request::is('attendance/*') ? 'active' : null; @endphp treeview">
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Attendance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-clock-o"></i> Timesheet</a></li>
            <li><a href="#"><i class="fa fa-list"></i> Employee Record</a></li>
          </ul>
        </li>
        <li class="@php echo Request::path() == 'payroll' || Request::is('payroll/*') ? 'active' : null; @endphp treeview">
          <a href="#">
            <i class="fa fa-money"></i> <span>Payroll</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-desktop"></i> Transaction</a></li>
            <li><a href="#"><i class="fa fa-cc-visa"></i> Payslip</a></li>
          </ul>
        </li>
        <li class="@php echo Request::path() == 'leave' || Request::is('leave/*') ? 'active' : null; @endphp treeview">
          <a href="#">
            <i class="fa fa-plane"></i> <span>Leave</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-list-alt"></i> Leave List</a></li>
            <li><a href="#"><i class="fa fa-plus"></i> Entitlements</a></li>
            <li><a href="#"><i class="fa fa-plus"></i> Assign Leave</a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cog"></i> <span>Configure</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Leave Period</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Leave Types</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Work Week</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Holidays</a></li>
              </ul>
            </li>  
          </ul>
        </li>
      -->  
        @can('report.view', Auth::user())
        <li class="@php echo Request::path() == 'reports' || Request::is('reports/*') ? 'active' : null; @endphp treeview">
          <a href="#">
            <i class="fa fa-area-chart"></i> <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@php echo Request::path() == 'reports/employee-master' ? 'active' : null; @endphp"><a href="{{ url('reports/employee-master') }}"><i class="fa fa-file"></i> Employee List</a></li>
            @can('manage-setup', auth()->user())
            <li class="@php echo Request::path() == 'reports/resigned' ? 'active' : null; @endphp"><a href="{{ url('reports/resigned') }}"><i class="fa fa-file"></i> Employee List - Resigned</a></li>
            @endcan
            <li class="@php echo Request::path() == 'reports/employee-related-accounts' ? 'active' : null; @endphp"><a href="{{ url('reports/employee-related-accounts') }}"><i class="fa fa-file"></i> Employee Related</a></li>
            <li class="@php echo Request::path() == 'reports/customer-uploads' ? 'active' : null; @endphp"><a href="{{ url('reports/customer-uploads') }}"><i class="fa fa-file"></i> Attachments by Customer</a></li>
            <!-- <li class="@php echo Request::path() == 'reports/attachments/branch' ? 'active' : null; @endphp"><a href="{{ url('reports/attachments/branch') }}"><i class="fa fa-file"></i> Attachments by Branch</a></li> -->
            <!-- <li class="@php echo Request::path() == 'reports/' ? 'active' : null; @endphp"><a href="#"><i class="fa fa-file"></i> Attendance List</a></li>            
            <li class="@php echo Request::path() == 'reports/' ? 'active' : null; @endphp"><a href="#"><i class="fa fa-file"></i> Leave Entitlement List</a></li>
            <li class="@php echo Request::path() == 'reports/' ? 'active' : null; @endphp"><a href="#"><i class="fa fa-file"></i> Payroll List</a></li> -->
          </ul>
        </li>
        @endcan
        @can('manage-setup', auth()->user())
        <li class="@php echo Request::path() == 'downloads' || Request::is('downloads/*') ? 'active' : null; @endphp treeview">
          <a href="#">
            <i class="fa fa-download"></i> <span>Downloads</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>ERA</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="@php echo Request::path() == 'downloads/era-form-download' ? 'active' : null; @endphp"><a href="{{ route('eraform.download') }}"><i class="fa fa-file"></i> ERA Fill-Up Form</a></li>
                <li class="@php echo Request::path() == 'downloads/rpt-form-download' ? 'active' : null; @endphp"><a href="{{ route('rptform.download') }}"><i class="fa fa-file"></i> RPT Fill-Up Form</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>IT</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="@php echo Request::path() == 'downloads/it-request-form-download' ? 'active' : null; @endphp"><a href="{{ route('itrequestform.download') }}"><i class="fa fa-file"></i> IT Request Form</a></li>
                <li class="@php echo Request::path() == 'downloads/user-request-form-download' ? 'active' : null; @endphp"><a href="{{ route('userrequestform.download') }}"><i class="fa fa-file"></i> User Request Form</a></li>
              </ul>
            </li>
          </ul>
        </li>
    <!--
        <li class="@php echo Request::path() == 'job' || Request::is('job/*') ? 'active' : null; @endphp treeview">
          <a href="#">
            <i class="fa fa-briefcase"></i> <span>Job</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@php echo Request::path() == 'job/title' ? 'active' : null; @endphp"><a href="{{ url('job/title') }}"><i class="fa fa-circle-o"></i> Job Titles</a></li>
            <li class="@php echo Request::path() == 'job/category' ? 'active' : null; @endphp"><a href="{{ url('job/category') }}"><i class="fa fa-circle-o"></i> Job Categories</a></li>
            <li class="@php echo Request::path() == 'job/status' ? 'active' : null; @endphp"><a href="{{ url('job/status') }}"><i class="fa fa-circle-o"></i> Employment Status</a></li>
            <li class="@php echo Request::path() == 'job/paygrade' ? 'active' : null; @endphp"><a href="{{ url('job/paygrade') }}"><i class="fa fa-circle-o"></i> Pay Grades</a></li>
            <li class="@php echo Request::path() == 'job/workshift' ? 'active' : null; @endphp"><a href="#"><i class="fa fa-circle-o"></i> Work Shifts</a></li> 
          </ul>
        </li>
      -->
        <li class="@php echo Request::path() == 'setup' || 
                             Request::path() == 'user' ||
                             Request::is('setups/*') || 
                             Request::is('setup/*') || 
                             Request::is('user/*') || 
                             Request::is('job/*') 
                             ? 'active' : null; @endphp treeview">
          <a href="#">
            <i class="fa fa-sitemap"></i> <span>Setup</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can('admin-only', auth()->user())
            <li class="@php echo Request::path() == 'user' || Request::is('user/*') ? 'active' : null; @endphp treeview">
              <a href="#">
                <i class="fa fa-user-circle"></i> <span>User Management</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="@php echo Request::path() == 'user' ? 'active' : null; @endphp"><a href="{{ url('user') }}"><i class="fa fa-users"></i> Users</a></li>
                <li class="@php echo Request::path() == 'user/create' ? 'active' : null; @endphp"><a href="{{ url('user/create') }}"><i class="fa fa-user-plus"></i> Create User</a></li>
              </ul>
            </li>
            @endcan
            <li class="@php echo Request::is('job/*') ? 'active' : null; @endphp treeview">
                <a href="#">
                  <i class="fa fa-briefcase"></i> <span>Job</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class="@php echo Request::path() == 'job/title' ? 'active' : null; @endphp"><a href="{{ url('job/title') }}"><i class="fa fa-circle-o"></i> Job Titles</a></li>
                  <li class="@php echo Request::path() == 'job/category' ? 'active' : null; @endphp"><a href="{{ url('job/category') }}"><i class="fa fa-circle-o"></i> Job Categories</a></li>
                  <li class="@php echo Request::path() == 'job/level' ? 'active' : null; @endphp"><a href="{{ url('job/level') }}"><i class="fa fa-circle-o"></i> Job Levels</a></li>
                  <li class="@php echo Request::path() == 'job/status' ? 'active' : null; @endphp"><a href="{{ url('job/status') }}"><i class="fa fa-circle-o"></i> Employment Status</a></li>
                </ul>  
            </li>
            <li class="@php echo Request::is('setup/*') ? 'active' : null; @endphp treeview">
                <a href="#">
                  <i class="fa fa-building"></i> <span>Company</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <!--   <li class="@php echo Request::path() == 'setup/skills' ? 'active' : null; @endphp"><a href="{{ url('setup/skills') }}"><i class="fa fa-circle-o"></i> Skills</a></li>
                  <li class="@php echo Request::path() == 'setup/education' ? 'active' : null; @endphp"><a href="{{ url('setup/education') }}"><i class="fa fa-circle-o"></i> Education</a></li> -->
                  <li class="@php echo Request::path() == 'setup/organization' ? 'active' : null; @endphp"><a href="{{ url('setup/organization') }}"><i class="fa fa-circle-o"></i> Organizations</a></li>
                  <li class="@php echo Request::path() == 'setup/location' ? 'active' : null; @endphp"><a href="{{ url('setup/location') }}"><i class="fa fa-circle-o"></i> Locations</a></li>
                </ul>  
            
            </li>
            <li class="@php echo Request::path() == 'setups/attachmentcategory' ? 'active' : null; @endphp"><a href="{{ url('setups/attachmentcategory') }}"><i class="fa fa-circle-o"></i> Attachment Category</a></li>
            <li class="@php echo Request::path() == 'setups/attachmenttype' ? 'active' : null; @endphp"><a href="{{ url('setups/attachmenttype') }}"><i class="fa fa-circle-o"></i> Attachment Type</a></li>
            <li class="@php echo Request::path() == 'setups/amlatype' ? 'active' : null; @endphp"><a href="{{ url('setups/amlatype') }}"><i class="fa fa-circle-o"></i> AMLA Type</a></li>
            <li class="@php echo Request::path() == 'setups/region' ? 'active' : null; @endphp"><a href="{{ url('setups/region') }}"><i class="fa fa-circle-o"></i> Region</a></li>
            <!--<li class="@php echo Request::path() == 'setups/postalcode' ? 'active' : null; @endphp"><a href="{{ url('setups/postalcode') }}"><i class="fa fa-circle-o"></i> Postal Code</a></li>-->
            <li class="@php echo Request::path() == 'setups/crimetype' ? 'active' : null; @endphp"><a href="{{ url('setups/crimetype') }}"><i class="fa fa-circle-o"></i> Crime Type</a></li>
            <li class="@php echo Request::path() == 'setups/infosource' ? 'active' : null; @endphp"><a href="{{ url('setups/infosource') }}"><i class="fa fa-circle-o"></i> Info Source</a></li>
            <li class="@php echo Request::path() == 'setups/infosourcetype' ? 'active' : null; @endphp"><a href="{{ url('setups/infosourcetype') }}"><i class="fa fa-circle-o"></i> Info Source Type</a></li>
            <li class="@php echo Request::path() == 'setups/idtype' ? 'active' : null; @endphp"><a href="{{ url('setups/idtype') }}"><i class="fa fa-circle-o"></i> ID Type</a></li>
            <li class="@php echo Request::path() == 'setups/educlevel' ? 'active' : null; @endphp"><a href="{{ url('setups/educlevel') }}"><i class="fa fa-circle-o"></i> Education Level</a></li>
              
          </ul>
        </li>
          @can('admin-only', auth()->user())
            <li class="@php echo Request::path() == 'config' || Request::is('config/*') ? 'active' : null; @endphp treeview">
              <a href="#">
                <i class="fa fa-cog"></i> <span>Configuration</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Modules</a></li>
              </ul>
            </li>
          @endcan
        @endcan
      </ul>
      
    </section>
    <!-- /.sidebar -->
  </aside>