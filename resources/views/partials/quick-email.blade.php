<!-- quick email widget -->
@if(session()->has('message'))
    <div class="alert alert-success" role="alert">
        <strong>Success:</strong> {{ session()->get('message') }}
    </div>
@endif
@if(!session()->has('message'))
<form action="dashboard" method="post">
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-envelope"></i>

            <h3 class="box-title">Quick Mail</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
            <button type="button" class="btn btn-success btn-sm" data-widget="remove" data-toggle="tooltip"
                    title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
            <!-- /. tools -->
        </div>
        <div class="box-body">
            <div class="form-group">
                <input type="email" class="form-control" name="emailto" value="{{ old('emailto') }}" placeholder="Email to:">
                <div class="text-danger">{{ $errors->first('emailto') }}</div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="subject" value="{{ old('subject') }}" placeholder="Subject">
                <div class="text-danger">{{ $errors->first('subject') }}</div>
            </div>
            <div>
                <textarea class="textarea" placeholder="Message" name="message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                    {{ old('message') }}
                </textarea>
                <div class="text-danger">{{ $errors->first('message') }}</div>
            </div>
        </div>
        <div class="box-footer clearfix">
            <button type="submit" class="pull-right btn btn-success" id="sendEmail">Send
            <i class="fa fa-arrow-circle-right"></i></button>
        </div>
    </div>
</form>
@endif