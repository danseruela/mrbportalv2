@if(Auth::user()->is_first_login)
<div class="content-header">
    <div class="callout callout-danger">
        <h4>You haven't changed your password.</h4>

        <p>Please <a href="#" data-toggle="modal" data-target="#changePasswordModal">update your password</a> for security purposes.</p>
    </div>
</div>
@endif