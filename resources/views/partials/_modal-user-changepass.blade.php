<!-- MODAL - Change Password -->
<div id="changePasswordModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h1 id="user-heading" class="text-center text-success"></h1>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="current_password" class="col-sm-3 control-label">Current Password</label>
                            <div class="col-sm-9">
                                <input type="password" id="current_password" name="current_password" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="new_password" class="col-sm-3 control-label">New Password</label>
                            <div class="col-sm-9">
                                <input type="password" id="new_password" name="new_password" class="form-control" autocomplete="off" required>
                                <div class="text-danger" id="errPassMatchMsg" style="margin-top: 8px !important;"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="confirm_password" class="col-sm-3 control-label">Confirm Password</label>
                            <div class="col-sm-9">
                                <input type="password" id="confirm_password" name="confirm_password" class="form-control" autocomplete="off" required>
                                <div class="text-danger" id="errPassConMatchMsg" style="margin-top: 8px !important;"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="save-password" class="btn btn-success">
                        <span class='glyphicon glyphicon-floppy-disk'></span> Save
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
</div>