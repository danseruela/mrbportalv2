<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0.1
    </div>
    <strong>Copyright &copy; 2019 <a href="#">{{ env('APP_NAME') }}</a>.</strong> All rights
    reserved.
 </footer>