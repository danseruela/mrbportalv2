<a href="#" data-toggle="modal" data-target="#changePasswordModal">
  <div style="float:left; margin:10px 14px 10px 0;">
    <i class="fa fa-warning text-red"></i> 
  </div>  
  <div style="float:left;">
    Your password was reset<br>
    <span style="color:#90949c; font-size:12px;">
      {{Carbon\Carbon::parse($notification->created_at)->diffForHumans()}}<!-- updateTime -->
    </span>
  </div>
</a>