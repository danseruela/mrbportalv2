<!-- MODALS -->
<!-- Modal form to deleteContact -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h4 id="delete_question" class="text-center">Are you sure you want to delete this contact?</h4>
                    <br />
                    <form class="form-horizontal" role="form">
                        <h2 id="delete_info" class="text-center text-success" style="margin: 0 auto !important;">
                        </h2>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-trash'></span> Delete
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>

<!-- Modal form addRelation -->
    <div id="addRelation" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    
                    {!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}  
                        @if(!empty($employee->id))
                            {!! Form::hidden('rel_emp_id', $employee->id, ['id' => 'rel_emp_id']) !!}
                        @endif
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                              <div class="checkbox icheck">
                                <label> 
                                  {!! Form::checkbox('is_not_employee', null, false, ['id' => 'is_not_employee']) !!} Not An Employee
                                </label>
                              </div>  
                            </div>
                        </div>
                        <div id="empRelativeDetails">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="emp_related_id">Employee Related:</label>
                                <div class="col-sm-9">
                                  @if(!empty($emp_relations))
                                    {!! Form::select('emp_related_id', $emp_relations, null, ['id' => 'emp_related_id', 'style' => 'width: 100%', 'class' => 'form-control select2']) !!}
                                  @endif  
                                </div>
                            </div>
                        </div>    
                        <div id="nonEmpRelativeDetails" class="hide-item">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="rel_firstname">First Name:</label>
                                <div class="col-sm-9">  
                                    {!! Form::text('rel_firstname', null, ['id' => 'rel_firstname', 'class' => 'form-control', 'placeholder' => 'First Name']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="rel_middlename">Middle Name:</label>
                                <div class="col-sm-9">
                                    {!! Form::text('rel_middlename', null, ['id' => 'rel_middlename', 'class' => 'form-control', 'placeholder' => 'Middle Name']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="rel_lastname">Last Name:</label>
                                <div class="col-sm-9">
                                    {!! Form::text('rel_lastname', null, ['id' => 'rel_lastname', 'class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                                </div>
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="rel_type">Relation:</label>
                            <div class="col-sm-9">
                              @if(!empty($relation_types))
                                {!! Form::select('rel_type', $relation_types, null, ['id' => 'rel_type', 'style' => 'width: 100%', 'class' => 'form-control select2']) !!}
                              @endif  
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="rel_consanguinity">Consanguinity:</label>
                            <div class="col-sm-9">
                              @if(!empty($consanguinities))
                                {!! Form::select('rel_consanguinity', $consanguinities, null, ['id' => 'rel_consanguinity', 'style' => 'width: 100%', 'class' => 'form-control select2']) !!}
                              @endif  
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                              <div class="checkbox icheck">
                                <label> 
                                  {!! Form::checkbox('is_emergency_contact', null, false, ['id' => 'is_emergency_contact']) !!} Is Emergency Contact Person
                                </label>
                              </div>  
                            </div>
                        </div>
                        <div id="emergencyContactDetails" class="hide-item">
                        <h4 class="box-title">In Case Of Emergency</h4>
                        <hr>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="emergency_address">Address:</label>
                            <div class="col-sm-9">
                                {!! Form::text('emergency_address', null, ['id' => 'emergency_address', 'class' => 'form-control', 'placeholder' => 'Address']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="emergency_contact">Contact No:</label>
                            <div class="col-sm-9">
                                {!! Form::text('emergency_contact', null, ['id' => 'emergency_contact', 'class' => 'form-control', 'placeholder' => 'Contact No']) !!}
                            </div>
                        </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" id="submit-relation" class="btn btn-success" data-dismiss="modal">
                        <span class='glyphicon glyphicon-floppy-disk'></span> Add
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form add salary components -->
    <div id="basicSalary" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    
                    {!! Form::open(['class' => 'form-horizontal', 'role' => 'form']) !!}  
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="pay_grd_code">Pay Grade:</label>
                            <div class="col-sm-9">
                            @if(!empty($employee->id))
                                {!! Form::hidden('emp_code', $employee->id, ['id' => 'emp_code']) !!}
                            @endif
                            @if(!empty($pay_grade))
                                {!! Form::select('pay_grd_code', $pay_grade, null, ['id' => 'pay_grd_code', 'style' => 'width: 100%', 'class' => 'form-control select2']) !!}
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="salary_component">Salary Component:</label>
                            <div class="col-sm-9">
                                {!! Form::text('salary_component', null, ['id' => 'salary_component', 'class' => 'form-control', 'placeholder' => 'Salary Component']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="sal_component_type">Type:</label>
                            <div class="col-sm-9">
                            @if(!empty($sal_component_type))
                                {!! Form::select('sal_component_type', $sal_component_type, null, ['id' => 'sal_component_type', 'style' => 'width: 100%', 'class' => 'form-control select2']) !!}
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="pay_freq_code">Pay Frequency:</label>
                            <div class="col-sm-9">
                            @if(!empty($pay_frequency))
                                {!! Form::select('pay_freq_code', $pay_frequency, null, ['id' => 'pay_freq_code', 'style' => 'width: 100%', 'class' => 'form-control select2']) !!}
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="basic_salary">Amount:</label>
                            <div class="col-sm-9">
                                {!! Form::text('basic_salary', null, ['id' => 'basic_salary', 'class' => 'form-control', 'placeholder' => 'Amount']) !!}  
                                <span class="salary-range"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="comments">Comments:</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('comments', null, ['id' => 'comments', 'class' => 'form-control', 'style' => 'width: 100%; resize: vertical; overflow: auto;', 'placeholder' => 'Comments. . .']) !!}  
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" id="submit-basic-salary" class="btn btn-success" data-dismiss="modal">
                        <span class='glyphicon glyphicon-floppy-disk'></span> Add
                    </button>
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>