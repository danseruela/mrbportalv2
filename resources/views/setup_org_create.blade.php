@extends('layouts.master')

@php 
  $page = request()->route()->getName() == 'organization.create' ? 'Create Organization' : 'Edit Organization';
@endphp

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')
<div class="row">
<div class="col-xs-6">  
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
      <div class="box-body">
      @if ($action == 'create')
          {!! Form::open(['route' => 'organization.store', 'role' => 'form', 'method' => 'POST']) !!}
      @else 
          {!! Form::model($organization, ['route' => ['organization.update', $organization->id], 'role' => 'form', 'method' => 'PUT']) !!}
      @endif
          {{ csrf_field() }}   
        <div class="form-group">
          <label for="id">ID</label>
          {!! Form::text('org_id', !empty($available_id) ? $available_id : $organization->id, ['id' => 'org_id', 'class' => 'form-control', 'placeholder' => 'ID', 'readonly', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Organization Name</label>
          {!! Form::text('desc', null, ['id' => 'desc', 'class' => 'form-control', 'placeholder' => 'Organization Name', 'required']) !!}
        </div>
        <hr>
        <div class="text-right">
          <input type="submit" class="btn btn-success" style="font-weight: 700;" value="Save">
        </div> 
        {!! Form::close() !!}
      </div>  <!-- / .box-body -->   
    </div>  <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection