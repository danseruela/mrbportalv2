@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection 

@section('title', $page_title)

@section('page_title', $page_title)

@section('breadcrumb_title', $page_title)

@section('content')

<div class="row employee-profile"> 
<div class="col-md-3">
    <div class="box box-success">
      <div class="box-body box-profile" id="customerProfile" data-api="{{ $api }}" data-user_branch="{{ $user->employee->location_code }}" data-can_create="{{ $user->hasAnyRole(['Administrator', 'ICBS - New Accounts']) ? 1 : 0 }}">
          @php
            // $photo = !empty($employee->emp_photo) ? $employee->emp_photo : 'user-mb-128x128.jpg';
            $photo = 'user-mb-128x128.jpg';
          @endphp
          <a href="#" target="_blank">
            <img id="customer_photo" class="profile-user-img img-responsive img-circle" src="{{ asset('img/'.$photo) }}" alt="User profile picture">
          </a>
          <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#333; position:absolute; left:40%; top:10%; display:none;"></i>
          <h3 class="profile-username text-center" id="customerName"></h3>

          <p class="text-muted text-center">
                <strong id="customerBranch"></strong><br>
          </p>

          <ul class="list-group list-group-unbordered">
            <!-- <li class="list-group-item">
               <i class="fa fa-building margin-r-5"></i> 
               <b>Branch</b> 
               <a class="pull-right text-success" id="customerBranch"></a>
            </li> -->
            <li class="list-group-item">
               <i class="fa fa-id-card margin-r-5"></i> 
               <b>Customer ID</b> 
               <a class="pull-right text-success" id="customerId"></a>
            </li>
            <li class="list-group-item">
               <i class="fa fa-calendar margin-r-5"></i> 
               <b>Customer Type</b> 
               <a class="pull-right text-success" id="customerType"></a>
            </li>
            <li class="list-group-item">
               <i class="fa fa-certificate margin-r-5"></i> 
               <b>Status</b> 
               <a class="pull-right text-success" id="customerStatus"></a>
            </li>
            <li class="list-group-item">
               <i class="fa fa-birthday-cake margin-r-5"></i> 
               <b>Age</b> 
               <a class="pull-right text-success" id="customerAge"></a>
            </li>
            </ul>
            <a href="#" id="viewCustomerPhoto" class="btn btn-success btn-block" target="_blank">
                <b><i class="fa fa-eye"></i> View Profile Picture</b>
            </a>
            <!-- {!! Form::open(['files' => true, 'id' => 'upload_form', 'name' => 'upload_form', 'role' => 'form']) !!} -->
            <!-- <form enctype="multipart/form-data" method="put" id="upload_form" role="form"> -->
              <!-- <label for="image" class="btn btn-success btn-block"><i class="fa fa-camera"></i> Upload Profile Photo</label>
              {!! Form::file('image', ['id' => 'image', 'style' => 'display:none;']) !!}
              {!! Form::hidden('file_name', null, ['id' => 'file_name']) !!} -->
              <!-- <input type="file" id="file" name="file" style="display:block;"> -->
              <!-- <input type="hidden" id="file_name" name="file_name" /> -->
            <!-- </form> -->
            <!-- {!! Form::close() !!} -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- Address Information -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Address Information</h3>
        </div>
        <div class="box-body" id="addressList">
          
        </div>
    </div>
    <!-- Contact Information -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Contact Information</h3>
        </div>
        <div class="box-body" id="contactList">
          
        </div>
    </div>
</div> <!-- col-md-3 -->
<div class="col-md-9">
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#primary" data-toggle="tab" aria-expanded="true">Primary Information</a></li>
    <li class="hidden"><a href="#address" data-toggle="tab" aria-expanded="false">Address</a></li>
    <li><a href="#relation" data-toggle="tab" aria-expanded="false">Relations</a></li>
    <li class="hidden"><a href="#education" data-toggle="tab" aria-expanded="false">Education</a></li>
    <li class="hidden"><a href="#employment" data-toggle="tab" aria-expanded="false">Employment</a></li>
    <li class="hidden"><a href="#business" data-toggle="tab" aria-expanded="false">Business</a></li>
    <li><a href="#ids" data-toggle="tab" aria-expanded="false">Presented IDs</a></li>
    <li><a href="#attachments" data-toggle="tab" aria-expanded="false">Attachments</a></li>
    
  </ul> <!-- / .nav-tabs -->
  <div class="tab-content">
    <!-- Primary Details -->
    <div class="tab-pane active" id="primary">
        <div class="row">
            <div class="col-md-6">
            <p><strong>Customer ID: </strong><span class="text-muted" id="txtCustomerId"></span></p>
            <p><strong>Display Name: </strong><span class="text-muted" id="txtCustomerName"></span></p>
            <p><strong>Title: </strong><span class="text-muted" id="txtTitle"></span></p>
            <p><strong>Initials: </strong><span class="text-muted" id="txtInitials"></span></p>
            <p><strong>Gender: </strong><span class="text-muted" id="txtGender"></span></p>
            <p><strong>Date of Birth: </strong><span class="text-muted" id="txtDateOfBirth"></span></p>
            <p><strong>Place of Birth: </strong><span class="text-muted" id="txtPlaceOfBirth"></span></p>
            </div>
            <div class="col-md-6">
            <p><strong>Customer Type: </strong><span class="text-muted" id="txtCustomerType"></span></p>
            <p><strong>Customer Group: </strong><span class="text-muted" id="txtCustomerGroup"></span></p>
            <p><strong>DOSRI Classification: </strong><span class="text-muted" id="txtDOSRI"></span></p>
            <p><strong>Civil Status: </strong><span class="text-muted" id="txtCivilStatus"></span></p>
            <p><strong>Nationality: </strong><span class="text-muted" id="txtNationality"></span></p>
            <p><strong>Record Status: </strong><span class="text-muted" id="txtStatus"></span></p>
            </div>
        </div>
    </div>
    <!-- Contact Details -->
    <div class="tab-pane hidden" id="address">
        <div class="box-group" id="accordion">
                    
        </div>
    </div> <!-- / #address -->   
    <div class="tab-pane" id="relation">
        <table class="table" id="relationTable">
            <thead>
            <tr>
                <th>Relation</th>
                <th>Customer ID</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Initials</th>
            </tr>
            </thead>
        </table>
    </div> <!-- / #relation -->  
    <div class="tab-pane hidden" id="education">
            <h1>Education</h1>
    </div> <!-- / #education -->  
    <div class="tab-pane hidden" id="employment">
            <h1>Employment Info</h1>
    </div> <!-- / #employment -->  
    <div class="tab-pane hidden" id="business">
            <h1>Business Info</h1>
    </div> <!-- / #business -->  
    <div class="tab-pane" id="ids">
        <table class="table" id="presentedIdTable">
            <thead>
            <tr>
                <th>ID Type</th>
                <th>ID No</th>
                <th>Issue Date</th>
                <th>Valid Till Date</th>
                <th>Gov't Issue</th>
                <th>With Picture</th>
                <th>With Signature</th>
            </tr>
            </thead>
        </table>
    </div> <!-- / #ids -->     
    <div class="tab-pane" id="attachments">
        <!-- CIF Attachments -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Customer Info Attachments</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div> <!-- /.box-header -->
            <div class="box-body no-padding">
                <table id="customerAttachmentTable" class="table table-bordered table-hover dt-responsive" data-api="{{ $api_docs }}">
                    <thead>
                        <tr>
                        <th>Attachment Type</th>
                        <th width="15%">No. Of Files</th>
                        <th width="10%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div> <!-- / .box -->    
        <!-- Loan Attachments -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Loan Attachments</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div> <!-- /.box-header -->
            <div class="box-body no-padding">
                <table id="loanAttachmentTable" class="table table-bordered table-hover dt-responsive" data-api="{{ $api_docs }}">
                    <thead>
                        <tr>
                        <th>Attachment Type</th>
                        <th width="15%">No. Of Files</th>
                        <th width="10%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div> <!-- / .box -->
        <!-- Deposit Attachments -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Deposit Attachments</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div> <!-- /.box-header -->
            <div class="box-body no-padding">
                <table id="depositAttachmentTable" class="table table-bordered table-hover dt-responsive" data-api="{{ $api_docs }}">
                    <thead>
                        <tr>
                        <th>Attachment Type</th>
                        <th width="15%">No. Of Files</th>
                        <th width="10%">Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div> <!-- / .box -->
    </div> <!-- / #attachments -->  
    
    </div> <!-- .tab-content -->    
    <div> <!-- .nav-tabs-custom -->
</div>  <!-- / .col-md-9 -->
</div> <!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <!-- Educational Attainment -->
        <div class="box box-success">
        <div class="box-header with-border">
                <h3 class="box-title">Educational Attainment</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                <table class="table" id="educationalAttainmentTable">
                    <thead>
                    <tr>
                        <th>Level</th>
                        <th>Name of School</th>
                        <th>Year Start</th>
                        <th>Year End</th>
                        <th>Degree</th>
                    </tr>
                    </thead>
                </table>
                </div>
                <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- Employment Details -->
        <div class="box box-success">
        <div class="box-header with-border">
                <h3 class="box-title">Employment Details</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Employer Name: </strong> <span class="text-muted" id="txtEmpName"></span></p>
                            <p><strong>Employer Address: </strong> <span class="text-muted" id="txtEmpAddress"></span></p>
                            <p><strong>Region: </strong> <span class="text-muted" id="txtEmpRegion"></span></p>
                            <p><strong>Designation: </strong> <span class="text-muted" id="txtEmpDesignation"></span></p>
                            <p><strong>Employment ID No.: </strong> <span class="text-muted" id="txtEmpIDNo"></span></p>
                            <p><strong>DEPED ID: </strong> <span class="text-muted" id="txtEmpDepedID"></span></p>
                        </div>
                        <div class="col-md-6">
                            <p><strong>Year Start: </strong> <span class="text-muted" id="txtEmpYearStart"></span></p>
                            <p><strong>Year End: </strong> <span class="text-muted" id="txtEmpYearEnd"></span></p>
                            <p><strong>Credit Savings: </strong> <span class="text-muted" id="txtEmpCreditSavings"></span></p>
                            <p><strong>Contact No.: </strong> <span class="text-muted" id="txtEmpContactNo"></span></p>
                            <p><strong>Fax No.: </strong> <span class="text-muted" id="txtEmpFaxNo"></span></p>
                            <p><strong>Email: </strong> <span class="text-muted" id="txtEmpEmail"></span></p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- Business Details -->
        <div class="box box-success">
        <div class="box-header with-border">
                <h3 class="box-title">Business Details</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Business Name: </strong> <span class="text-muted" id="txtBusName"></span></p>
                            <p><strong>Business Address: </strong> <span class="text-muted" id="txtBusAddress"></span></p>
                            <p><strong>Business Registration: </strong> <span class="text-muted" id="txtBusRegistration"></span></p>
                            <p><strong>Business Activity: </strong> <span class="text-muted" id="txtBusActivity"></span></p>
                            <p><strong>Fax No.: </strong> <span class="text-muted" id="txtBusFaxNo"></span></p>
                            <p><strong>Email: </strong> <span class="text-muted" id="txtBusEmail"></span></p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
</div>

<!-- Modals Here -->
@include('partials.modals.viewAttachmentTypeModal')
    
@endsection

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Select2 Cascade -->
<script src="{{ asset('custom/js/select2Cascade.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Jquery Number Format -->
<script src="{{ asset('custom/js/jquery.number.min.js') }}"></script>
<!-- page script -->
<script>
// Initial Customer Attachment Type Table
var cifAttachmentTypeTable = loadCustomerAttachmentTypeTable('#customerAttachmentTable', 1);
var loanAttachmentTypeTable = loadCustomerAttachmentTypeTable('#loanAttachmentTable', 2);
var depAttachmentTypeTable = loadCustomerAttachmentTypeTable('#depositAttachmentTable', 3);

// Config dropzone
Dropzone.autoDiscover = false;

$('div#attachedFilesDropZone').dropzone({
    url: '/api/uploadFiles',
    acceptedFiles: '.doc,.docx,.xls,xlsx,.pdf,.jpg,.jpeg.,.JPG,.gif,.png',
    addRemoveLinks: true,
    maxFilesize: 5, // MB
    timeout: 180000, 
    success: function(response) {
        if(response.status == 'success') {
            viewAttachedFiles($('#file_id').val(), $('#file_cid').val(), $('#file_category').val());

            $('#confirmModal').modal({show: true});
            loadCustomerPhoto($('#file_cid').val());
            reloadAttachmentTypeDataTable(cifAttachmentTypeTable, '#customerAttachmentTable', 1);
            reloadAttachmentTypeDataTable(loanAttachmentTypeTable, '#loanAttachmentTable', 2);
            reloadAttachmentTypeDataTable(depAttachmentTypeTable, '#depositAttachmentTable', 3);
            // toastr.success('Documents successfully uploaded!', 'Upload Success!');
            // $('.dz-complete').remove();
            // $('.dz-complete').addClass('hidden');
            // $('.dz-message').attr('style','display:block');
        } else {
            toastr.warning('Error while uploading data!', 'Warning!');
        }
    },
    error: function(response) {
        toastr.error('File reached the maximum file size limit!', 'ERROR!');
    },
    removedfile: function(file) {
        var name = file.name;

        $.ajax({
            type: 'DELETE',
            url: '/api/removeFiles/' + $('#file_id').val(),
            data: { file: name },
            dataType: 'html'
        });

        // remove the thumbnail
        var previewElement;
        return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);        
    },
    sending: function(file, xhr, formData) {
        formData.append('file_id', $('#file_id').val());
        formData.append('file_cid', $('#file_cid').val());
        formData.append('file_name', file.name);
    }
});

// Files
function viewAttachedFiles(id, cid, category) {
    $.ajax({
            type: 'GET',
            url: '/api/viewFiles/' + id + '/' + cid,
            success: function(response) {
                var is_admin = $('.tblAttachments').data('is_admin');
                var is_newacct = $('.tblAttachments').data('is_newacct');
                var is_loan = $('.tblAttachments').data('is_loan');
                var action_create = $('.tblAttachments').data('can_create');
                var action_delete = $('.tblAttachments').data('can_delete');
                var customer_branch = $('#customerProfile').data('branch');
                var user_branch = $('#customerProfile').data('user_branch');

                // Dropzone upload restrictions
                if(category=='Customer Information' || category=='Deposit Account') {
                    ((is_newacct == 1 && customer_branch == user_branch || is_admin == 1) ? $('.dropzone').removeClass('hidden') : $('.dropzone').addClass('hidden'));
                }
                if(category=='Loan Account') {
                    ((is_loan == 1 && customer_branch == user_branch || is_admin == 1) ? $('.dropzone').removeClass('hidden') : $('.dropzone').addClass('hidden'));
                }   

                if(response.data.length > 0) {
                    $('.tblAttachments tbody tr').replaceWith('');
                    // populate attached files
                    $.each(response.data, function(index, value) {

                        // ((is_newacct == 1 && value.category.trim() == 'Customer Information' && customer_branch == user_branch || 
                        //   is_newacct == 1 && value.category.trim() == 'Deposit Account' && customer_branch == user_branch ||
                        //   is_loan == 1 && value.category.trim() == 'Loan Account' && customer_branch == user_branch ||
                        //   is_admin == 1) ? $('.dropzone').removeClass('hidden') : $('.dropzone').addClass('hidden'));

                        // Delete attachment restrictions
                        if(category=='Customer Information' || category=='Deposit Account') {
                            var actions = ((is_newacct == 1 && customer_branch == user_branch || is_admin == 1) ? '<a class="tooltipped btn btn-danger deleteFiles" title="Delete" href="#" data-id="' + value.id + '" data-filename="' + value.file_name + '"><i class="fa fa-trash"></i></a>' : '');
                        }
                        if(category=='Loan Account') {
                            var actions = ((is_loan == 1 && customer_branch == user_branch || is_admin == 1) ? '<a class="tooltipped btn btn-danger deleteFiles" title="Delete" href="#" data-id="' + value.id + '" data-filename="' + value.file_name + '"><i class="fa fa-trash"></i></a>' : '');
                        }
                        // var actions = ((is_newacct == 1 && value.category.trim() == 'Customer Information' && customer_branch == user_branch || 
                        //                 is_newacct == 1 && value.category.trim() == 'Deposit Account' && customer_branch == user_branch ||
                        //                 is_loan == 1 && value.category.trim() == 'Loan Account' && customer_branch == user_branch || 
                        //                 is_admin == 1) ? '<a class="tooltipped btn btn-danger deleteFiles" title="Delete" href="#" data-id="' + value.id + '"><i class="fa fa-trash"></i></a>' : '');
                                      
                        var file = '<tr>' +
                                        '<td>' + value.file_name + '</td>' +
                                        '<td>' + value.file_size + '</td>' +
                                        '<td>' +
                                            '<a class="tooltipped btn btn-success viewFiles" title="Preview" href="/storage/' + value.id + '/' + value.file_name + '" target="_blank" data-id="' + value.id + '"><i class="fa fa-eye"></i></a> ' +
                                            '<a class="tooltipped btn btn-primary downloadFiles" title="Download" href="/api/downloadFile/' + value.id + '" data-id="' + value.id + '"><i class="fa fa-cloud-download"></i></a> ' +
                                            actions +
                                        '</td>' +
                                    '</tr>';

                        $('.tblAttachments').append(file);            
                    });

                    $('.tooltipped').attr('data-placement', 'top');
                    $('.tooltipped').tooltip();

                } else {
                    $('.tblAttachments tbody tr').replaceWith('');
                }                
            },
            error: function(response) {
                console.log('Error: ' + response.status);
            }
        });
}

function loadCustomerAttachmentTypeTable(tableName, category) {
    var api = $(tableName).data('api');

    var table = $(tableName).DataTable({
        'ajaxSource'  : api + '/' + category,
        'columns'     : [
            { 'data':  'desc' },
            { 'data':  'total_files' ,
            'render': function(data, type, row){
                return '<p style="text-align: center;">' + row.total_files + '</p>';
            }},
            { 'data':   'id',
            'render': function(data, type, row){
                var actions; 
                var viewAttachments= '';

                viewAttachments = '<a class="tooltipped btn btn-success viewAttachments" href="#" title="View Attachments" data-id="' + row.id + '" data-description="' + row.desc + '" data-category="' + row.category + '"><i class="fa fa-folder"></i></a> ';

                actions = viewAttachments;
                
                return actions;
            } },
        ],
        'lengthChange': false,        
        //'lengthMenu'  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'paging'      : false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : false,
        'autoWidth'   : false
    });

    // Initialize tooltip on datatable redraw
    table.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    return table;
}

function reloadAttachmentTypeDataTable(dataTable, table, category) {
    // reload customer attachment type table.
    dataTable.ajax.url( $(table).data('api') + '/' + category ).load(); 
}

// Customer Photo
function loadCustomerPhoto(cid) {
    $.ajax({
        type: 'GET',
        url: '/api/customerphoto/' + cid + '/6', // 6 = Model ID => Photo
        success: function(response){
            if(response.data==null) {
                $('#customer_photo').attr('src', '/img/user-mb-128x128.jpg');
                $('#viewCustomerPhoto, #customerProfile a').attr('href', '/img/user-mb-128x128.jpg');
            } else {
                $('#customer_photo').attr('src', '/storage/' + response.data.id + '/' + response.data.file_name);
                $('#viewCustomerPhoto, #customerProfile a').attr('href', '/storage/' + response.data.id + '/' + response.data.file_name);
            }
        },
        error: function(response){
            console.log('ERROR: '+ response.status);
        },
    });
}

// Remove attached file
function attachmentRemoved(id) {
    $.ajax({
        type: 'DELETE',
        url: '/api/removeFiles/' + id,
        success: function(response) {
            toastr.success('File successfully deleted!', 'Success!');
            viewAttachedFiles($('#file_id').val(), $('#file_cid').val(), $('#file_category').val());
            loadCustomerPhoto($('#file_cid').val());
            reloadAttachmentTypeDataTable(cifAttachmentTypeTable, '#customerAttachmentTable', 1);
            reloadAttachmentTypeDataTable(loanAttachmentTypeTable, '#loanAttachmentTable', 2);
            reloadAttachmentTypeDataTable(depAttachmentTypeTable, '#depositAttachmentTable', 3);
        },
        error: function(response) {
            console.log('Error: ' + response.status);
        }
    });
}

// Override Request
$.extend({
    overrideRequest: function(username, password, file_id, custBranchId) {
        $.ajax({
            type: 'POST',
            url: '/api/overrideAttachments/',
            data:   {
                '_token': $('input[name=_token]').val(),
                'username': username,
                'password': password,
                'customerBrID': custBranchId,
            },
        })
        .done(function(response){
            if(response.status == 'Success') {
                attachmentRemoved(file_id);
                $('#overrideModal').modal('hide');
                // setInterval(function() {
                //     cache_clear()
                // }, 2000);
                // toastr.success(response.message, 'Success!');
            } else {
                toastr.error(response.message, 'Error!');
            }
        })
        .fail(function(response){
            console.log('Error: ' + response.status);
        });
    }
});

function cache_clear() {
  // window.location.reload(true);
    window.location.reload(); // use this if you do not remove cache
}

$(document).ready(function(){
// Format Fullname
function formatDisplayName(fname, mname, lname) {
    return fname + ' ' + ((mname) != null ? mname.substr(0, 1) : '') + '. ' + lname;
} 
// Format Date of Birth
function formatDOB(dob) {
    return dob.split(' ', 1);
} 
// Get Age
function getAge(birthDate) {
    return Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10) + ' yrs old'
}  
// Retrieve customer profile data
function displayCustomerInfo(api) {
    $.ajax({
        type: 'GET',
        url:  api,
        success: function(response){
            $('#customerBranch').text(response.data.branch);
            $('#customerId, #txtCustomerId').text(response.data.customer_id);
            $('#txtTitle').text(response.data.title);
            $('#customerName').text(formatDisplayName(response.data.firstname, response.data.middlename, response.data.lastname));
            $('#txtCustomerName').text(response.data.customer_name);
            $('#customerType, #txtCustomerType').text(response.data.customer_type);
            $('#txtCustomerGroup').text(response.data.customer_group);
            $('#txtInitials').text(response.data.initials);
            $('#txtGender').text(response.data.gender);
            $('#txtDateOfBirth').text(formatDOB(response.data.birth_date));
            $('#txtPlaceOfBirth').text(response.data.birth_place);
            $('#txtCustomerGroup').text(response.data.customer_group);
            $('#txtCivilStatus').text(response.data.civil_status);
            $('#txtNationality').text(response.data.nationality);
            $('#customerStatus, #txtStatus').text(response.data.status);
            $('#txtDOSRI').text(response.data.dosri);
            $('#customerAge').text(getAge(formatDOB(response.data.birth_date)));

            $('#customerProfile').attr('data-branch', response.data.branch_id);

            loadCustomerPhoto(response.data.id);

            // assign cid to hidden input file_cid
            $('#file_cid').val(response.data.id);

            $.each(response.address, function(index, value){
                var itemAddress = '<div class="panel box box-success">' +
                                '<div class="box-header with-border">' +
                                '<h4 class="box-title">' +
                                    '<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + value.id + '" aria-expanded="false" class="collapsed text-success">' +
                                    '<i class="fa fa-home margin-r-5"></i> ' + value.type.description +
                                    '</a>' +
                                '</h4>' +
                                '</div>' +
                                '<div id="collapse' + value.id + '" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">' +
                                '<div class="box-body">' + 
                                    '<p><strong>Address: </strong>' + value.address1 + ', ' + value.address2 + ', ' + value.town.description + ' ' + value.address3 + '</p>' + 
                                    '<p><strong>Phone No.: </strong>' + value.phone1 + '</p>' + 
                                    '<p><strong>Resident Since: </strong>' + value.residence_since + '</p>' + 
                                '</div>' +
                                '</div>' +
                            '</div>';

                $('#accordion').append(itemAddress);
            });
            // addresses
            $.each(response.address, function(index, value){
                var itemAddress =   '<div id="item-' + value.id + '">' +
                                    '<ul class="list-unstyled">' +
                                        '<li class="margin-r-10"><strong><i class="fa fa-map-marker margin-r-5"></i> ' + value.type.description + '</strong></li>' +  
                                        '<li class="text-muted"> ' + value.address1 + ', ' + value.address2 + ', ' + value.town.description + ' ' + value.address3 + '</li>' +
                                    '</ul>' +
                                    '<hr>' +
                                    '<ul class="list-unstyled">' +
                                        '<li class="margin-r-10"><strong><i class="fa fa-phone margin-r-5"></i> Phone</strong></li>' + 
                                        '<li class="text-muted"> ' + ((value.phone1) != null ? value.phone1 : 'None') + ' </li>' +  
                                    '</ul>' +
                                    '</div>';

                $('#addressList').append(itemAddress);                    
            });
            // contacts
            $.each(response.contact, function(index, value){
                var icon;

                if(value.type.item_value == 'Cellphone/Mobile') { icon = 'mobile'; }
                else if(value.type.item_value == 'Email') { icon = 'envelope'; }
                else if(value.type.item_value == 'Landline') { icon = 'phone'; }
                else if(value.type.item_value == 'Facebook') { icon = 'facebook-official'; }
                else if(value.type.item_value == 'Twitter') { icon = 'twitter'; }
                else if(value.type.item_value == 'Instagram') { icon = 'instagram'; }
                else { icon = 'home'; }

                var itemContact =   '<strong>' +
                                        '<i class="fa fa-' + icon + ' margin-r-5"></i>' + value.type.item_value +
                                    '</strong>' +
                                        '<ul class="list-unstyled">' +
                                            '<li class="item-1 text-muted margin-r-10"> ' + value.contact_value + ' </li>' +  
                                        '</ul>' +
                                    '<hr>';

                $('#contactList').append(itemContact);                    
            });
            // education
            $.each(response.education, function(index, value){

                var itemEducation =   '<tr>' +
                                        '<td>' + value.type.item_value + '</td>' +
                                        '<td>' + value.school_attended + '</td>' +
                                        '<td>' + value.year_start + '</td>' +
                                        '<td>' + value.year_end + '</td>' +
                                        '<td>' + ((value.education_degree != null) ? value.education_degree : '') + '</td>' +
                                      '</tr>';

                $('#educationalAttainmentTable thead').append(itemEducation);                    
            });
            // employment
            $.each(response.employment, function(index, employment){
                $('#txtEmpName').text(employment.employer_name);
                $('#txtEmpAddress').text(employment.address1 + ' ' + employment.address2 + ' ' + employment.address3);
                $('#txtEmpRegion').text(employment.region.item_value);
                $('#txtEmpDesignation').text(employment.designation);
                $('#txtEmpIDNo').text(employment.employment_id);
                $('#txtEmpDepedID').text(employment.deped_employment_id);
                $('#txtEmpYearStart').text(employment.year_start);
                $('#txtEmpYearEnd').text((employment.year_end) != null ? employment.year_end : '');
                $('#txtEmpCreditSavings').text((employment.debit_account) != null ? employment.debit_account : '');
                $('#txtEmpContactNo').text((employment.contact_no) != null ? employment.contact_no : '');
                $('#txtEmpFaxNo').text((employment.fax_no) != null ? employment.fax_no : '');
                $('#txtEmpEmail').text((employment.e_mail) != null ? employment.e_mail : '');
            });
            // business
            $.each(response.business, function(index, business){
                $('#txtBusName').text((business.name) != null ? business.name : '');
                $('#txtBusAddress').text(((business.address1) != null ? business.address1 : '') + ' ' + ((business.address2) != null ? business.address2 : '')) + ' ' + ((business.address3) != null ? business.address3 : '');
                $('#txtBusRegistration').text((business.registration_date) != null ? business.registration_date : '');
                $('#txtBusActivity').text((business.project) != null ? business.project : '');
                $('#txtBusFaxNo').text((business.fax_no) != null ? business.fax_no : '');
                $('#txtBusEmail').text((business.e_mail) != null ? business.e_mail : '');
            });
            // presented id
            $.each(response.presented_id, function(index, presented_id){

            var itemPresentedId =   '<tr>' +
                                    '<td>' + presented_id.type.item_value + '</td>' +
                                    '<td>' + presented_id.id_no + '</td>' +
                                    '<td>' + ((presented_id.issue_date) != null ? presented_id.issue_date : '') + '</td>' +
                                    '<td>' + ((presented_id.valid_date) != null ? presented_id.valid_date : '') + '</td>' +
                                    '<td>' + ((presented_id.is_govt_issue) == true ? '<i class="fa fa-check-circle text-success"></i>' : '<i class="fa fa-times-circle text-danger"></i>')  + '</td>' +
                                    '<td>' + ((presented_id.is_with_photo) == true ? '<i class="fa fa-check-circle text-success"></i>' : '<i class="fa fa-times-circle text-danger"></i>') + '</td>' +
                                    '<td>' + ((presented_id.is_with_signature) == true ? '<i class="fa fa-check-circle text-success"></i>' : '<i class="fa fa-times-circle text-danger"></i>') + '</td>' +
                                    '<td>' + ((presented_id.education_degree != null) ? presented_id.education_degree : '') + '</td>' +
                                '</tr>';

            $('#presentedIdTable thead').append(itemPresentedId);                    
            });
            // presented id
            $.each(response.relation, function(index, relation){

            var itemRelation =   '<tr>' +
                                    '<td>' + relation.type.item_value + '</td>' +
                                    '<td><a class="text-success" href="/customers/' + relation.relative_info.id + '">' + relation.relative_info.customer_id + '</a></td>' +
                                    '<td>' + relation.relative_info.name1 + '</td>' +
                                    '<td>' + relation.relative_info.name2 + '</td>' +
                                    '<td>' + relation.relative_info.name3 + '</td>' +
                                    '<td>' + relation.relative_info.name4 + '</td>' +
                                '</tr>';

            $('#relationTable thead').append(itemRelation);                    
            });

            console.log('Customer profile successfully loaded.');
        },
        error: function(response){
            console.log('Error: ' + response.status);
        }
    });
}

// Initialize Toastr
toastr.options.positionClass = 'toast-bottom-right';

// Admin / ICBS - New Accounts user roles in uploading profile picture
(($('#customerProfile').data('can_create') == 1) ? $('#upload_form').removeClass('hidden') : $('#upload_form').addClass('hidden'))

// Display Customer Information
displayCustomerInfo($('#customerProfile').data('api'));

});

// view attachment action button is clicked
$(document).on('click', '.viewAttachments', function(){
    $('h4#modal-header-title').text('Attachment Type: ' + $(this).data('description'));

    attachmentType = $(this).data('id');
    attachmentCategory = $(this).data('category');
    
    // assign attached to hidden input file_cid
    $('#file_id').val(attachmentType);
    $('#file_category').val(attachmentCategory);

    // Retrieve attached files
    viewAttachedFiles(attachmentType, $('#file_cid').val(), attachmentCategory);

    // Show Modal
    $('#viewAttachmentType').modal({
                      show:  true,
                      backdrop: 'static',
                      keyboard: false,
                  });


});

// Remove attached file
$(document).on('click', '.deleteFiles', function() {
    deleteFileId = $(this).data('id');
    filename = $(this).data('filename');
    custBranchId = $('#customerProfile').data('branch');
    
    if($('.tblAttachments').data('is_admin')==1) {
        attachmentRemoved(deleteFileId);
    } else {
        // show confirm delete modal
        $('h4#deleteModal-header-title').text('Confirm Delete Attachment');
        $('#itemName').text(filename);
        // $('#viewAttachmentType').modal('hide');
        $('#deleteItemModal').modal('show');
    }
});
// Confirm delete attached file
$(document).on('click', '.btnConfirmDeleteFile', function(){
    // show override modal
    $('#deleteItemModal').modal('hide');
    $('#overrideModal-header-title').text('Override Credentials');
    $('#overrideModal input').each(function(){
        $(this).val('');
    });
    $('#overrideModal').modal('show');
});
// Override on confirm delete
$(document).on('click', '.btnOverride', function(){
    username = $('#username').val();
    password = $('#password').val();
    
    if(username.length < 1 || password.length < 1) {
        toastr.error('Please fill-in override credentials to proceed.', 'Error!');
    } else {
        $.overrideRequest(username, password, deleteFileId, custBranchId);
    }

});
// Successful upload
$(document).on('click', '#confirmSuccess', function(){
    $('.dz-complete').remove();
    $('.dz-message').attr('style','display:block');
    $('#confirmModal').modal('hide');
});

</script>
@endsection
