@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@section('title', 'Pay Grades')

@section('page_title', 'Pay Grades')

@section('breadcrumb_title', 'Pay Grades')

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="{{ route('paygrade.create') }}" class="btn btn-success">Create Pay Grade</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    @if (count($pay_grades) > 0)  
        <table id="mbTbl1" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>ID</th>
              <th>Pay Grade</th>
              <th>Mininum Salary</th>
              <th>Maximum Salary</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($pay_grades as $pay_grade)  
            <tr>
              <td><a class="text-success" href="{{ route('paygrade.edit', ['id' => $pay_grade->id]) }}">{{ $pay_grade->id }}</a></td>
              <td><a class="text-success" href="{{ route('paygrade.edit', ['id' => $pay_grade->id]) }}">{{ $pay_grade->desc }}</a></td>
              <td>{{ number_format($pay_grade->min_salary, 2) }}</td>
              <td>{{ number_format($pay_grade->max_salary, 2) }}</td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Pay Grade</th>
              <th>Mininum Salary</th>
              <th>Maximum Salary</th>
            </tr>
          </tfoot>
        </table>  <!-- / table -->  
    @endif      
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#mbTbl1').DataTable()
    $('#mbTbl2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection