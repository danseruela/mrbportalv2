@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@section('title', 'User List')

@section('page_title', 'User List')

@section('breadcrumb_title', 'User List')

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="{{ route('user.create') }}" class="btn btn-success">Create User</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    @if (count($users) > 0)  
        <table id="userTbl1" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>Logged In</th>
              <th>Employee Name</th>
              <th>Username</th>
              <th>Branch</th>
              <!-- <th>Roles</th> -->
              <th>Status</th>
              <th style="width: 14%;">Actions</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($users as $user)  
            <tr class="item{{ $user->id }}">
              <td>
              @if($user->isOnline())
                <i class="fa fa-circle text-success"></i> Online  
              @else
                <i class="fa fa-circle text-danger"></i> Offline  
              @endif
              </td>
              <td><a class="text-success" href="#">{{ $user->employee->lastname }}, {{ $user->employee->firstname }} {{ $user->employee->middlename }}</a></td>
              <td>{{ $user->username }}</td>
              <td>{{ $user->employee->getLocation->desc }}</td>
              <!--<td>
              @foreach($user->roles as $role)
                <span class="badge bg-green">{{ $role->name }}</span>
              @endforeach  
              </td> -->
              <td><span class="badge {{ $user->deleted == 0 ? 'bg-green' : 'bg-red' }}">{{ $user->deleted == 0 ? 'Active' : 'Inactive' }}</span></td>
              <td>
                  <a href="#" data-toggle="tooltip" title="Reset Password" class="reset-password btn btn-primary" data-reset-id="{{ $user->id }}" data-reset-username="{{ $user->username }}"><i class="fa fa-key"></i></a>
                  <a href="#" data-toggle="tooltip" title="Edit" class="edit-user btn btn-warning" data-id="{{ $user->id }}" data-emp-id="{{ $user->emp_id }}" data-username="{{ $user->username }}" data-status="{{ $user->deleted }}"><i class="fa fa-edit"></i></a>                   
                  <a href="#" data-toggle="tooltip" title="Delete" class="delete-user btn btn-danger" data-del-id="{{ $user->id }}" data-del-username="{{ $user->username }}"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Employee Name</th>
              <th>Username</th>
              <th>Roles</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </tfoot>
        </table>  <!-- / table -->  
    @endif      
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->

@include('partials._modal-user')
    
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- passwordGenerator -->
<script src="{{ asset('custom/js/passwordGenerator.js') }}"></script>
<!-- page script -->
<script src="{{ asset('custom/js/userValidation.js') }}"></script>
@endsection