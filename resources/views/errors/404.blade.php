<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Page Not Found</title>

    @include('partials.header_assets')
</head>
<body class="hold-transition skin-green login-page">
<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-wrapper"> -->

    <!-- Main content -->
    <section class="content">
      <div class="error-page text-center" style="margin: 80px auto 0 auto;">
        <h2 class="headline text-red" style="float: none; font-weight: inherit; font-size: 150px;"> 404</h2>

        <div class="error-content" style="margin-left: 0;">
          <h3><i class="fa fa-warning text-red"></i> Oops! Page not found.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="/dashboard" class="text-success">return to dashboard</a>.
          </p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  <!-- </div> -->
  <!-- /.content-wrapper -->

</body>
</html>