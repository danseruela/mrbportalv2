<!DOCTYPE html>
<html lang="en">
<head>
	<!-- START META SECTION -->
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> <!-- Tell the browser to be responsive to screen width -->
	<meta name="description" content="Mactanbank - Bridge for a better life.">
    <meta name="keywords" content="Mactanbank, Mactan Bank, Mactan Rural Bank, MRB, Mr. Yu Group of Companies">
    <!-- <meta name="_token" content="{{ csrf_token() }}"/> CSFR token for ajax call -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ env('APP_NAME') }} - @yield('title')</title>

	<!-- app.scss -->
	<link rel="stylesheet" href="{{ asset('sass/app.scss') }}">

	<!-- Favicon Logo -->
    <link rel="icon" href="{{ asset('img/mactanbank-icon-32x32.png') }}">

	@yield('css_styles')
</head>
<body>
<div class="wrapper">

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
			    
        
	    <!-- Main content -->
    	<section class="content">
    		@yield('content')
            
    	</section> <!-- / .content -->
	</div> <!-- / .content-wrapper -->

	<!-- @include('partials.sidebar_control') -->
</div> <!-- / .wrapper -->	

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>