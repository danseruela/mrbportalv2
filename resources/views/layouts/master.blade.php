<!DOCTYPE html>
<html lang="en">
<head>
	<!-- START META SECTION -->
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> <!-- Tell the browser to be responsive to screen width -->
	<meta name="description" content="Mactanbank - Bridge for a better life.">
    <meta name="keywords" content="Mactanbank, Mactan Bank, Mactan Rural Bank, MRB, Mr. Yu Group of Companies">
    <meta name="_token" content="{{ csrf_token() }}"/> <!-- CSFR token for ajax call -->
	<title>{{ env('APP_NAME') }} - @yield('title')</title>

	@include('partials.header_assets')

	@yield('css_styles')
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
	@include('partials.header_main')

	@include('partials.sidebar')

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
			@yield('callout')
			<!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        @yield('page_title')
	        <small>Control panel</small>
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li class="active">@yield('breadcrumb_title')</li>
	      </ol>
	    </section>

	    <!-- Main content -->
    	<section class="content">
    		@yield('content')
    	</section> <!-- / .content -->
	</div> <!-- / .content-wrapper -->

	@include('partials.footer')	

	@include('partials.sidebar_control')
</div> <!-- / .wrapper -->	

@include('partials.corePlugins')

@yield('scripts')
</body>
</html>