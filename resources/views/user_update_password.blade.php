@extends('layouts.master')

@section('title', 'Update Password')

@section('page_title', 'Update Password')

@section('breadcrumb_title', 'Update Password')

@section('content')
<div class="row">
<div class="col-xs-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
          {{ csrf_field() }}  
      <div class="box-body">
      <div class="row">  
      <div class="col-xs-6">  
        <div class="form-group">
          <label for="old_password">Old Password</label>
          {!! Form::text('password', null, [
                            'id' => 'old_password', 
                            'class' => 'form-control', 
                            'placeholder' => 'Old Password', 
                            'required'
                            ]) !!}
          <div class="text-danger" id="errPassMatchMsg" style="margin-top: 8px !important;"></div>
        </div> 
        <div class="form-group">
          <label for="password">New Password</label>
          {!! Form::text('password', null, [
                            'id' => 'password', 
                            'class' => 'form-control', 
                            'placeholder' => 'New Password', 
                            'required'
                            ]) !!}
          <div class="text-danger" id="errPassMatchMsg" style="margin-top: 8px !important;"></div>
        </div>
        <div class="form-group">
          <label for="confirm_password">Confirm Password</label>
          {!! Form::text('confirm_password', null, [
                            'id' => 'confirm_password', 
                            'class' => 'form-control', 
                            'placeholder' => 'Confirm Password', 
                            'required'
                            ]) !!}
          <div class="text-danger" id="errPassMatchMsg" style="margin-top: 8px !important;"></div>
        </div>
      </div>  <!-- / .col-xs-6 -->
      </div> <!-- / .row -->
      <div class="row">  
      <hr> 
         
      </div>  
      </div> <!-- / .row -->
      </div>  <!-- / .box-body --> 
    {!! Form::close() !!}   
    </div>  <!-- / .box -->  
</div> <!-- / .col-xs-12 -->
</div>  <!-- / .row -->
@endsection

<!-- Form request error messages -->
<!-- @include('partials.error') -->

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- passwordGenerator -->
<script src="{{ asset('custom/js/passwordGenerator.js') }}"></script>
<script>
  // Create a new password
  $(".getNewPass").click(function(){
    //$("#save-password").attr('data-dismiss', 'modal');
    var field = $(this).closest('div').find('input[rel="gp"]');
    field.val(randString(field));
  });

  // Auto Select Pass On Focus
  $('input[rel="gp"]').on("click", function () {
    $(this).select();
  });

  $(document).ready(function() {
      //Initialize Select2 Elements
      $('#emp_id').select2();

      $('#user_roles').select2({
        placeholder: "Choose User Roles",
      });

      //Initialize status value
      if ($('#deleted').val() != null) {
        $("#status").select2().select2('val',$('#deleted').val()); 
      }

      //Check password length.
      $("#password").blur(checkPasswordLength);

      //Check password matched.
      $("#password_confirmation").keyup(checkPasswordMatch);

  });    
</script>  
@endsection