@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('title', $page_title)

@section('page_title', $page_title)

@section('breadcrumb_title', $page_title)

@section('content')

<div class="row">
<div class="col-xs-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#countries" data-toggle="tab" aria-expanded="true">Watchlist Countries</a>
            </li>
            <li>
                <a href="#corporation" data-toggle="tab" aria-expanded="false">Watchlist Corporation</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="countries">
                @can('amla.create', auth()->user())
                    <a href="#" id="addItem1" data-group="countries" class="btn btn-success">Create {{ $page_title }}</a>
                @endcan
                <table id="amlaGroupTable1" class="table table-bordered table-hover dt-responsive" 
                       data-api="{{ $api_countries }}" 
                       data-item="{{ $page_title }}" 
                       data-can_update="{{ $user->hasAnyRole(['Administrator', 'Content Manager - AMLA', 'Contributor - AMLA']) ? 1 : 0 }}"
                       data-can_delete="{{ $user->hasAnyRole(['Administrator', 'Content Manager - AMLA']) ? 1 : 0 }}"
                >
                    <thead>
                        <tr>
                        <th width="50%">Name of Country</th>
                        <th>Date Issued</th>
                        <th>Created By</th>
                        <th>Modified By</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                </table> 
            </div>
            <div class="tab-pane" id="corporation">
                @can('amla.create', auth()->user())
                    <a href="#" id="addItem2" data-group="corporation" class="btn btn-success">Create {{ $page_title }}</a>
                @endcan
                <table id="amlaGroupTable2" class="table table-bordered table-hover dt-responsive" 
                       data-api="{{ $api_corporation }}" 
                       data-item="{{ $page_title }}" 
                       data-can_update="{{ $user->hasAnyRole(['Administrator', 'Content Manager - AMLA', 'Contributor - AMLA']) ? 1 : 0 }}"
                       data-can_delete="{{ $user->hasAnyRole(['Administrator', 'Content Manager - AMLA']) ? 1 : 0 }}"
                >
                    <thead>
                        <tr>
                        <th width="50%">Name of Company with Cease and Desist Order</th>
                        <th>Date Issued</th>
                        <th>Created By</th>
                        <th>Modified By</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>  <!-- / .col -->
</div>  <!-- / .row -->

@include('partials.modals.setupAmlaGroupModal')

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    // Initialize add button text
    var api_tab1 = $('#amlaGroupTable1').data('api');
    var title_tab1 = $('#amlaGroupTable1').data('item');

    var api_tab2 = $('#amlaGroupTable2').data('api');
    var title_tab2 = $('#amlaGroupTable1').data('item');

    // Initialize table
    var table1 = $('#amlaGroupTable1').DataTable({
        'ajaxSource'  : api_tab1,
        'columns'     : [
            { 'data':  'desc' },
            { 'data':  'date_issued' },
            { 'data':  'created_by' },
            { 'data':  'modified_by' },
            { 'data':   'id',
              'render': function(data, type, row){
                  var actions; 
                  var a_view = ''
                  var a_update = ''
                  var a_delete = '';

                  if($('#amlaGroupTable1').data('can_update') == 1) {
                    a_update = '<a class="tooltipped btn btn-primary editItem" href="#" title="Edit Item" data-id="' + row.id + '" data-desc="' + row.desc + '" data-type="' + row.type + '" data-dateissued="' + row.date_issued + '"><i class="fa fa-edit"></i></a> '; 
                  } 
                  if ($('#amlaGroupTable1').data('can_delete') == 1) {
                    a_delete = '<a class="tooltipped btn btn-danger deleteItem" href="#" title="Delete Item" data-id="' + row.id + '" data-desc="' + row.desc + '" data-type="' + row.type + '"><i class="fa fa-trash"></i></a> ';
                  } 
                  if ($('#amlaGroupTable1').data('can_update') == 0 && $('#amlaGroupTable1').data('can_delete') == 0) {
                    a_view = '<a class="tooltipped btn btn-success viewDetails" href="#" title="View Details" data-id="' + row.id + '" data-type="' + row.type + '"><i class="fa fa-eye"></i></a> ';
                  }

                  actions = a_view + a_update + a_delete;
                  
                  return actions;
            } },
        ],
        'lengthChange': false,        
        'paging'      : true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    var table2 = $('#amlaGroupTable2').DataTable({
        'ajaxSource'  : api_tab2,
        'columns'     : [
            { 'data':  'desc' },
            { 'data':  'date_issued' },
            { 'data':  'created_by' },
            { 'data':  'modified_by' },
            { 'data':   'id',
              'render': function(data, type, row){
                  var actions; 
                  var a_view = ''
                  var a_update = ''
                  var a_delete = '';

                  if($('#amlaGroupTable2').data('can_update') == 1) {
                    a_update = '<a class="tooltipped btn btn-primary editItem" href="#" title="Edit Item" data-id="' + row.id + '" data-desc="' + row.desc + '" data-type="' + row.type + '" data-dateissued="' + row.date_issued + '"><i class="fa fa-edit"></i></a> '; 
                  } 
                  if ($('#amlaGroupTable2').data('can_delete') == 1) {
                    a_delete = '<a class="tooltipped btn btn-danger deleteItem" href="#" title="Delete Item" data-id="' + row.id + '" data-desc="' + row.desc + '" data-type="' + row.type + '"><i class="fa fa-trash"></i></a> ';
                  } 
                  if ($('#amlaGroupTable2').data('can_update') == 0 && $('#amlaGroupTable2').data('can_delete') == 0) {
                    a_view = '<a class="tooltipped btn btn-success viewDetails" href="#" title="View Details" data-id="' + row.id + '" data-type="' + row.type + '"><i class="fa fa-eye"></i></a> ';
                  }

                  actions = a_view + a_update + a_delete;
                  
                  return actions;
            } },
        ],
        'lengthChange': false,        
        'paging'      : true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    // Initialize tooltip on datatable redraw
    table1.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    table2.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    // Initialize Toastr
    toastr.options.positionClass = 'toast-bottom-right';
    

    // Retrieve specified AMLA 
    function showAmlaGroupDetails(id) {
        $.ajax({
            type: 'GET',
            url:   '/api/amlagroup/' + id,
            success: function(response) {
                // Assign value to input field.
                $('#desc').val(response.data.desc);
                $('#date_issued').val(response.data.date_issued);
                $('#remarks').val(response.data.remarks);
                
                // Show Modal
                $('#setupAmlaGroupModal').modal({
                    show:  true,
                    backdrop: 'static',
                    keyboard: false,
                });
                $('#setupAmlaGroupModal').on('shown.bs.modal', function () {
                    $('#desc').focus();
                });
                console.log('Data populated to fields.');

            },
            error: function(response) {
                console.log('Error: ' + response.status)
            },
        });
    }

    // Add button is clicked
    $(document).on('click', '#addItem1, #addItem2', function(){
        if(this.id == 'addItem1') {
            $('#modal-header-title').text($(this).text() + ' (Countries)');
            type = $(this).data('group');
        } 
        if(this.id == 'addItem2') {
            $('#modal-header-title').text($(this).text() + ' (Corporation)');
            type = $(this).data('group');
        }
        
        $('#btnPost span').text(' Save');
        $('#btnPost').addClass('btnSave');
        $('#btnPost').removeClass('btnUpdate');
        
        $('.id_no').addClass('hidden');
        $('#desc').val('');
        $('#date_issued').val('');
        $('#remarks').val('');

        // Initialize Modal
        $('#setupAmlaGroupModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
        $('#setupAmlaGroupModal').on('shown.bs.modal', function () {
            $('#desc').focus();
        });
    });
    // Item Saved
    $(document).on('click', '.btnSave', function(){
        $.ajax({
            type:   'POST',
            url:    'api/amlagroup',
            data:   {
                '_token': $('input[name=_token]').val(),
                'desc': $('#desc').val(),
                'date_issued': $('#date_issued').val(),
                'type': type,
                'remarks': $('#remarks').val(),
                'user': $(this).data('user'),
            },
            success: function(response){
                // reload datatable
                table1.ajax.url( api_tab1 ).load();
                table2.ajax.url( api_tab2 ).load();
                $('#setupAmlaGroupModal').modal('hide');
                toastr.success('Setup item successfully created!', 'Success!');
                console.log('Setup item successfully created.');
            },
            error: function(response){
                console.log('Error: ' + response.status);
            }
        });
    });
    // View button is clicked
    $(document).on('click', '.viewDetails', function(){
        if($(this).data('type') == 'Watchlist Countries') {
            $('#modal-header-title').text('View: ' + title_tab1 + ' (Countries)');
        } 
        if($(this).data('type') == 'Watchlist Corporation') {
            $('#modal-header-title').text('View: ' + title_tab2 + ' (Corporation)');
        }
        $('#btnPost').addClass('hidden');

        // retrieve remote data.
        showAmlaGroupDetails($(this).data('id'));
    });
    // Edit button is clicked
    $(document).on('click', '.editItem', function(){
        if($(this).data('type') == 'Watchlist Countries') {
            $('#modal-header-title').text('Edit ' + title_tab1 + ' (Countries)');
        } 
        if($(this).data('type') == 'Watchlist Corporation') {
            $('#modal-header-title').text('Edit ' + title_tab2 + ' (Corporation)');
        }

        $('#btnPost span').text(' Update');
        $('#btnPost').addClass('btnUpdate');
        $('#btnPost').removeClass('btnSave');
        // retrieve remote data.
        showAmlaGroupDetails($(this).data('id'));
        
        id = $(this).data('id');
    });
    // Update Item
    $(document).on('click', '.btnUpdate', function(){
        $.ajax({
            type:   'PUT',
            url:    'api/amlagroup/' + id,
            data:   {
                '_token': $('input[name=_token]').val(),
                'desc': $('#desc').val(),
                'date_issued': $('#date_issued').val(),
                'remarks': $('#remarks').val(),
                'user': $(this).data('user'),
            },
            success: function(response){
                // reload datatable
                table1.ajax.url( api_tab1 ).load();
                table2.ajax.url( api_tab2 ).load();
                $('#setupAmlaGroupModal').modal('hide');
                toastr.success('Setup item successfully updated!', 'Success!');
                console.log('Setup item successfully updated.');
            },
            error: function(response){
                console.log('Error: ' + response.status);
            }
        });
    });

    // Delete Item Modal
    $(document).on('click', '.deleteItem', function(){
        id = $(this).data('id');
        desc = $(this).data('desc');

        if($(this).data('type') == 'Watchlist Countries') {
            $('#deleteModal-header-title').text('Delete ' + title_tab1 + ' (Countries)');
        } 
        if($(this).data('type') == 'Watchlist Corporation') {
            $('#deleteModal-header-title').text('Delete ' + title_tab2 + ' (Corporation)');
        }

        $('#itemName').text(desc);

        $('#deleteItemModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
    });
    // Confirm Delete Item
    $(document).on('click', '#btnConfirmDelete', function(){
        $.ajax({
            type: 'DELETE',
            url:  'api/amlagroup/' + id,
            data: {
            '_token': $('input[name=_token]').val(),
            },
            success:  function(response){
                if(response.error==null) {
                    table1.ajax.url( api_tab1 ).load();
                    table2.ajax.url( api_tab2 ).load();
                    console.log('Setup item successfully deleted.');
                    toastr.success('Setup item has been deleted!', 'Success!')
                } else {
                    toastr.warning(response.error, "Warning!");
                    console.log(response.error);
                }
            },
            error:  function(response){
            console.log('Error: ' + response.status)
            }
        });

        $('#deleteItemModal').modal('hide');
    });

  });
</script>
@endsection