@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@section('title', 'Job Titles')

@section('page_title', 'Job Titles')

@section('breadcrumb_title', 'Job Titles')

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="{{ route('title.create') }}" class="btn btn-success">Create Job Title</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    @if (count($job_titles) > 0)  
        <table id="mbTbl1" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>ID</th>
              <th>Job Title</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($job_titles as $job_title)  
            <tr>
              <td><a class="text-success" href="{{ route('title.edit', ['id' => $job_title->id]) }}">{{ $job_title->id }}</a></td>
              <td><a class="text-success" href="{{ route('title.edit', ['id' => $job_title->id]) }}">{{ $job_title->desc }}</a></td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Job Title</th>
            </tr>
          </tfoot>
        </table>  <!-- / table -->  
    @endif      
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#mbTbl1').DataTable()
    $('#mbTbl2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection