@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('title', $page_title)

@section('page_title', $page_title)

@section('breadcrumb_title', $page_title)

@section('content')

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    @can('amla.create', auth()->user())
    <div class="box-header">
      <a href="#" id="addItem" class="btn btn-success">Create {{ $page_title }}</a>
    </div>
    @endcan
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row report-filter">
        <div class="col-xs-2">
            <strong>Group By Type:</strong>
        </div>
        <div class="col-xs-4">
            <p> 
            @if (count($amla_types) > 0)
                {!! Form::select('grp_amla_type', $amla_types, null, ['id' => 'grp_amla_type', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            @endif
            </p>
        </div>
        </div> 
        @php
          $group = !empty($request->id) ? $request->id : 0;
        @endphp 
        <table id="amlaTable" class="table table-bordered table-hover dt-responsive" 
               data-api="{{ $api }}" 
               data-item="{{ $page_title }}" 
               data-group="{{$group}}" 
               data-can_update="{{ $user->hasAnyRole(['Administrator', 'Content Manager - AMLA', 'Contributor - AMLA']) ? 1 : 0 }}"
               data-can_delete="{{ $user->hasAnyRole(['Administrator', 'Content Manager - AMLA']) ? 1 : 0 }}"
        >
          <thead>
            <tr>
              <th>Fullname</th>
              <th>Type</th>
              <th>Region</th>
              <th>Position</th>
              <th>Created By</th>
              <th>Modified By</th>
              <th width="10%">Actions</th>
            </tr>
          </thead>
        </table>  <!-- / table -->  
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->

@include('partials.modals.setupAmlaModal')

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('custom/js/select2dropdown.js') }}"></script>
<!-- page script -->
<script>
$(document).ready(function(){
    // Initialize add button text
    var api = $('#amlaTable').data('api');
    var title = $('#amlaTable').data('item');

    var api_relation = $('#relationTable').data('api');

    //Initialize Select2 Elements
    $('#grp_amla_type').select2();
    $("#grp_amla_type").on("select2:open", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Select an option...");
    });

    $("#grp_amla_type").on("select2:close", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });
    // Set new data to append
    var newOption = new Option('All', 0, true, true);
    // Append it to the select
    $('#grp_amla_type').append(newOption).trigger('change');
    // Check if there is a request id
    var groupBy = $('#amlaTable').data('group');
    if(groupBy!=null) {
      $('#grp_amla_type').val(groupBy).trigger('change');
    }

    // Initialize table
    var table = $('#amlaTable').DataTable({
        'ajaxSource'  : api,
        'columns'     : [
            { 'data':  'fullname' },
            { 'data':  'type' },
            { 'data':  'region' },
            { 'data':  'position' },
            { 'data':  'created_by' },
            { 'data':  'modified_by' },
            { 'data':   'id',
              'render': function(data, type, row){
                  var actions; 
                  var a_view = ''
                  var a_update = ''
                  var a_delete = '';

                  if($('#amlaTable').data('can_update') == 1) {
                    a_update = '<a class="tooltipped btn btn-primary editDetails" href="#" title="Edit Details" data-id="' + row.id + '"><i class="fa fa-edit"></i></a> '; 
                  } 
                  if ($('#amlaTable').data('can_delete') == 1) {
                    a_delete = '<a class="tooltipped btn btn-danger deleteItem" href="#" title="Delete Item" data-id="' + row.id + '"><i class="fa fa-trash"></i></a> ';
                  } 
                  if ($('#amlaTable').data('can_update') == 0 && $('#amlaTable').data('can_delete') == 0) {
                    a_view = '<a class="tooltipped btn btn-success viewDetails" href="#" title="View Details" data-id="' + row.id + '"><i class="fa fa-eye"></i></a> ';
                  }

                  actions = a_view + a_update + a_delete;
                  
                  return actions;
            } },
        ],
        'lengthChange': false,        
        //'lengthMenu'  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'paging'      : true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    var tableRelation = $('#relationTable').DataTable({
        //'ajaxSource'  : api_relation + response.data.id,
        'columns'     : [
            { 'data':  'fullname' },
            { 'data':  'type' },
            { 'data':  'consanguinity' },
            { 'data':   'id',
            'render': function(data, type, row){
                var actions;
                  if($('#amlaTable').data('can_update') == 1) {
                    actions = '<a class="tooltipped btn btn-primary editRelation" href="#" title="Edit Details" data-id="' + row.id + '"><i class="fa fa-edit"></i></a> ' +
                              '<a class="tooltipped btn btn-danger deleteRelation" href="#" title="Delete Item" data-id="' + row.id + '"><i class="fa fa-trash"></i></a> ';
                  } else {
                    actions = '';
                  }
                  
                  return actions;
            } },
        ],
        'lengthChange': false,        
        //'lengthMenu'  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'paging'      : true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    // Initialize tooltip on datatable redraw
    table.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    tableRelation.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    // Initialize Toastr
    toastr.options.positionClass = 'toast-bottom-right';
    
    // Initialize Dropdown menus
    dropdownSelect($('#organization'), '/api/organization');
    dropdownSelect($('#region'), '/api/region');
    dropdownSelect($('#amla_type'), '/api/amlatype');
    dropdownSelect($('#info_source'), '/api/infosource');
    dropdownSelect($('#info_source_type'), '/api/infosourcetype');
    dropdownSelect($('#rel_type'), '/api/rel_type');
    dropdownSelect($('#rel_consanguinity'), '/api/rel_consanguinity');
    
    var selectUI = $('#crimes').select2();

    // Select group type sorting
    $(document).on('change', '#grp_amla_type', function(){
      table.ajax.url( api + $(this).val() + '/group' ).load();
    });

    // Retrieve specified AMLA 
    function showAmlaDetails(id) {
    $.ajax({
            type: 'GET',
            url:   api + id,
            success: function(response) {
                // Assign value to input field.
                $('#firstname').val(response.data.firstname);
                $('#middlename').val(response.data.middlename);
                $('#lastname').val(response.data.lastname);
                $('#organization').val(response.data.organization).trigger('change');
                $('#position').val(response.data.position);
                $('#barangay').val(response.data.barangay);
                $('#city').val(response.data.city);
                $('#province').val(response.data.province);
                $('#postal_code').val(response.data.postal_code);
                $('#region').val(response.data.region).trigger('change');
                $('#amla_type').val(response.data.amla_type).trigger('change');
                $('#info_source').val(response.data.info_source).trigger('change');
                $('#info_source_type').val(response.data.info_source_type).trigger('change');
                $('#remarks').val(response.data.remarks);

                var relationAmlaId = response.data.id;
                var crimes = [];

                // populate crimes                
                $.each(response.crimes, function(index, value) {
                    crimes.push(value.pivot.crime_id);
                });
                
                $('#crimes').val(crimes).trigger('change');
                
                // Display relations table
                $('.nav-tabs li a').each(function(){
                    if($(this).text() == 'Family Relations') {
                        $(this).removeClass('hidden');
                    }
                });
                $('#relations').removeClass('hidden');
                
                // Show Modal
                $('#setupAmlaModal').modal({
                    show:  true,
                    backdrop: 'static',
                    keyboard: false,
                });
                $('#setupAmlaModal').on('shown.bs.modal', function () {
                    $('#firstname').focus();
                });
                console.log('Data populated to fields.');

                // reload relations table
                tableRelation.ajax.url( api_relation + relationAmlaId ).load();

            },
            error: function(response) {
                console.log('Error: ' + response.status)
            },
        });
    }

    // Add button is clicked
    $(document).on('click', '#addItem', function(){
        $('#modal-header-title').text($(this).text());
        $('#btnPost span').text(' Save');
        $('#btnPost').addClass('btnSave');
        $('#btnPost').removeClass('btnUpdate');
        
        $('.id_no').addClass('hidden');
        $('#setupAmlaModal input').each(function(){
            $(this).val('');
        });
        $('#setupAmlaModal textarea').each(function(){
            $(this).val('');
        });
        $('#setupAmlaModal select').each(function(){
            $(this).val(0).trigger('change');
        });

        // Initialize Modal
        $('#setupAmlaModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
        $('#setupAmlaModal').on('shown.bs.modal', function () {
            $('#firstname').focus();
        });

        $('#defaultTab a[href="#personal"]').tab('show');

        // Family Relations Tab for initial amla setup
        $('.nav-tabs li a').each(function(){
            if($(this).text() == 'Family Relations') {
                $(this).addClass('hidden');
            }
        });
        $('#relations').addClass('hidden');

    });
    // Item Saved
    $(document).on('click', '.btnSave', function(){
        if($('#firstname').val() == '' || 
           $('#middlename').val() == '' ||
           $('#lastname').val() == '' ||
           $('#amla_type').val() == null ||
           $('#organization').val() == null ||
           $('#position').val() == '' ) {

            toastr.warning('Please supply empty fields.', 'Warning!');
        } else {
            $.ajax({
                type:   'POST',
                url:    api,
                data:   {
                    '_token': $('input[name=_token]').val(),
                    'firstname': $('#firstname').val(),
                    'middlename': $('#middlename').val(),
                    'lastname': $('#lastname').val(),
                    'amla_type': $('#amla_type').val(),
                    'organization': $('#organization').val(),
                    'position': $('#position').val(),
                    'barangay': $('#barangay').val(),
                    'city': $('#city').val(),
                    'province': $('#province').val(),
                    'postal_code': $('#postal_code').val(),
                    'region': $('#region').val(),
                    'crimes': $('#crimes').val(),
                    'info_source': $('#info_source').val(),
                    'info_source_type': $('#info_source_type').val(),
                    'remarks': $('#remarks').val(),
                    'user': $(this).data('user'),
                },
                success: function(response){
                    // reload datatable
                    table.ajax.url( api ).load();
                    $('#setupAmlaModal').modal('hide');
                    toastr.success('Setup item successfully created!', 'Success!');
                    console.log('Setup item successfully created.');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }
    });
    // View button is clicked
    $(document).on('click', '.viewDetails', function(){
        $('#modal-header-title').text('View: ' + title);
        $('#btnPost').addClass('hidden');
        $('#addRelation').addClass('hidden');
        // reset tabs to default
        $('.nav-tabs li, .tab-pane').each(function(){
            $(this).removeClass('active');
        });
        // $('#defaultTab, #personal').addClass('active');
        $('#defaultTab a[href="#personal"]').tab('show');
        // retrieve remote data
        showAmlaDetails($(this).data('id'));
    });
    // Edit button is clicked
    $(document).on('click', '.editDetails', function(){
        $('#modal-header-title').text('Edit: ' + title);
        $('#btnPost span').text(' Update');
        $('#btnPost').addClass('btnUpdate');
        $('#btnPost').removeClass('btnSave hidden');
        $('#addRelation').removeClass('hidden');
        // reset tabs to default
        $('.nav-tabs li, .tab-pane').each(function(){
            $(this).removeClass('active');
        });
        // $('#defaultTab, #personal').addClass('active');
        $('#defaultTab a[href="#personal"]').tab('show');
        // retrieve remote data
        showAmlaDetails($(this).data('id'));

        updateItemId = $(this).data('id');

        // Show Add Relation Modal
        $(document).on('click', '#addRelation', function(){
            $('#setupRelationModal h4').text('Add Family Relation');
            $('#submit-relation span').text(' Save');
            $('#submit-relation').addClass('btnSaveRelation');
            $('#submit-relation').removeClass('btnUpdateRelation');

            $('#setupRelationModal input').each(function(){
                $(this).val('');
            });
            $('#setupRelationModal select').each(function(){
                $(this).val(0).trigger('change');
            });

            $('#setupRelationModal').modal({
                show:  true,
                backdrop: 'static',
                keyboard: false,
            });
            $('#setupRelationModal').on('shown.bs.modal', function () {
                $('#rel_firstname').focus();
            });

            $('#amla_id').val(updateItemId);
        });
        // Edit Relation
        $(document).on('click', '.editRelation', function(){
            $('#setupRelationModal h4').text('Edit Family Relation');
            $('#submit-relation span').text(' Update');
            $('#submit-relation').addClass('btnUpdateRelation');
            $('#submit-relation').removeClass('btnSaveRelation');

            // retrieve remote data
            $.ajax({
                type: 'GET',
                url:   api_relation + $(this).data('id') + '/show',
                success: function(responseGetRelation) {
                    // Assign value to input field.
                    $('#rel_firstname').val(responseGetRelation.data.firstname);
                    $('#rel_middlename').val(responseGetRelation.data.middlename);
                    $('#rel_lastname').val(responseGetRelation.data.lastname);
                    $('#rel_type').val(responseGetRelation.data.type).trigger('change');
                    $('#rel_consanguinity').val(responseGetRelation.data.consanguinity).trigger('change');

                    $('#setupRelationModal').modal({
                        show:  true,
                        backdrop: 'static',
                        keyboard: false,
                    });
                    $('#setupRelationModal').on('shown.bs.modal', function () {
                        $('#rel_firstname').focus();
                    });
                },
                error: function(responseGetRelation) {
                    console.log('Error: ' + responseGetRelation.status);
                },
            });

        updateRelationId = $(this).data('id');
        $('#amla_id').val(updateItemId);
        });

        // Delete relation
        $(document).on('click', '.deleteRelation', function(){
            $('#btnConfirmDelete').addClass('deleteRelationConfirm');
            $('#btnConfirmDelete').removeClass('deleteItemConfirm');
            deleteRelationId = $(this).data('id');
            $('#deleteModal-header-title').text('Delete : Family Relation');

            $('#amla_id').val(updateItemId);

            $('#deleteItemModal').modal({
                show:  true,
                backdrop: 'static',
                keyboard: false,
            });
        });
        
    });
    // Update Item
    $(document).on('click', '.btnUpdate', function(){
        if($('#firstname').val() == '' || 
           $('#middlename').val() == '' ||
           $('#lastname').val() == '' ||
           $('#amla_type').val() == null ||
           $('#organization').val() == null ||
           $('#position').val() == '' ) {

            toastr.warning('Please supply empty fields.', 'Warning!');
        } else {
            $.ajax({
                type:   'PUT',
                url:    api + updateItemId,
                data:   {
                    '_token': $('input[name=_token]').val(),
                    'firstname': $('#firstname').val(),
                    'middlename': $('#middlename').val(),
                    'lastname': $('#lastname').val(),
                    'amla_type': $('#amla_type').val(),
                    'organization': $('#organization').val(),
                    'position': $('#position').val(),
                    'barangay': $('#barangay').val(),
                    'city': $('#city').val(),
                    'province': $('#province').val(),
                    'postal_code': $('#postal_code').val(),
                    'region': $('#region').val(),
                    'crimes': $('#crimes').val(),
                    'info_source': $('#info_source').val(),
                    'info_source_type': $('#info_source_type').val(),
                    'remarks': $('#remarks').val(),
                    'user': $(this).data('user'),
                },
                success: function(response){
                    // reload datatable
                    table.ajax.url( api ).load();
                    $('#setupAmlaModal').modal('hide');
                    toastr.success('Setup item successfully updated!', 'Success!');
                    console.log('Setup item successfully updated.');
                },
                error: function(response){
                    console.log('Error: ' + response.status);
                }
            });
        }
    });

    // Delete Item Modal
    $(document).on('click', '.deleteItem', function(){
        $('#btnConfirmDelete').addClass('deleteItemConfirm');
        $('#btnConfirmDelete').removeClass('deleteRelationConfirm');
        deleteItemId = $(this).data('id');
        $('#deleteModal-header-title').text('Delete : ' + title);

        $('#deleteItemModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
    });
    // Confirm Delete Item
    $(document).on('click', '.deleteItemConfirm', function(){
        $.ajax({
            type: 'DELETE',
            url:  api + deleteItemId,
            data: {
            '_token': $('input[name=_token]').val(),
            },
            success:  function(response){
                if(response.error==null) {
                    table.ajax.url( api ).load();
                    console.log('Setup item successfully deleted.');
                    toastr.success('Setup item has been deleted!', 'Success!');
                } else {
                    toastr.warning(response.error, "Warning!");
                    console.log(response.error);
                }
            },
            error:  function(response){
            console.log('Error: ' + response.status)
            }
        });

        $('#deleteItemModal').modal('hide');
    });


    // Relation Management
    // Relation Save
    $(document).on('click', '.btnSaveRelation', function(){
        if($('#rel_firstname').val() == '' || 
        $('#rel_middlename').val() == '' ||
        $('#rel_lastname').val() == '' ||
        $('#rel_type').val() == null ||
        $('#rel_consanguinity').val() == null ) {

            toastr.warning('Please supply empty fields.', 'Warning!');
        } else {
            $.ajax({
                type:   'POST',
                url:    api_relation,
                data:   {
                    '_token': $('input[name=_token]').val(),
                    'amla_id': $('#amla_id').val(),
                    'firstname': $('#rel_firstname').val(),
                    'middlename': $('#rel_middlename').val(),
                    'lastname': $('#rel_lastname').val(),
                    'type': $('#rel_type').val(),
                    'consanguinity': $('#rel_consanguinity').val(),
                },
                success: function(responsePost){
                    // reload datatable
                    tableRelation.ajax.url( api_relation + $('#amla_id').val() ).load();
                    $('#setupRelationModal').modal('hide');
                    toastr.success('Relation successfully created!', 'Success!');
                    console.log('Relation successfully created.');
                },
                error: function(responsePost){
                    console.log('Error: ' + responsePost.status);
                }
            });
        }
    });
    // Update relation info
    $(document).on('click', '.btnUpdateRelation', function(){
        if($('#rel_firstname').val() == '' || 
        $('#rel_middlename').val() == '' ||
        $('#rel_lastname').val() == '' ||
        $('#rel_type').val() == null ||
        $('#rel_consanguinity').val() == null ) {

            toastr.warning('Please supply empty fields.', 'Warning!');
        } else {
            $.ajax({
                type:   'PUT',
                url:    api_relation + updateRelationId,
                data:   {
                    '_token': $('input[name=_token]').val(),
                    'amla_id': $('#amla_id').val(),
                    'firstname': $('#rel_firstname').val(),
                    'middlename': $('#rel_middlename').val(),
                    'lastname': $('#rel_lastname').val(),
                    'type': $('#rel_type').val(),
                    'consanguinity': $('#rel_consanguinity').val(),
                },
                success: function(responseUpdateRelation){
                    // reload datatable
                    tableRelation.ajax.url( api_relation + $('#amla_id').val() ).load();
                    $('#setupRelationModal').modal('hide');
                    toastr.success('Relation successfully updated!', 'Success!');
                    console.log('Relation successfully updated.');
                },
                error: function(responseUpdateRelation){
                    console.log('Error: ' + responseUpdateRelation.status);
                }
            });
        }
    });
    // Confirm Delete Item
    $(document).on('click', '.deleteRelationConfirm', function(){
        $.ajax({
            type: 'DELETE',
            url:  api_relation + deleteRelationId,
            data: {
            '_token': $('input[name=_token]').val(),
            },
            success:  function(responseDeleteRelation){
                if(responseDeleteRelation.error==null) {
                    tableRelation.ajax.url( api_relation + $('#amla_id').val() ).load();
                    console.log('Setup item successfully deleted.');
                    toastr.success('Setup item has been deleted!', 'Success!')
                } else {
                    toastr.warning(responseDeleteRelation.error, "Warning!");
                    console.log(responseDeleteRelation.error);
                }
            },
            error:  function(responseDeleteRelation){
            console.log('Error: ' + responseDeleteRelation.status)
            }
        });

        $('#deleteItemModal').modal('hide');
    });
    // end of relation management


  });
</script>
@endsection