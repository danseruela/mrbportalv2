@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@php 
  $page = request()->route()->getName() == 'employee.create' ? 'Create Employee' : 'Employee Profile';
@endphp  

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row employee-profile"> 
<div class="col-md-3">
    <div class="box box-success">
      <div class="box-body box-profile">
          @php
            $photo = !empty($employee->emp_photo) ? $employee->emp_photo : 'user-mb-128x128.jpg';
          @endphp
          <img id="preview_image" class="profile-user-img img-responsive img-circle" src="{{ asset('storage/'.$photo) }}" alt="User profile picture">
          <i id="loading" class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#333; position:absolute; left:40%; top:10%; display:none;"></i>
          <h3 class="profile-username text-center">
            @if(!empty($employee)) 
                {!! Form::hidden('employee_id', $employee->id, ['id' => 'employee_id']) !!}
                {{ $employee->firstname }} {{ !empty($employee->middlename) ? substr($employee->middlename, 0, 1). '. ' : '' }} {{ $employee->lastname }}
            @endif
          </h3>

          <p class="text-muted text-center">
            @if(!empty($employee->job_title)) 
                <strong>{{ $employee->getOrg->desc }}</strong><br>
                {{ $employee->getLocation->desc }} - {{ $employee->getJob->desc }}
            @endif    
          </p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
               <i class="fa fa-id-card margin-r-5"></i> 
               <b>Employee No.</b> 
               <a class="pull-right text-success">
                  @if(!empty($employee)) {{ $employee->emp_no }} @endif
               </a>
            </li>
            <li class="list-group-item">
               <i class="fa fa-calendar margin-r-5"></i> 
               <b>Date Hired</b> 
               <a class="pull-right text-success">
                  @if(!empty($employee)) {{ $employee->emp_start_date }} @endif
               </a>
            </li>
            <li class="list-group-item">
               <i class="fa fa-certificate margin-r-5"></i> 
               <b>Status</b> 
               <a class="pull-right text-success">
                  @if(!empty($employee->emp_status)) {{ $employee->getStatus->desc }} @endif
               </a>
            </li>
            <li class="list-group-item">
               <i class="fa fa-birthday-cake margin-r-5"></i> 
               <b>Age</b> 
               <a class="pull-right text-success">
                  @if(!empty($employee->birthdate)) 
                    {{ \Carbon\Carbon::parse($employee->birthdate)->diff(\Carbon\Carbon::now())->format('%y years old') }} 
                  @endif
               </a>
            </li>
            </ul>
            {!! Form::open(['files' => true, 'id' => 'upload_form', 'name' => 'upload_form', 'role' => 'form']) !!}
            <!-- <form enctype="multipart/form-data" method="put" id="upload_form" role="form"> -->
              <label for="image" class="btn btn-success btn-block"><i class="fa fa-camera"></i> Upload Profile Photo</label>
              {!! Form::file('image', ['id' => 'image', 'style' => 'display:none;']) !!}
              {!! Form::hidden('file_name', null, ['id' => 'file_name']) !!}
              <!-- <input type="file" id="file" name="file" style="display:block;"> -->
              <!-- <input type="hidden" id="file_name" name="file_name" /> -->
            <!-- </form> -->
            {!! Form::close() !!}
      </div>
      <!-- /.box-body -->
    </div>
    <!-- Contact Information -->
    @if (!empty($contacts))
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Contact Information</h3>
        </div>
        <div class="box-body">
          <strong>
            <i class="fa fa-home margin-r-5"></i>
            Personal
          </strong>
            <ul class="list-unstyled">
            @foreach ($contacts as $contact)
              @if ($contact->category == 1)
                <li class="item{{ $contact->id }} text-muted"> {{ $contact->desc }} </li>  
              @endif 
            @endforeach
            </ul>
          <hr>
          <strong>
            <i class="fa fa-building margin-r-5"></i>
            Work
          </strong>
            <ul class="list-unstyled">
            @foreach ($contacts as $contact)
              @if ($contact->category == 2)
                <li class="item{{ $contact->id }} text-muted"> {{ $contact->desc }} </li>  
              @endif 
            @endforeach
            </ul>
          <hr>
          <strong>
            <i class="fa fa-address-card margin-r-5"></i>
            Others
          </strong>
            <ul class="list-unstyled" style="margin-bottom: 10px;">
            @foreach ($contacts as $contact)
              @if ($contact->category == 3)
                <li class="item{{ $contact->id }} text-muted"> {{ $contact->desc }} </li>  
              @endif 
            @endforeach
            </ul>  
        </div>
    </div>
    @endif
    <!-- Emergency Contact -->
    @if (!empty($relations))
    <div id="incaseemergency" class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">In Case of Emergency</h3>
        </div>
        <div class="box-body">
          @foreach ($relations as $info)
              <div class="info-{{ $info->id }}">
              @if ($info->is_emergency_contact == 1)
                <p id="e_name">
                  <i class="fa fa-address-card margin-r-5"></i> 
                  @if ($info->is_related == 1)
                    <strong>{{ $info->empRelatives['lastname'] }}, {{ $info->empRelatives['firstname'] }} {{ substr($info->empRelatives['middlename'], 0, 1) }}.</strong>
                  @else
                    <strong>{{ $info->lastname }}, {{ $info->firstname }} {{ substr($info->middlename, 0, 1) }}.</strong>
                  @endif  
                </p>  
                <p id="e_address" class="text-muted"> {{ $info->address }} </p>
                <p id="e_contact" class="text-muted"> {{ $info->contact_no }} </p>
                <hr>
              @endif
              </div> 
          @endforeach
          
        </div>
    </div>
    @endif
</div>
<div class="col-md-9">
@if ($action == 'create')
    {!! Form::open(['route' => 'employee.store', 'method' => 'POST']) !!}
@else 
    {!! Form::model($employee, ['route' => ['employee.update', $employee->id], 'method' => 'PUT', 'class' => !empty($isEnabled) ? 'disabled' : null]) !!}
@endif
    {{ csrf_field() }}  
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active">
      <a href="#personal" data-toggle="tab" aria-expanded="true">Personal Info</a>
    </li>
    <li class>
      <a href="#contact" data-toggle="tab" aria-expanded="false">Contacts</a>
    </li>
    @if ( !empty($relations) )
    <li class>
      <a href="#familyrelations" data-toggle="tab" aria-expanded="false">Relations</a>
    </li>
    @endif
    <li class>
      <a href="#job" data-toggle="tab" aria-expanded="false">Job</a>
    </li>
    <li class>
      <a href="#education" data-toggle="tab" aria-expanded="false">Education</a>
    </li>
    <li class>
      <a href="#employment_history" data-toggle="tab" aria-expanded="false">Employment History</a>
    </li>
    <li class>
      <a href="#ids" data-toggle="tab" aria-expanded="false">IDs</a>
    </li>
    @if ( !empty($salaries) )
    <!-- <li class>
      <a href="#salary" data-toggle="tab" aria-expanded="false">Salary</a>
    </li> -->
    @endif
    <!-- <li class>
      <a href="#qualifications" data-toggle="tab" aria-expanded="false">Qualifications</a>
    </li> -->
  </ul> <!-- / .nav-tabs -->
  <div class="tab-content">
    <!-- Personal Details -->
    <div class="tab-pane active" id="personal">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="emp_no" class="col-sm-2 control-label">Employee No.</label>

                  <div class="col-sm-10">
                    {!! Form::text('emp_no', null, ['id' => 'emp_no', 'class' => 'form-control', 'placeholder' => 'Employee No', 'required']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="nickname" class="col-sm-2 control-label">Nick Name</label>

                  <div class="col-sm-10">
                    {!! Form::text('nickname', null, ['id' => 'nickname', 'class' => 'form-control', 'placeholder' => 'Nick Name']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="firstname" class="col-sm-2 control-label">First Name</label>

                  <div class="col-sm-10">
                    {!! Form::text('firstname', null, ['id' => 'firstname', 'class' => 'form-control', 'placeholder' => 'First Name', 'required']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="middlename" class="col-sm-2 control-label">Middle Name</label>

                  <div class="col-sm-10">
                    {!! Form::text('middlename', null, ['id' => 'middlename', 'class' => 'form-control', 'placeholder' => 'Middle Name']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="lastname" class="col-sm-2 control-label">Last Name</label>

                  <div class="col-sm-10">
                    {!! Form::text('lastname', null, ['id' => 'lastname', 'class' => 'form-control', 'placeholder' => 'Last Name', 'required']) !!}
                  </div>
                </div>
                <div class="form-group">
                  <label for="birthdate" class="col-sm-2 control-label">Date of Birth</label>

                  <div class="col-sm-10">
                    <div class="input-group date">
                      {!! Form::text('birthdate', null, ['id' => 'birthdate', 'class' => 'form-control pull-right datepicker', 'required']) !!}
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="gender" class="col-sm-2 control-label">Gender</label>
                  <div class="col-sm-10">
                    @foreach ($gender as $sex)
                      <label class="radio-inline" style="padding: 7px 15px 0 0;">
                       {!! Form::radio('gender', $sex->id, false, ['id' => $sex->desc, 'class' => 'flat-red', 'required']) !!} {{ $sex->desc }}
                      </label>
                    @endforeach 
                  </div>
                </div>
                <div class="form-group">
                  <label for="civil_status" class="col-sm-2 control-label">Civil Status</label>
                  <div class="col-sm-10">
                    @php
                      $employee_civil_status = !empty($employee->civil_status) ? $employee->civil_status : null;
                    @endphp

                    {!! Form::select('civil_status', $civil_status, $employee_civil_status, ['id' => 'civil_status', 'style' => 'width: 100%;', 'class' => 'form-control select2 col-sm-10', 'required']) !!}
                  </div>  
                </div>
                <div class="form-group">
                  <label for="blood_type" class="col-sm-2 control-label">Blood Type</label>
                  <div class="col-sm-10">
                    @php
                      $employee_blood_type = !empty($employee->blood_type) ? $employee->blood_type : null;
                    @endphp

                    {!! Form::select('blood_type', $blood_type, $employee_blood_type, ['id' => 'blood_type', 'style' => 'width: 100%;', 'class' => 'form-control select2 col-sm-10', 'required']) !!}
                  </div>  
                </div>
              </div> <!-- /.box-body -->
            </div>
    </div>
    <!-- Contact Details -->
    <div class="tab-pane" id="contact">
      <div class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
            <label for="address1" class="col-sm-2 control-label">Address Street 1</label>

            <div class="col-sm-10">
              {!! Form::text('address1', null, ['id' => 'address1', 'class' => 'form-control', 'placeholder' => 'Address Street 1', 'required']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="address2" class="col-sm-2 control-label">Address Street 2</label>

            <div class="col-sm-10">
              {!! Form::text('address2', null, ['id' => 'address2', 'class' => 'form-control', 'placeholder' => 'Address Street 2']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="city" class="col-sm-2 control-label">City</label>

            <div class="col-sm-10">
              {!! Form::text('city', null, ['id' => 'city', 'class' => 'form-control', 'placeholder' => 'City', 'required']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="province" class="col-sm-2 control-label">State/Province</label>

            <div class="col-sm-10">
              {!! Form::text('province', null, ['id' => 'province', 'class' => 'form-control', 'placeholder' => 'State/Province', 'required']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="postal_code" class="col-sm-2 control-label">Zip/Postal Code</label>

            <div class="col-sm-10">
              {!! Form::text('postal_code', null, ['id' => 'postal_code', 'class' => 'form-control', 'placeholder' => 'Zip/Postal Code']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="country" class="col-sm-2 control-label">Country</label>

            <div class="col-sm-10">
              @php
                $employee_country = !empty($employee->country_id) ? $employee->country_id : null;
              @endphp

              {!! Form::select('country', $countries, $employee_country, ['id' => 'country', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            </div>
          </div>
        </div> <!-- / .box-body -->  
          @if(!empty($employee)) 
          <hr>
          <div class="box-header">
              <h3 class="box-title">Contact Numbers</h3> 
          </div>
          <div class="box-body">
          <div class="form-group col-sm-12">
            <a href="#" id="addContact" class="btn btn-success"><i class="fa fa-plus"></i> Add More Contacts</a>
          </div>
            @if (count($contacts) > 0)
            @foreach($contacts as $contact)
            <div class="item{{ $contact->id }} form-group">
                <div class="col-sm-4">
                  {!! Form::text('contact_info[]', $contact->desc, ['id' => 'contact_info', 'class' => 'form-control', 'placeholder' => 'Contact Info', 'required']) !!}
                </div>  
                <div class="col-sm-3">
                  {!! Form::select('contact_type[]', $contact_type, $contact->type, ['id' => 'contact_type', 'style' => 'width: 100%;', 'class' => 'form-control select2']) !!}
                </div>
                <div class="col-sm-3">
                  {!! Form::select('contact_cat[]', $contact_cat, $contact->category, ['id' => 'contact_cat', 'style' => 'width: 100%;', 'class' => 'form-control select2']) !!}
                </div>
                <a href="#" id="removeRow" class="col-sm-2 text-danger" data-id="{{ $contact->id }}" data-content="{{ $contact->desc }}" style="margin-top: 6px;"><i class="glyphicon glyphicon-trash"></i> Remove</a>
                {!! Form::hidden('contact_ids[]', $contact->id, ['id' => 'contact_ids']) !!}

            </div>
            @endforeach
            @endif
            <div id="contactRow"></div> <!-- / #contactRow -->
          </div> <!-- / .box-body -->
          @endif
       </div>
    </div> <!-- / #contact -->      
    <!-- Family Relations -->
    @if ( !empty($relations) )
    <div class="tab-pane" id="familyrelations">
        <div class="row">
          <div class="col-xs-12">
              <a href="#" id="addRelationBtn" class="add-modal btn btn-success"><i class="fa fa-plus"></i> Add Family Relation</a>  
              <table id="relationsTbl1" class="table table-bordered table-hover dt-responsive">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th></th>
                    <th>Relation</th>
                    <th>Degree of Consanguinity</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($relations as $relation)
                  <tr class="rel-item{{$relation->id}}">
                    <td>
                      @if($relation->is_related > 0)
                        {{ $relation->empRelatives['lastname'] }}, {{ $relation->empRelatives['firstname'] }} {{ substr($relation->empRelatives['middlename'], 0, 1) }}.
                      @else
                        {{ $relation->lastname }}, {{ $relation->firstname }} {{ !empty($relation->middlename) ? substr($relation->middlename, 0, 1) . '.' : '' }}
                      @endif
                    </td>
                    <td class="text-center">
                      @if($relation->is_related == 1)
                        <i class="fa fa-star text-success"></i> 
                      @endif
                      @if($relation->is_emergency_contact == 1)
                        <i class="fa fa-address-card text-success"></i> 
                      @endif
                    </td>
                    <td>{{ $relation->getType->desc }}</td>
                    <td>{{ $relation->getConsanguinity->desc1 }}</td>
                    <td>
                        <a href="#" class="edit-modal btn btn-primary" data-toggle="tooltip" title="Edit" data-id="{{ $relation->id }}" data-emp-related-id="{{ $relation->emp_related_id }}" data-is-related="{{ $relation->is_related }}" data-firstname="{{ $relation->firstname }}" data-middlename="{{ $relation->middlename }}" data-lastname="{{ $relation->lastname }}" data-reltype="{{ $relation->type }}" data-relcon="{{ $relation->consanguinity }}" data-is-emergency-contact="{{ $relation->is_emergency_contact }}" data-e-address="{{ $relation->address }}" data-e-contact="{{ $relation->contact_no }}"><i class="fa fa-edit"></i></a>
                      @if($relation->is_related > 0)
                        <a href="#" class="delete-modal btn btn-danger" data-toggle="tooltip" title="Delete" data-id="{{ $relation->id }}" data-fullname="{{ $relation->empRelatives['lastname'] }}, {{ $relation->empRelatives['firstname'] }} {{ substr($relation->empRelatives['middlename'], 0, 1) }}."><i class="fa fa-trash"></i></a>
                      @else
                        <a href="#" class="delete-modal btn btn-danger" data-toggle="tooltip" title="Delete" data-id="{{ $relation->id }}" data-fullname="{{ $relation->lastname }}, {{ $relation->firstname }} {{ substr($relation->middlename, 0, 1) }}."><i class="fa fa-trash"></i></a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th></th>
                    <th>Relation</th>
                    <th>Degree of Consanguinity</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
              </table>  <!-- / table -->        
          </div>  <!-- / .col -->
        </div>  <!-- / .row -->
    </div> <!-- / #familyrelations -->  
    @endif
    <!-- Job Details -->  
    <div class="tab-pane" id="job">
      <div class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
            <label for="job_cat" class="col-sm-2 control-label">Job Category</label>

            <div class="col-sm-10">
              @php
                $employee_job_cat = !empty($employee->job_cat) ? $employee->job_cat : null;
              @endphp

              {!! Form::select('job_cat', $job_cat, $employee_job_cat, ['id' => 'job_cat', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="job_title" class="col-sm-2 control-label">Job Title</label>

            <div class="col-sm-10">
              @php
                $employee_job = !empty($employee->job_title) ? $employee->job_title : null;
              @endphp

              {!! Form::select('job_title', $job_title, $employee_job, ['id' => 'job_title', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="job_level" class="col-sm-2 control-label">Job Level</label>

            <div class="col-sm-10">
              @php
                $employee_job_level = !empty($employee->job_level) ? $employee->job_level : null;
              @endphp

              {!! Form::select('job_level', $job_level, $employee_job_level, ['id' => 'job_level', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="job_specifics" class="col-sm-2 control-label">Specifications</label>

            <div class="col-sm-10">
              {!! Form::textarea('job_specifics', null, ['id' => 'job_specifics', 'class' => 'form-control', 'style' => 'width: 100%; resize: vertical; overflow: auto;', 'placeholder' => 'Job Specifications. . .']) !!}
            </div>
          </div>
          <div class="form-group">
            <label for="emp_status" class="col-sm-2 control-label">Status</label>

            <div class="col-sm-10">
              @php
                $employee_status = !empty($employee->emp_status) ? $employee->emp_status : null;
              @endphp

              {!! Form::select('emp_status', $emp_status, $employee_status, ['id' => 'emp_status', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            </div>
          </div>
        </div> 
          <hr>
          <div class="box-header">
              <h3 class="box-title">Company Details</h3>
          </div>
          <div class="box-body">
          <div class="form-group">
            <label for="org_code" class="col-sm-2 control-label">Organization</label>

            <div class="col-sm-10">
              @php
                $org_code = !empty($location->org_code) ? $location->org_code : null;
              @endphp

              {!! Form::select('org_code', $organizations, $org_code, ['id' => 'org_code', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            </div>  
          </div>
          <div class="form-group">
            <label for="location_code" class="col-sm-2 control-label">Location</label>

            <div class="col-sm-10">
              @php
                $location_code = !empty($location->location_code) ? $location->location_code : null;
              @endphp

              {!! Form::select('location_code', $locations, $location_code, ['id' => 'location_code', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
            </div>  
          </div>
          </div>
          <hr>
            <div class="box-header">
              <h3 class="box-title">Employment Contract</h3>
            </div>
            <div class="box-body">
            <div class="form-group">
            <label for="emp_start_date" class="col-sm-2 control-label">Start Date</label>

                <div class="col-sm-10">
                  <div class="input-group date">
                    {!! Form::text('emp_start_date', null, ['id' => 'emp_start_date', 'class' => 'form-control pull-right datepicker', 'required']) !!}
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
            <label for="emp_end_date" class="col-sm-2 control-label">End Date</label>

                <div class="col-sm-10">
                  <div class="input-group date">
                    {!! Form::text('emp_end_date', null, ['id' => 'emp_end_date', 'class' => 'form-control pull-right datepicker']) !!}
                    <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                </div>
              </div>
            </div>
            <!--
            <hr>
            <a href="#" id="terminateEmployee" class="btn btn-danger"><b>Terminate Employment</b></a> -->
          </div>
      </div>    
    </div> <!-- / #job -->
    <!-- Education Details -->
    <div class="tab-pane" id="education">
      <div class="row">
        <div class="col-xs-12">
          <a href="#" id="addEducation" class="btn btn-success"><i class="fa fa-plus"></i> Add Education</a>
          <table id="tblEducation" class="table table-bordered table-hover dt-responsive">
            <thead>
              <tr>
                <th>Level</th>
                <th>School Name</th>
                <th>Year Started</th>
                <th>Year Ended</th>
                <!-- <th>Degree</th> -->
                <th style="width: 13%;">Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    <!-- Employment Details -->
    <div class="tab-pane" id="employment_history">
      <div class="row">
        <div class="col-xs-12">
          <a href="#" id="addEmpHistory" class="btn btn-success"><i class="fa fa-plus"></i> Add Employment History</a>
          <table id="tblEmpHistory" class="table table-bordered table-hover dt-responsive">
            <thead>
              <tr>
                <th>Employer Name</th>
                <th>Designation</th>
                <th>Date Started</th>
                <th>Date Ended</th>
                <!-- <th>Degree</th> -->
                <th style="width: 13%;">Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    <!-- ID Details -->
    <div class="tab-pane" id="ids">
      <div class="row">
        <div class="col-xs-12">
          <a href="#" id="addID" class="btn btn-success"><i class="fa fa-plus"></i> Add Personal ID</a>
          <table id="tblID" class="table table-bordered table-hover dt-responsive">
            <thead>
              <tr>
                <th>Type</th>
                <th>ID Number</th>
                <th>Date Issued</th>
                <th>Date Expiry</th>
                <th style="width: 13%;">Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    @if (!empty($salaries))
    <!-- Salary Details -->
    <div class="tab-pane" id="salary">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
              <a href="#" id="addBasicSalaryBtn" class="add-salary-modal btn btn-success"><i class="fa fa-plus"></i> Add Salary Component</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">  
                <table id="salaryTbl2" class="table table-bordered table-hover dt-responsive">
                  <thead>
                    <tr>
                      <th>Salary Component</th>
                      <th>Type</th>
                      <th>Pay Frequency</th>
                      <th>Amount</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($salaries as $salary)
                    <tr class="salary-item{{$salary->id}}">
                      <td>{{ $salary->salary_component }}</td>
                      <td><span class="badge bg-green">{{ $salary->getSalComponentType->desc }}</span></td>
                      <td>{{ $salary->getPayFrequency->desc }}</td>
                      <td>{{ number_format($salary->basic_salary, 2) }}</td>
                      <td>
                        <a href="#" class="delete-sal-comp-modal btn btn-danger" data-toggle="tooltip" title="Delete" data-id="{{ $salary->id }}" data-salary-component="{{ $salary->salary_component }}"><i class="fa fa-trash"></i></a>
                        <a href="#" class="edit-sal-comp-modal btn btn-warning" data-toggle="tooltip" title="Edit" data-id="{{ $salary->id }}" data-salary-component="{{ $salary->salary_component }}" data-pay-grade="{{ $salary->pay_grd_code }}" data-sal-component-type="{{ $salary->sal_component_type }}" data-pay-freq="{{ $salary->pay_freq_code }}" data-basic-salary="{{ $salary->basic_salary }}" data-comments="{{ $salary->comments }}"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="3" class="text-right"><h3>Gross Salary</h3></td>
                      <td colspan="2"><h3>18,000.00</h3></td>
                    </tr>
                  </tfoot>
                </table>  <!-- / table -->        
            </div>  <!-- / .box-body -->  
          </div> <!-- / .box -->  
        </div>  <!-- / .col -->
        </div>  <!-- / .row -->
    </div> <!-- / #salary -->
    @endif
    <!-- Qualifications Details -->
    <div class="tab-pane" id="qualifications">
      <div class="form-horizontal">
        <div class="box-body">
          <h4>Qualifications</h4>
        </div>
      </div>
    </div>
    <hr>
    <div class="text-right">
          <a href="{{ url()->current() }}" id="cancelEmployeeProfile" class="btn btn-default" style="margin-right: 10px;">
            Cancel
          </a> 
      @if (!empty($isEnabled) ? 'disabled' : null == 'disabled')
          <button type="button" id="editEmployeeProfile" class="btn btn-success">
            <i class="glyphicon glyphicon-edit"></i> Edit
          </button>
      @else
          <button type="submit" id="updateEmployeeProfile" class="btn btn-success">
            <i class="glyphicon glyphicon-floppy-disk"></i> Save
          </button>
      @endif
    </div>  
  </div> <!-- / .tab-content -->
</div>  <!-- / .nav-tabs-custom -->
{!! Form::close() !!}

<!-- Form request error messages -->
@include('partials.error')

</div>  <!-- / .col-md-9 -->
</div> <!-- /.row .employee-profile -->

@include('partials._modal-employee')
@include('partials.modals.personalIDs')
@include('partials.modals.educationalAttainment')
@include('partials.modals.EmpHistory')
    
@endsection

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Select2 Cascade -->
<script src="{{ asset('custom/js/select2Cascade.js') }}"></script>
<!-- select2dropdown -->
<script src="{{ asset('custom/js/select2dropdown.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Jquery Number Format -->
<script src="{{ asset('custom/js/jquery.number.min.js') }}"></script>
<!-- page script -->
<script>
$(function () {
    $('#salaryTbl1').DataTable()
    $('#salaryTbl2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'order'       : [[ 1, 'asc' ], [ 0, 'asc' ]],
      'info'        : false,
      'autoWidth'   : false
    });

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
      increaseArea: '20%' // optional
    });
});

/*******************
 * Contact Numbers 
 *******************/

$(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#contactRow"); //Fields wrapper
    var add_button      = $("#addContact"); //Add button ID
    
    var x = 1; //initlal text box count

// Add Contact Row    
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append(
              '<div class="form-group">' +
              '<div class="col-sm-4">' +
                '<input type="text" class="form-control" id="contact_info" name="contact_info[]" placeholder="Contact Info" required>' +
              '</div>' + 
              '<div class="col-sm-3">' +
                  '{!! Form::select("contact_type[]", $contact_type, null, ["id" => "contact_type", "style" => "width: 100%;", "class" => "form-control select2"]) !!}' +
              '</div>' +
              '<div class="col-sm-3">' +
                  '{!! Form::select("contact_cat[]", $contact_cat, null, ["id" => "contact_cat", "style" => "width: 100%;", "class" => "form-control select2"]) !!}' +
              '</div>' +
              '{!! Form::hidden("contact_ids[]", null, ["id" => "contact_ids"]) !!}' +
              '<a href="#" id="deleteRow" class="col-sm-2 text-danger" style="margin-top: 6px;"><i class="glyphicon glyphicon-trash"></i> Remove</a></div>'); //add input box
        }
    });

// Remove Contact Row    
    $(wrapper).on("click","#deleteRow", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<script src="{{ asset('custom/js/employeeValidation.js') }}"></script> 
<script src="{{ asset('custom/js/employeePersonalIDMaintenance.js') }}"></script> 
<script src="{{ asset('custom/js/employeeEducationMaintenance.js') }}"></script>
<script src="{{ asset('custom/js/employeeEmpHistoryMaintenance.js') }}"></script>
@endsection
