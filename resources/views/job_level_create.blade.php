@extends('layouts.master')

@php 
  $page = request()->route()->getName() == 'level.create' ? 'Create Job Level' : 'Edit Job Level';
@endphp

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')
<div class="row">
<div class="col-xs-6">  
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
      <div class="box-body">
      @if ($action == 'create')
          {!! Form::open(['route' => 'level.store', 'role' => 'form', 'method' => 'POST']) !!}
      @else 
          {!! Form::model($job_level, ['route' => ['level.update', $job_level->id], 'role' => 'form', 'method' => 'PUT']) !!}
      @endif
          {{ csrf_field() }}   
        <div class="form-group">
          <label for="id">ID</label>
          {!! Form::text('job_level_id', !empty($available_id) ? $available_id : $job_level->id, ['id' => 'job_level_id', 'class' => 'form-control', 'placeholder' => 'ID', 'readonly', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Job Level Description</label>
          {!! Form::text('desc', null, ['desc' => 'desc', 'class' => 'form-control', 'placeholder' => 'Description', 'required']) !!}
        </div>
        <hr>
        <div class="text-right">
          <input type="submit" class="btn btn-success" style="font-weight: 700;" value="Save">
        </div> 
        {!! Form::close() !!}
      </div>  <!-- / .box-body -->   
    </div>  <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection