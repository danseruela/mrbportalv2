@extends('layouts.master')

@php 
  $page = request()->route()->getName() == 'paygrade.create' ? 'Create Pay Grade' : 'Edit Pay Grade';
@endphp

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')
<div class="row">
<div class="col-xs-6">  
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
      <div class="box-body">
      @if ($action == 'create')
          {!! Form::open(['route' => 'paygrade.store', 'role' => 'form', 'method' => 'POST']) !!}
      @else 
          {!! Form::model($pay_grade, ['route' => ['paygrade.update', $pay_grade->id], 'role' => 'form', 'method' => 'PUT']) !!}
      @endif
          {{ csrf_field() }}   
        <div class="form-group">
          <label for="id">ID</label>
          {!! Form::text('pay_grade_id', !empty($available_id) ? $available_id : $pay_grade->id, ['id' => 'pay_grade_id', 'class' => 'form-control', 'placeholder' => 'ID', 'readonly', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Pay Grade Description</label>
          {!! Form::text('desc', null, ['id' => 'desc', 'class' => 'form-control', 'placeholder' => 'Description', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="min_salary">Minimum Salary</label>
          {!! Form::text('min_salary', null, ['id' => 'min_salary', 'class' => 'form-control', 'placeholder' => 'Minimum Salary', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="max_salary">Maximum Salary</label>
          {!! Form::text('max_salary', null, ['id' => 'max_salary', 'class' => 'form-control', 'placeholder' => 'Maximum Salary', 'required']) !!}
        </div>
        <hr>
        <div class="text-right">
          <input type="submit" class="btn btn-success" style="font-weight: 700;" value="Save">
        </div> 
        {!! Form::close() !!}
      </div>  <!-- / .box-body -->   
    </div>  <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection