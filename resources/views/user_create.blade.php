@extends('layouts.master')

@php 
  $page = request()->route()->getName() == 'user.create' ? 'Create User' : 'Edit User';
@endphp

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')
<div class="row">
<div class="col-xs-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
    @if ($action == 'create')
          {!! Form::open(['route' => 'user.store', 'role' => 'form', 'method' => 'POST']) !!}
    @else 
          {!! Form::model($user, ['route' => ['user.update', $user->id], 'role' => 'form', 'method' => 'PUT']) !!}
    @endif
          {{ csrf_field() }}  
      <div class="box-body">
      <div class="row">  
      <div class="col-xs-6">   
        <div class="form-group">
          <label for="employee">Employee Name</label>
          {!! Form::hidden('user_id', !empty($available_id) ? $available_id : $user->id, ['id' => 'user_id']) !!}
          @php
              $emp_id = !empty($user->emp_id) ? $user->emp_id : null;
          @endphp
          {!! Form::select('emp_id', $employees, $emp_id, ['id' => 'emp_id', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="username">Username</label>
          {!! Form::text('username', null, ['id' => 'username', 'class' => 'form-control', 'placeholder' => 'Username', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <div class="input-group">
          {!! Form::text('password', null, [
                            'id' => 'password', 
                            'class' => 'form-control', 
                            'placeholder' => 'Generate Password', 
                            'rel' => 'gp',
                            'data-size' => '8',
                            'data-character-set' => 'a-z,A-Z,0-9',
                            'required', 
                            ]) !!}
          <span class="input-group-btn">
            <button type="button" class="getNewPass btn btn-success">
              <i class="fa fa-lock"></i> 
            </button>
          </span>
          </div>
          <div class="text-danger" id="errPassMatchMsg" style="margin-top: 8px !important;"></div>
        </div>
      </div>  <!-- / .col-xs-6 -->
      <div class="col-xs-6">
        <div class="form-group">
          <label for="user_role">Role</label>
          {!! Form::select('user_roles[]', $roles, null, ['id' => 'user_roles', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'multiple' => 'multiple', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="status">Status</label>
          {!! Form::hidden('deleted', !empty($user->deleted) ? $user->deleted : '', ['id' => 'deleted']) !!}
          <select name="status" id="status" class="select2" style="width: 100%;">
              <option value="0">Active</option>
              <option value="1">Inactive</option>
          </select>
        </div>
      </div> <!-- / .col-xs-6 -->
      </div> <!-- / .row -->
      <div class="row">  
      <hr> 
      <div class="col-sm-12">  
        @php $add_class = $action == 'create' ? 'add-user' : 'edit-user'; @endphp  
        <div class="text-right">
            <button type="submit" id="saveUser" class="btn btn-success {{ $add_class }}">
                <i class="glyphicon glyphicon-floppy-disk"></i> Save
            </button>  
        </div>   
      </div>  
      </div> <!-- / .row -->
      </div>  <!-- / .box-body --> 
    {!! Form::close() !!}   
    </div>  <!-- / .box -->  
</div> <!-- / .col-xs-12 -->
</div>  <!-- / .row -->
@endsection

<!-- Form request error messages -->
<!-- @include('partials.error') -->

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- passwordGenerator -->
<script src="{{ asset('custom/js/passwordGenerator.js') }}"></script>
<script>
  // Create a new password
  $(".getNewPass").click(function(){
    //$("#save-password").attr('data-dismiss', 'modal');
    var field = $(this).closest('div').find('input[rel="gp"]');
    field.val(randString(field));
  });

  // Auto Select Pass On Focus
  $('input[rel="gp"]').on("click", function () {
    $(this).select();
  });

  $(document).ready(function() {
      //Initialize Select2 Elements
      $('#emp_id').select2();

      $('#user_roles').select2({
        placeholder: "Choose User Roles",
      });

      //Initialize status value
      if ($('#deleted').val() != null) {
        $("#status").select2().select2('val',$('#deleted').val()); 
      }

      //Check password length.
      $("#password").blur(checkPasswordLength);

      //Check password matched.
      $("#password_confirmation").keyup(checkPasswordMatch);

  });    
</script>  
@endsection