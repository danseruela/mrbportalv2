@component('mail::message')
<p>
    <strong>From:</strong> {{ $data['sender'] }}
</p>
<p>
    <strong>Subject:</strong> {{ $data['subject'] }}
</p>
<p>
    <strong>Message:</strong>
    {{ $data['message'] }}
</p>
@endcomponent
