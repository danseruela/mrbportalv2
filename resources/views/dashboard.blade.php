@extends('layouts.master')

@section('title', 'Dashboard')

@section('page_title', 'Dashboard')

@section('breadcrumb_title', 'Dashboard')

@section('callout')
  @include('partials.callout')
@endsection

@section('content')
<!-- Small boxes (Stat box) -->
      <div class="row">

      <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>ICBS</h3>

              <p>MBPhil Express-O</p>
            </div>
            <div class="icon">
              <i class="fa fa-calculator"></i>
            </div>
            <!-- <a href="/reports/employee-related-accounts/dosri" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            <a href="http://172.168.10.12:8080/icbs/" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>Jasper</h3>

              <p>Reports</p>
            </div>
            <div class="icon">
              <i class="fa fa-area-chart"></i>
            </div>
            <a href="http://172.168.10.14:8080/jasperserver/login.html" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Helpdesk</h3>

              <p>Portal</p>
            </div>
            <div class="icon">
              <i class="fa fa-headphones"></i>
            </div>
            <!-- <a href="/reports/employee-related-accounts/dosri" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            <a href="http://172.168.10.251:9675/portal" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>User</h3>

              <p>Access Request</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <!-- <a href="/reports/employee-related-accounts/accounts" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
            <a href="http://172.168.10.251:9675/portal/page/13-access-request" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- <div class="col-lg-3 col-xs-6">
          //-- small box --//
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>{{ $total_users }}</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> -->
        
        <!-- <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-purple">
              <div class="inner">
                 <h3>{{ $total_employees }}</h3>
                <p>All Employees</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-people"></i>
              </div>
              <a href="/reports/employee-master/" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div> -->

        <!-- @foreach ($organizations as $org) -->
          <!-- <div class="col-lg-3 col-xs-6"> -->
            <!-- small box -->
            <!-- <div class="small-box bg-green">
              <div class="inner">
                @if ($org->id)
                  <h3>{{ App\Employee::where('org_code', $org->id)->where('emp_no', 'NOT LIKE', '%SysAd%')->count() }}</h3>
                @endif
                <p>{{ $org->desc }}</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-people"></i>
              </div>
              <a href="/reports/employee-master/{{$org->id}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        @endforeach -->
        
      </div>

      <div class="row">
        <!-- <section class="col-sm-4 connectedSortable">
          <div class="box box-success">
              <div class="box-header with-border">
                <i class="fa fa-bar-chart-o"></i>

                <h3 class="box-title">Consolidated Employee Gender</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div id="gender-chart" style="height: 300px;"></div>
              </div>
              //-- /.box-body--//
            </div>
        </section>
        <section class="col-sm-4 connectedSortable">
          <div class="box box-success">
              <div class="box-header with-border">
                <i class="fa fa-bar-chart-o"></i>

                <h3 class="box-title">Consolidated Employee Status</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div id="empstat-chart" style="height: 300px;"></div>
              </div>
              //-- /.box-body--//
            </div>
        </section> -->
        <section class="col-sm-8">
            <!-- quick email widget -->
            @include('partials.quick-email')
        </section>
        <section class="col-sm-4">
          <div class="box box-success">
              <div class="box-header with-border">
                <i class="fa fa-download"></i>
                <h3 class="box-title">Downloadable Forms (ERA)</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div> <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <li class="item">
                    <div class="product-img">
                      <i class="fa fa-file text-success" style="font-size: 36px; margin-right: 10px;"></i>
                    </div>
                    <div>
                      <a href="/era-form-download" class="product-title">ERA Fill-Up Form</a>
                      <span class="product-description">Employee Related Accounts Fill-Up Form</span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fa fa-file text-success" style="font-size: 36px; margin-right: 10px;"></i>
                    </div>
                    <div>
                      <a href="/rpt-form-download" class="product-title">RPT Fill-Up Form</a>
                      <span class="product-description">Related Party Fill-Up Form</span>
                    </div>
                  </li>
                </ul>
              </div> <!-- /.box-body -->
            </div> <!-- box -->
            <div class="box box-success">
              <div class="box-header with-border">
                <i class="fa fa-download"></i>
                <h3 class="box-title">Downloadable Forms (IT)</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div> <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <li class="item">
                    <div class="product-img">
                      <i class="fa fa-file text-success" style="font-size: 36px; margin-right: 10px;"></i>
                    </div>
                    <div>
                      <a href="/it-request-form-download" class="product-title">IT Request Form</a>
                      <span class="product-description">An IT request form for branches.</span>
                    </div>
                  </li>
                  <li class="item">
                    <div class="product-img">
                      <i class="fa fa-file text-success" style="font-size: 36px; margin-right: 10px;"></i>
                    </div>
                    <div>
                      <a href="/user-request-form-download" class="product-title">User Request Form</a>
                      <span class="product-description">An offline user access request form for ICBS.</span>
                    </div>
                  </li>
                </ul>
              </div> <!-- /.box-body -->
            </div> <!-- box -->
        </section>
      </div> <!-- /.row -->
<!-- / small boxes -->

@include('partials._modal-user-changepass')
@endsection
@section('scripts')
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="{{ asset('adminlte/dist/js/pages/dashboard.js') }}"></script> -->
<!-- FLOT Charts -->
<script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.categories.js') }}"></script>
<!-- page script -->
<script src="{{ asset('custom/js/userChangePassword.js') }}"></script>
<script type="text/javascript">
/*
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }

  function showChart(element, data) {
    $.plot(element, data, {
        series: {
          pie: {
            show       : true,
            radius     : 1,
            innerRadius: 0.5,
            label      : {
              show     : true,
              radius   : 2 / 3,
              formatter: labelFormatter,
              threshold: 0.1
            }

          }
        },
        legend: {
          show: true
        }
      });
  }

$(document).ready(function(){
  // Get gender data
  // $.ajax({
  //   url:      'genderdata',
  //   type:     'GET',
  //   data: {
  //       '_token': $('input[name=_token]').val(),
  //   },
  //   success:  function(result){
  //     showChart('#gender-chart', result);
  //     console.log(result);
  //   },
  //   error:  function(err) {
  //     console.log(err.status);
  //   }
  // });

  // Get employee status data
  // $.ajax({
  //   url:      'empstatusdata',
  //   type:     'GET',
  //   data: {
  //       '_token': $('input[name=_token]').val(),
  //   },
  //   success:  function(result){
  //     showChart('#empstat-chart', result);
  //     console.log(result);
  //   },
  //   error:  function(err) {
  //     console.log(err.status);
  //   }
  // });

});
</script>
@endsection