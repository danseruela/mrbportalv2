@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net/css/buttons.dataTables.min.css') }}">
  <!-- Media Print -->
  <link rel="stylesheet" media="print" href="{{ asset('custom/css/print.css') }}">
  <!-- custom -->
  <style>
    .report-filter { margin-bottom: 20px; }
    .report-filter .iradio_square-green {
      margin-left: 20px !important;
    }
  </style>
  
@endsection

@if ($request->path() === 'reports/attachments/customer')
    @php
        $report_title = 'Attachments by Customer Report';
        $api = '/api/rptCustomerUploads/';
    @endphp
@elseif ($request->path() === 'reports/customer-uploads')
    @php
        $report_title = 'Attachments by Customer Report';
        $api = '/api/rptCustomerUploads/';
    @endphp
@elseif ($request->path() === 'reports/attachments/branch')
    @php
        $report_title = 'Attachments by Branch Report';
        $api = '/api/rptBranchUploads/';
    @endphp    
@else
    @php
        $report_title = $request->path();
        $api = '';
    @endphp
@endif    

@section('title', $report_title)

@section('page_title', 'Reports')

@section('breadcrumb_title', 'Reports')

@section('content')

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <h2 class="page-header text-center">
        <!-- <i class="fa fa-file"></i> -->
        <span id="report_title">{{ $report_title }}</span>
        <small>as of </small>
        <small id="cutoff_title">{{ Carbon\Carbon::now()->format('Y-m-d') }}</small>
      </h2>
      <!-- <a href="#" class="btn btn-success print pull-right" onclick="window.print()">
        <i class="fa fa-print"></i> 
        Print
      </a> -->
      <!-- <a href="#" class="btn btn-success">Create Organization</a> -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">  
      <div class="report-filter">
          <a href="#" class="btn btn-success setCutoff">
              <span id="" class='fa fa-calendar'></span> Cutoff Dates
          </a>
      </div>
        <table id="tblCustomer" class="table table-striped dt-responsive" data-api="{{ $api }}">
          <thead>
            <tr>
              <th>Branch</th>
              @if($request->path() === 'reports/attachments/customer' || $request->path() === 'reports/customer-uploads')
              <th>Customer ID</th>
              <th>Customer Name</th>
              <th>Actual CIF</th>
              <th>CIF Files</th>
              <th>Loan Files</th>
              <th>Deposit Files</th>
              @endif
              <th>Total</th>
            </tr>
          </thead>
        </table>  <!-- / table -->    
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->

<!-- MODALS -->
<!-- Modal form to set cutoff dates -->
<div id="generateReport" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Set-up: Cut-off dates</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="date_from" class="col-sm-2 control-label">Date from</label>
                        <div class="col-xs-10">
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right datepicker" id="date_from" name="date_from" placeholder="Date From" required>
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date_to" class="col-sm-2 control-label">Date To</label>
                        <div class="col-xs-10">
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right datepicker" id="date_to" name="date_to" placeholder="Date To" required>
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>    
                        </div>
                    </div>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success generate">
                    <span id="" class='fa fa-refresh'></span> Generate Report
                </button>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <span class='glyphicon glyphicon-remove'></span> Close
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- iCheck -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- DataTables Buttons -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake_vfs_fonts.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.print.min.js') }}"></script>
<!-- page script -->
<script>
$(document).ready(function() {
    // Initialize datatable.
    var api = $('#tblCustomer').data('api');
    var columns = '';

    if(api == '/api/rptCustomerUploads/') {
      columns =   [
        { 'data':  'branch' },
        { 'data':  'customer_id' },
        { 'data':  'display_name' },
        { 'data':  'actual_cif_files' },
        { 'data':  'cif_files' },
        { 'data':  'loan_files' },
        { 'data':  'deposit_files' },
        { 'data':  'uploaded_files' },
      ];
    } 
    if(api == '/api/rptBranchUploads/') {
      columns =   [
        { 'data':  'branch' },
        { 'data':  'uploaded_files' },
      ];
    }

    var table = $('#tblCustomer').DataTable({
      'processing'  : true,
      'deferLoading': 10,
      'serverSide'  : false,
      'ajaxSource'  : api + $('#cutoff_title').text() + '/' + $('#cutoff_title').text() + ' 23:59:59',
      'columns'     : columns,
      'dom'         : 'Bfrtip',
      'buttons'     : [
                        {
                          extend: 'pdf',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-file"></i> Export PDF',
                        }, 
                        {
                          extend: 'csv',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-table"></i> Export CSV',
                        }, 
                        {
                          extend: 'print', 
                          text: '<i class="fa fa-print"></i> <u>P</u>rint', 
                          autoPrint: true,
                          customize: function(win) {
                            $(win.document.body).css({margin: '0 20px'});
                          },
                        }
                      ],
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });

    // Initialize modal upon page load.
    $('#generateReport').modal('show');

    // show cut-off dates modal.
    $(document).on('click', '.setCutoff', function(){
        $('#generateReport').modal('show');
    });

    $(document).on('click', '.generate', function(){
        if( $('#date_from').val() == '' || $('#date_to').val() == '' ) {
            toastr.warning('Please supply empty fields.', 'Warning!');
        } else {
          // reload data table
          table.ajax.url( api + $('#date_from').val() + '/' + $('#date_to').val() + ' 23:59:59' ).load();
          $('#cutoff_title').text($('#date_from').val() + ' to ' + $('#date_to').val())
          $('#generateReport').modal('hide');
        }
        
    });
    
});

</script>
@endsection