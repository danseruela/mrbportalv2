@extends('layouts.master')

@php 
  $page = request()->route()->getName() == 'title.create' ? 'Create Job Title' : 'Edit Job Title';
@endphp

@section('title', $page)

@section('page_title', $page)

@section('breadcrumb_title', $page)

@section('content')
<div class="row">
<div class="col-xs-6">  
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Setup</h3>
      </div>
      <div class="box-body">
      @if ($action == 'create')
          {!! Form::open(['route' => 'title.store', 'role' => 'form', 'method' => 'POST']) !!}
      @else 
          {!! Form::model($job_title, ['route' => ['title.update', $job_title->id], 'role' => 'form', 'method' => 'PUT']) !!}
      @endif
          {{ csrf_field() }}   
        <div class="form-group">
          <label for="id">ID</label>
          {!! Form::text('job_id', !empty($available_id) ? $available_id : $job_title->id, ['id' => 'job_id', 'class' => 'form-control', 'placeholder' => 'ID', 'readonly', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="id">Job Category</label>
          @php
            $emp_job_cat = !empty($job_title->cat_id) ? $job_title->cat_id : null;
          @endphp

          {!! Form::select('cat_id', $job_cat, $emp_job_cat, ['id' => 'cat_id', 'style' => 'width: 100%;', 'class' => 'form-control select2', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="desc">Job Title Description</label>
          {!! Form::text('desc', null, ['desc' => 'desc', 'class' => 'form-control', 'placeholder' => 'Description', 'required']) !!}
        </div>
        <hr>
        <div class="text-right">
          <input type="submit" class="btn btn-success" style="font-weight: 700;" value="Save">
        </div> 
        {!! Form::close() !!}
      </div>  <!-- / .box-body -->   
    </div>  <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Page Script -->
<script>
$(document).ready(function() {
    //Initialize Select2 Elements
    $('.select2').select2();
    $(".select2").on("select2:open", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", "Select an option...");
    });

    $(".select2").on("select2:close", function() {
        $(".select2-search--dropdown .select2-search__field").attr("placeholder", null);
    });
});    
</script>
@endsection