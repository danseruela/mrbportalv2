@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('title', $page_title)

@section('page_title', $page_title)

@section('breadcrumb_title', $page_title)

@section('content')

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <!-- /.box-header -->
    <div class="box-body">
        @php
          $group = !empty($request->id) ? $request->id : 0;
        @endphp 
        <table id="customerTable" class="table table-bordered table-hover dt-responsive" data-api="{{ $api }}" data-item="{{ $page_title }}">
          <thead>
            <tr>
              <th>Customer ID</th>
              <th>Branch</th>
              <th>Customer Name</th>
              <th>Customer Type</th>
              <th>Status</th>
              <th>Created By</th>
              <th>Updated By</th>
              <th width="10%">Actions</th>
            </tr>
          </thead>
        </table>  <!-- / table -->  
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->

<!-- Modals -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('custom/js/select2dropdown.js') }}"></script>
<!-- page script -->
<script>
$(document).ready(function(){
    // Initialize add button text
    var api = $('#customerTable').data('api');
    var title = $('#customerTable').data('item');

    var api_relation = $('#relationTable').data('api');

    // Initialize table
    var table = $('#customerTable').DataTable({
        'ajaxSource'  : api,
        'columns'     : [
            { 'data':  'customer_id' },
            { 'data':  'branch' },
            { 'data':  'customer_name' },
            { 'data':  'customer_type' },
            { 'data':  'status' },
            //   'render': function(data, type, row){
            //       var str = row.birth_date;
            //       var res = str.split(" ", 1);

            //       return res;
            //   } },
            { 'data':  'created_by' },
            { 'data':  'updated_by' },
            { 'data':   'id',
              'render': function(data, type, row){
                  var actions; 
                  var a_view = ''

                //   if($('#customerTable').data('can_update') == 1) {
                //     a_update = '<a class="tooltipped btn btn-primary editDetails" href="#" title="Edit Details" data-id="' + row.id + '"><i class="fa fa-edit"></i></a> '; 
                //   } 
                //   if ($('#customerTable').data('can_update') == 0 && $('#customerTable').data('can_delete') == 0) {
                //     a_view = '<a class="tooltipped btn btn-success viewDetails" href="#" title="View Details" data-id="' + row.id + '"><i class="fa fa-eye"></i></a> ';
                //   }

                  a_view = '<a class="tooltipped btn btn-success viewDetails" href="/customers/' + row.id + '" title="View Details" data-id="' + row.id + '"><i class="fa fa-eye"></i></a> ';
                // a_update = '<a class="tooltipped btn btn-primary editDetails" href="#" title="Edit Details" data-id="' + row.id + '"><i class="fa fa-edit"></i></a> '; 

                  actions = a_view;
                  
                  return actions;
            } },
        ],
        'lengthChange': false,        
        //'lengthMenu'  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'paging'      : true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    // Initialize tooltip on datatable redraw
    table.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    // Initialize Toastr
    toastr.options.positionClass = 'toast-bottom-right';


  });
</script>
@endsection