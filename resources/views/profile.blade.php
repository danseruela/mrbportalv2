@extends('layouts.master')

@section('title', 'Profile')

@section('page_title', 'Profile')

@section('breadcrumb_title', 'Profile')

@section('callout')
  @include('partials.callout')
@endsection

@section('content')
@foreach($emp_details as $employee)

<div class="row">
  <div class="col-sm-3">
    <div class="box box-success">
      <div class="box-body box-profile">
        @php
          $photo = !empty($employee->emp_photo) ? $employee->emp_photo : 'user-mb-128x128.jpg';
        @endphp
        <img id="preview_image" class="profile-user-img img-responsive img-circle" src="{{ Storage::url($photo) }}" alt="User profile picture">
        <h3 class="profile-username text-center">
            {{ $employee->firstname . ' ' .substr($employee->middlename, 0, 1). '. '. $employee->lastname }}
        </h3>
        <p class="text-muted text-center">
          <strong>{{ $employee->getOrg->desc }}</strong><br>
            {{ $employee->getLocation->desc }} - {{ $employee->getJob->desc }}
        </p>
        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <i class="fa fa-id-card margin-r-5"></i> 
                <b>Employee No.</b> 
                <a class="pull-right text-success">
                  {{ $employee->emp_no }}
                </a>
            </li>
            <li class="list-group-item">
                <i class="fa fa-calendar margin-r-5"></i> 
                <b>Date Hired</b> 
                <a class="pull-right text-success">
                  {{ $employee->emp_start_date }}
                </a>
            </li>
            <li class="list-group-item">
                <i class="fa fa-certificate margin-r-5"></i> 
                <b>Status</b> 
                <a class="pull-right text-success">
                  {{ $employee->getStatus->desc }}
                </a>
            </li>
          </ul>
          <!-- open modal for change password -->
          <a href="#" class="btn btn-success btn-block change-password" data-toggle="tooltip" title="Change Password" data-user-id="{{auth()->user()->id}}" data-username="{{auth()->user()->username}}"><i class="fa fa-key margin-r-5"></i> Change Password</a>          
      </div> <!-- .box-body -->
    </div> <!-- .box -->
  
  <!-- ID Information -->
    @if (!empty($personal_ids))
      <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Personal IDs</h3>
          </div>
          <div class="box-body">
          <ul class="list-group list-group-unbordered" style="margin-bottom: 0;">
          @foreach ($personal_ids as $id)
            <li class="list-group-item" style="border: none;">
                <i class="fa fa-id-card margin-r-5"></i> 
                <b>{{ $id->id_type }}</b> 
                <a class="pull-right text-success">{{ $id->id_no }}</a>
            </li>
          @endforeach
          </ul>
          </div>
      </div>
      @endif
  
  <!-- Contact Details -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Contacts</h3>
          <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <p>
              <i class="fa fa-map-marker margin-r-5"></i> 
              <b>Address: </b> <br><span class="text-muted">{{ $employee->address1 }}, {{ $employee->address2 }}, {{ $employee->city }}, {{ $employee->province }}, {{ $employee->postal_code }} {{ $employee->getCountry->desc }}</span>
            </p>
          <ul class="list-unstyled">
            @foreach ($contacts as $contact)
              @php
                switch($contact->type) {
                  case 1: $contact_icon = 'phone-square'; break;
                  case 2: $contact_icon = 'mobile'; break;
                  case 4: $contact_icon = 'envelope'; break;
                  default: $contac_icon = 'map-marker'; break;
                }
              @endphp
              <li class="item{{ $contact->id }}" style="margin-top:8px;"> 
                  <b><i class="fa fa-{{$contact_icon}} margin-r-5"></i> [{{ $contact->getCategory->desc }}] {{ $contact->getType->desc }}:</b> <br>
                  <span class="text-muted">{{ $contact->desc }}</span> 
              </li>  
            @endforeach
          </ul>
        </div><!-- /.box-body -->
      </div> <!--/.box -->
  </div> <!-- col-sm-3 -->
  <div class="col-sm-9">
    <!-- Personal Details -->
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Personal Info</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div> <!-- /.box-header -->
        <div class="box-body">
            <p><i class="fa fa-user margin-r-5"></i> <b>Nick Name: </b> <span class="text-muted">{{ $employee->nickname }}</span></p>
            <p><i class="fa fa-calendar margin-r-5"></i> <b>Date of Birth: </b> <span class="text-muted">{{ $employee->birthdate }}</span></p>
            @php $gender = $employee->getGender->desc == 'Male' ? 'male' : 'female'; @endphp	
            <p><i class="fa fa-{{$gender}} margin-r-5"></i> <b>Gender: </b> <span class="text-muted">{{ $employee->getGender->desc }}</span></p>
            <p><i class="fa fa-certificate margin-r-5"></i> <b>Civil Status: </b> <span class="text-muted">{{ $employee->getCivilStatus->desc }}</span></p>
            <p><i class="fa fa-certificate margin-r-5"></i> <b>Blood Type: </b> <span class="text-muted">{{ $employee->getBloodType->desc }}</span></p>
        </div>
        <!-- /.box-body -->
    </div> <!--/.box -->
  
    <!-- Education Details -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Educational Attainment</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div> <!-- /.box-header -->
      <div class="box-body no-padding">
        <table class="table" id="educationalAttainmentTable">
          <thead>
            <tr>
              <th>Level</th>
              <th>Name of School</th>
              <th>Year Start</th>
              <th>Year End</th>
              <th>Degree</th>
            </tr>
          </thead>
          <tbody>
          @foreach($educ_attainment as $educ)
            <tr>
              <td>{{ $educ->educ_level }}</td>
              <td>{{ $educ->school_name }}</td>
              <td>{{ $educ->year_start }}</td>
              <td>{{ $educ->year_end }}</td>
              <td>{{ $educ->degree }}</td>
            </tr>  
          @endforeach
          </tbody>
        </table>
      </div> <!-- /.box-body -->
    </div> <!--/.box -->

    <!-- Employment History Details -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Employment History</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div> <!-- /.box-header -->
      <div class="box-body no-padding">
        <table class="table" id="employmentHistoryTable">
          <thead>
            <tr>
              <th>Employer</th>
              <th>Designation</th>
              <th>ID No.</th>
              <th>Date Started</th>
              <th>Date Ended</th>
            </tr>
          </thead>
          <tbody>
          @foreach($emp_history as $eh)
            <tr>
              <td>{{ $eh->employer_name }}</td>
              <td>{{ $eh->designation }}</td>
              <td>{{ $eh->employment_id_no }}</td>
              <td>{{ $eh->date_start }}</td>
              <td>{{ $eh->date_end }}</td>
            </tr>  
          @endforeach
          </tbody>
        </table>
      </div> <!-- /.box-body -->
    </div> <!--/.box -->

    <!-- FAMILY RELATIONS LIST -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Relations</h3>
        <div class="box-tools pull-right">
          <span class="label label-success">{{ count($relations) }} Members</span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
        </div> <!-- /.box-header -->
        <div class="box-body no-padding">
            <ul class="users-list clearfix">
              @foreach ($relations as $relation)
                <li>
                  <img class="relations" src="{{Storage::url('user-mb-128x128.jpg')}}" alt="User Image"> 
                  <a class="users-list-name" href="#">
                    @if ($relation->is_related == 1)
                      {{ $relation->empRelatives['lastname'] }}, {{ $relation->empRelatives['firstname'] }} {{ substr($relation->empRelatives['middlename'], 0, 1) }}.
                    @else
                      {{ $relation->lastname }}, {{ $relation->firstname }} {{ substr($relation->middlename, 0, 1) }}.
                    @endif
                  </a>
                  <span class="users-list-date">{{ $relation->getType->desc }}</span>
                </li>
              @endforeach  
            </ul> <!-- /.users-list -->
        </div> <!-- /.box-body -->
        <!-- <div class="box-footer text-center">
            <a href="javascript:void(0)" class="uppercase">View All Users</a>
        </div> -->
        <!-- /.box-footer -->
    </div> <!--/.box -->
  </div> <!-- .col-sm-9 -->
</div> <!-- .row -->


@include('partials._modal-user-changepass')
@endforeach
@endsection

@section('scripts')
<!-- page script -->
<script src="{{ asset('custom/js/userChangePassword.js') }}"></script>
@endsection