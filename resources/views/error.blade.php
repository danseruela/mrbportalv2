@extends('layouts.master')

@section('title', 'Error Page')

@section('breadcrumb_title', 'Error Page')

@section('content')
<div class="row">
	<div class="col-xs-12">
		@if($error)
		<div class="text-center">
			<h1 class="errormsg">{{ $error }}</h1>
			<p class="errormsg">{{ $message }}</p>
		</div>
		@endif	
	</div>
</div>
@endsection
