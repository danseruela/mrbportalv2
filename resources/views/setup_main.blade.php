@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@if ($request->path() === 'setups/attachmentcategory')
    @php
        $page_title = 'Setup : Attachment Category';
        $api = '/api/attachmentcategory/';
    @endphp
@elseif ($request->path() === 'setups/attachmenttype')
    @php
        $page_title = 'Setup : Attachment Type';
        $api_2 = '/api/attachmenttype/';
    @endphp
@elseif ($request->path() === 'setups/amlatype')
    @php
        $page_title = 'Setup : AMLA Type';
        $api = '/api/amlatype/';
    @endphp
@elseif ($request->path() === 'setups/region')
    @php
        $page_title = 'Setup : Region';
        $api = '/api/region/';
    @endphp 
@elseif ($request->path() === 'setups/postalcode')
    @php
        $page_title = 'Setup : Postal Code';
        $api = '/api/postalcode/';
    @endphp     
@elseif ($request->path() === 'setups/crimetype')
    @php
        $page_title = 'Setup : Crime Type';
        $api = '/api/crimetype/';
    @endphp      
@elseif ($request->path() === 'setups/infosource')
    @php
        $page_title = 'Setup : Info Source';
        $api = '/api/infosource/';
    @endphp     
@elseif ($request->path() === 'setups/infosourcetype')
    @php
        $page_title = 'Setup : Info Source Type';
        $api = '/api/infosourcetype/';
    @endphp   
@elseif ($request->path() === 'setups/idtype')
    @php
        $page_title = 'Setup : ID Type';
        $api = '/api/idtype/';
    @endphp 
@elseif ($request->path() === 'setups/educlevel')
    @php
        $page_title = 'Setup : Education Level';
        $api = '/api/educlevel/';
    @endphp                         
@else
    @php
        $page_title = 'Setup : ' . $request->path();
    @endphp
@endif

@section('title', $page_title)

@section('page_title', $page_title)

@section('breadcrumb_title', $page_title)

@section('content')

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="#" id="addItem"  class="btn btn-success">Create {{ $page_title }}</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if (!empty($api))  
        <table id="setupTable" data-api="{{ $api }}" data-item="{{ $page_title }}" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>ID</th>
              <th>Description</th>
              <th>Actions</th>
            </tr>
          </thead>
        </table>  <!-- / table -->  
        @else
        <table id="setupTable_2" data-api="{{ $api_2 }}" data-item="{{ $page_title }}" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>ID</th>
              <th>Description</th>
              <th>Category</th>
              <th>Actions</th>
            </tr>
          </thead>
        </table>  <!-- / table -->  
        @endif
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->

@include('partials.modals.setupModal')

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('custom/js/select2dropdown.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    // Initialize add button text
    var api = $('#setupTable').data('api') != null ? $('#setupTable').data('api') : $('#setupTable_2').data('api');
    var title = $('#setupTable').data('item') != null ? $('#setupTable').data('item') : $('#setupTable_2').data('item');

    // Initialize table
    var table = $('#setupTable').DataTable({
        'ajaxSource'  : api,
        'columns'     : [
            { 'data':  'id' },
            { 'data':  'desc' },
            { 'data':   'id',
              'render': function(data, type, row){
                  return '<a class="tooltipped btn btn-primary editItem" href="#" title="Edit Item" data-id="' + row.id + '" data-desc="' + row.desc + '"><i class="fa fa-edit"></i></a> ' +
                         '<a class="tooltipped btn btn-danger deleteItem" href="#" title="Delete Item" data-id="' + row.id + '" data-desc="' + row.desc + '"><i class="fa fa-trash"></i></a> '
            } },
        ],
        'lengthChange': false,        
        //'lengthMenu'  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'paging'      : true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    var table_2 = $('#setupTable_2').DataTable({
        'ajaxSource'  : api,
        'columns'     : [
            { 'data':  'id' },
            { 'data':  'desc' },
            { 'data':  'category' }, 
            { 'data':   'id',
              'render': function(data, type, row){
                  return '<a class="tooltipped btn btn-primary editItem" href="#" title="Edit Item" data-id="' + row.id + '" data-desc="' + row.desc + '" data-cat_id="' + row.cat_id + '"><i class="fa fa-edit"></i></a> ' +
                         '<a class="tooltipped btn btn-danger deleteItem" href="#" title="Delete Item" data-id="' + row.id + '" data-desc="' + row.desc + '"><i class="fa fa-trash"></i></a> '
            } },
        ],
        'lengthChange': false,        
        //'lengthMenu'  : [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'paging'      : true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
        'order'       : [2, 'asc'],
    });

    // Initialize tooltip on datatable redraw
    table.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    table_2.on( 'draw', function () {
        $('.tooltipped').attr('data-placement', 'top');
        $('.tooltipped').tooltip();
    } );

    // Initialize Toastr
    toastr.options.positionClass = 'toast-bottom-right';
    
    // Initialize Dropdown menus
    dropdownSelect($('#category'), '/api/attachmentcategory');

    // Display/Hide Category element
    if($('#setupTable').data('api') != null) {
        $('#category-wrapper').addClass('hidden');
    } else {
        $('#category-wrapper').removeClass('hidden');
    }

    // Add button is clicked
    $(document).on('click', '#addItem', function(){
        $('#modal-header-title').text($(this).text());
        $('#btnPost span').text(' Save');
        $('#btnPost').addClass('btnSave');
        $('#btnPost').removeClass('btnUpdate');
        
        $('.id_no').addClass('hidden');
        $('#desc').val('');

        if($('#setupTable_2').data('api') != null) {
            $('#category').val(0).trigger('change');
        } 

        // Initialize Modal
        $('#setupModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
        $('#setupModal').on('shown.bs.modal', function () {
            $('#desc').focus();
        });
    });
    // Item Saved
    $(document).on('click', '.btnSave', function(){
        $.ajax({
            type:   'POST',
            url:    api,
            data:   {
                '_token': $('input[name=_token]').val(),
                'desc': $('#desc').val(),
                'cat_id': $('#category').val() != null ? $('#category').val() : '',
            },
            success: function(response){
                // reload datatable
                table.ajax.url( api ).load();
                table_2.ajax.url( api ).load();
                $('#setupModal').modal('hide');
                toastr.success('Setup item successfully created!', 'Success!');
                console.log('Setup item successfully created.');
            },
            error: function(response){
                console.log('Error: ' + response.status);
            }
        });
    });

    // Edit button is clicked
    $(document).on('click', '.editItem', function(){
        $('#modal-header-title').text('Edit ' + title);
        $('#btnPost span').text(' Update');
        $('#btnPost').addClass('btnUpdate');
        $('#btnPost').removeClass('btnSave');
        // Assign value to input field.
        $('#desc').val($(this).data('desc'));
        if($('#setupTable_2').data('api') != null) {
            $('#category').val($(this).data('cat_id')).trigger('change');
        }
        
        id = $(this).data('id');

        $('#setupModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
        $('#setupModal').on('shown.bs.modal', function () {
            $('#desc').focus();
        });
    });
    // Update Item
    $(document).on('click', '.btnUpdate', function(){
        $.ajax({
            type:   'PUT',
            url:    api + id,
            data:   {
                '_token': $('input[name=_token]').val(),
                'desc': $('#desc').val(),
                'cat_id': $('#category').val() != null ? $('#category').val() : '',
            },
            success: function(response){
                // reload datatable
                table.ajax.url( api ).load();
                table_2.ajax.url( api ).load();
                $('#setupModal').modal('hide');
                toastr.success('Setup item successfully updated!', 'Success!');
                console.log('Setup item successfully updated.');
            },
            error: function(response){
                console.log('Error: ' + response.status);
            }
        });
    });

    // Delete Item Modal
    $(document).on('click', '.deleteItem', function(){
        id = $(this).data('id');
        desc = $(this).data('desc');
        $('#deleteModal-header-title').text('Delete : ' + title);
        $('#itemName').text(desc);

        $('#deleteItemModal').modal({
            show:  true,
            backdrop: 'static',
            keyboard: false,
        });
    });
    // Confirm Delete Item
    $(document).on('click', '#btnConfirmDelete', function(){
        $.ajax({
            type: 'DELETE',
            url:  api + id,
            data: {
            '_token': $('input[name=_token]').val(),
            },
            success:  function(response){
                if(response.error==null) {
                    table.ajax.url( api ).load();
                    table_2.ajax.url( api ).load();
                    console.log('Setup item successfully deleted.');
                    toastr.success('Setup item has been deleted!', 'Success!')
                } else {
                    toastr.warning(response.error, "Warning!");
                    console.log(response.error);
                }
            },
            error:  function(response){
            console.log('Error: ' + response.status)
            }
        });

        $('#deleteItemModal').modal('hide');
    });

  });
</script>
@endsection