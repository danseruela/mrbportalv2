@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@section('title', 'Employee List')

@section('page_title', 'Employee List')

@section('breadcrumb_title', 'Employee List')

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="{{ route('employee.create') }}" class="btn btn-success">Create Employee</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    @if (count($employees) > 0)  
        <table id="employeeTbl1" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>Employee No.</th>
              <th>Employee Name</th>
              <th>Company</th>
              <th>Date Employed</th>
              <th>Employment Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($employees as $emp)  
              @php 
                $status = $emp->getStatus->desc == 'Resigned' ? 'red' : 'green';
              @endphp
            <tr>
              <td><a class="text-success" href="{{ route('employee.show', ['id' => $emp->id]) }}">{{ $emp->emp_no }}</a></td>
              <td>{{ $emp->lastname }}, {{ $emp->firstname }} {{ $emp->middlename }}</td>
              <td>{{ $emp->getOrg['desc'] }}</td>
              <td>{{ $emp->emp_start_date }}</td>
              <td><span class="badge bg-{{$status}}">{{ $emp->getStatus->desc }}</span></td>
              <td>
                  <a href="{{ route('employee.show', ['id' => $emp->id]) }}" class="btn btn-warning" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
                  <a href="#" class="btn btn-danger" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Employee No.</th>
              <th>Employee Name</th>
              <th>Company</th>
              <th>Date Employed</th>
              <th>Employment Status</th>
              <th>Actions</th>
            </tr>
          </tfoot>
        </table>  <!-- / table -->  
    @endif      
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#employeeTbl1').DataTable({
      'order'       : [[ 1, 'asc' ]], 
    });
    $('#employeeTbl2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
@endsection