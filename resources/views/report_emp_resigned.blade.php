@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net/css/buttons.dataTables.min.css') }}">
  <!-- Media Print -->
  <link rel="stylesheet" media="print" href="{{ asset('custom/css/print.css') }}">
  <!-- custom -->
  <style>
    .report-filter { margin-bottom: 20px; }
  </style>
@endsection

@section('title', $report_title)

@section('page_title', 'Reports')

@section('breadcrumb_title', 'Reports')

@section('content')

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <h2 class="page-header text-center">
        <!-- <i class="fa fa-file"></i> -->
        <span id="report_title">{{ $report_title }}</span>
        <small>as of </small>
        <small id="cutoff_title">Cut-off dates</small>
      </h2>
    </div><!-- /.box-header -->
    <div class="box-body"> 
      <div class="report-filter">
          <a href="#" class="btn btn-success setCutoff">
              <span id="" class='fa fa-calendar'></span> Cutoff Dates
          </a>
      </div>
      <table id="mbTbl2" class="table table-striped dt-responsive">
          <thead>
            <tr>
                <th>Employee No.</th>
                <th>Employee Name</th>
                <th>Designation</th>
                <th>Job Level</th>
                <th>Employment Status</th>
                <th>Date Resigned</th>
            </tr>
          </thead>
      </table>  <!-- / table -->  
    
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->

<!-- MODALS -->
<!-- Modal form to set cutoff dates -->
<div id="generateReport" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Set-up: Cut-off dates</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="date_from" class="col-sm-2 control-label">Date from</label>
                        <div class="col-xs-10">
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right datepicker" id="date_from" name="date_from" placeholder="Date From" required>
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date_to" class="col-sm-2 control-label">Date To</label>
                        <div class="col-xs-10">
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right datepicker" id="date_to" name="date_to" placeholder="Date To" required>
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>    
                        </div>
                    </div>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success generate">
                    <span id="" class='fa fa-refresh'></span> Generate Report
                </button>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <span class='glyphicon glyphicon-remove'></span> Close
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- DataTables Buttons -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/pdfmake_vfs_fonts.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net/js/buttons.print.min.js') }}"></script>
<!-- page script -->
<script>
$(document).ready(function(){ 
    // Initialize DataTables
    var table = $('#mbTbl2').DataTable({
      // 'ajaxSource'  : '/reports/emp-master/1',
      'columns'     : [
        { 
          'data'    :  'emp_no',
          'render'  :  function(data, type, row, meta) {
            if(type==='display') {
              data = '<a href="/reports/employee/' + row.id + '" class="text-success" target="_blank">' + data + '</a>';
            }
            return data;
          }   
        },
        { 'data':  'employee_name' },
        { 'data':  'designation' },
        { 'data':  'job_level' },
        { 'data':  'emp_status' },
        { 'data':  'emp_end_date' },
      ],
      'dom'         : 'Bfrtip',
      'buttons'     : [
                        {
                          extend: 'pdf',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-file"></i> Export PDF',
                        }, 
                        {
                          extend: 'csv',
                          charset: 'UTF-16LE',
                          bom: true,
                          text: '<i class="fa fa-table"></i> Export CSV',
                        },
                        {
                          extend: 'print', 
                          text: '<i class="fa fa-print"></i> <u>P</u>rint', 
                          autoPrint: true,
                          customize: function(win) {
                            $(win.document.body).css({margin: '0 20px'});
                          },
                        }
                      ],
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });

    // Initialize modal upon page load.
    $('#generateReport').modal('show');

    // show cut-off dates modal.
    $(document).on('click', '.setCutoff', function(){
        $('#generateReport').modal('show');
    });

    $(document).on('click', '.generate', function(){
        // $.ajax({
        //     type:   'POST',
        //     url:    '/reports/emp-master/resigned',
        //     data:   {
        //         '_token':       $('input[name=_token]').val(),
        //         'date_from':    $('#date_from').val(),
        //         'date_to':      $('#date_to').val(),
        //     },
        //     success: function(response){
        //         // reload datatable
        //         table.ajax.url( '/reports/emp-master/resigned' ).load();
        //         $('#generateReport').modal('hide');
        //     },
        //     error: function(response) {

        //     },
        // });
        if( $('#date_from').val() == '' || $('#date_to').val() == '' ) {
            toastr.warning('Please supply empty fields.', 'Warning!');
        } else {
          // reload data table
          table.ajax.url( '/reports/emp-master/resigned/' + $('#date_from').val() + '/' + $('#date_to').val() ).load();
          $('#cutoff_title').text($('#date_from').val() + ' to ' + $('#date_to').val())
          $('#generateReport').modal('hide');
        }
        
    });

});
</script>
@endsection