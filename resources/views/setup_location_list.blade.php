@extends('layouts.master')

@section('css_styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
@endsection

@section('title', 'Location')

@section('page_title', 'Location')

@section('breadcrumb_title', 'Location')

@section('content')

@if ($flash = session('message'))
  <div id="toast-container" class="flash-message toast-bottom-right">
    <div class="toast toast-success" aria-live="polite" style="display: block;">
      <div class="toast-message">{{ $flash }}</div>
    </div>  
  </div>
@endif

<div class="row">
<div class="col-xs-12">
  <div class="box box-success">
    <div class="box-header">
      <a href="{{ route('location.create') }}" class="btn btn-success">Create Location</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    @if (count($locations) > 0)  
        <table id="mbTbl1" class="table table-bordered table-hover dt-responsive">
          <thead>
            <tr>
              <th>ID</th>
              <th>Organization</th>
              <th>Location Name</th>
              <th>Address</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($locations as $location)  
            <tr>
              <td><a class="text-success" href="{{ route('location.edit', ['id' => $location->id]) }}">{{ $location->id }}</a></td>
              <td>{{ $location->getOrg->desc }}</td>
              <td><a class="text-success" href="{{ route('location.edit', ['id' => $location->id]) }}">{{ $location->desc }}</a></td>
              <td>{{ $location->address }}</td>
              <td>
                <a href="#" class="delete-modal btn btn-danger" data-toggle="tooltip" title="Delete" data-id="{{ $location->id }}" data-desc="{{ $location->desc }}"><i class="fa fa-trash"></i></a>
                <a href="{{ route('location.edit', ['id' => $location->id]) }}" class="btn btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
              </td>
            </tr>
          @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Organization</th>
              <th>Location Name</th>
              <th>Address</th>
              <th>Actions</th>
            </tr>
          </tfoot>
        </table>  <!-- / table -->  
    @endif      
    </div>  <!-- / .box-body -->  
  </div> <!-- / .box -->  
</div>  <!-- / .col -->
</div>  <!-- / .row -->
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#mbTbl1').DataTable()
    $('#mbTbl2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection