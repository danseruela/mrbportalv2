<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToEmpBasicSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_basic_salary', function (Blueprint $table) {
            $table->integer('sal_component_type')->after('pay_grd_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_basic_salary', function (Blueprint $table) {
            $table->dropColumn([ 'sal_component_type']);
        });
    }
}
