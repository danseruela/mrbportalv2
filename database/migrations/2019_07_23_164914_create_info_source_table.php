<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_source', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        DB::table('info_source')->insert([
            ['desc' => 'Philippine Drug Enforcement Agency'],
            ['desc' => 'Philippine National Police'],
            ['desc' => 'Police Regional Office - 7'],
            ['desc' => 'United Nations Security Council (AMLC)'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_source');
    }
}
