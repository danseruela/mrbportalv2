<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachment_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        App\AttachmentCategory::insert([
            ['desc' => 'Customer Information'],
            ['desc' => 'Loan Account'],
            ['desc' => 'Deposit Account'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachment_category');
    }
}
