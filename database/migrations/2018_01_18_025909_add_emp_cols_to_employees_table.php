<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpColsToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->integer('job_cat')->after('job_title');
            $table->string('job_specifics')->after('job_cat');
            $table->date('emp_start_date')->after('emp_status');
            $table->date('emp_end_date')->after('emp_start_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn([ 'job_cat', 'job_specifics', 'emp_start_date', 'emp_end_date' ]);
        });
    }
}
