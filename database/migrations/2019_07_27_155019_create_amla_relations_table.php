<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmlaRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amla_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amla_id');
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->integer('type');
            $table->integer('consanguinity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amla_relations');
    }
}
