<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('address1')->after('job_title');
            $table->string('address2')->after('address1');
            $table->string('city')->after('address2');
            $table->string('province')->after('city');
            $table->string('postal_code')->after('province');
            $table->integer('country_id')->after('postal_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn(['address1', 'address2', 'city', 'province', 'postal_code', 'country_id']);
        });
    }
}
