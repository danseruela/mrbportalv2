<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmlaGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amla_group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
            $table->integer('amla_type');
            $table->date('date_issued');
            $table->datetime('created_at');
            $table->integer('created_by');
            $table->datetime('modified_at')->nullable();
            $table->integer('modified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amla_group');
    }
}
