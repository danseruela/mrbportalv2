<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrimeTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crime_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        DB::table('crime_type')->insert([
            ['desc' => 'Arson'],
            ['desc' => 'Carnapping'],
            ['desc' => 'Dangerous Drug Manufacturers and Traffickers'],
            ['desc' => 'Drug Protector'],
            ['desc' => 'Grave Coercion'],
            ['desc' => 'Homicide'],
            ['desc' => 'ISIL (Da’esh) and Al-Qaida Sanctions List'],
            ['desc' => 'Kidnapping'],
            ['desc' => 'Kidnapping with Ransom'],
            ['desc' => 'Kidnapping with Ransom and Serious Illegal Detention'],
            ['desc' => 'Motornapping'],
            ['desc' => 'Murder'],
            ['desc' => 'Qualified Theft'],
            ['desc' => 'Rebellion'],
            ['desc' => 'Robbery'],
            ['desc' => 'Robbery in Band '],
            ['desc' => 'Robbery with Force Upon Things In An Inhabited House'],
            ['desc' => 'Theft'],
            ['desc' => 'Violation of RA 6539 (Anti-Carnapping Act)'],
            ['desc' => 'Violation of RA 9165'],
            ['desc' => 'Violation of RA 3019 Section 3 (Anti-Graft and Corrupt Practices Act)'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crime_type');
    }
}
