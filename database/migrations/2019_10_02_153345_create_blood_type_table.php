<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloodTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blood_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        App\BloodType::insert([
            ['desc' => 'None'],
            ['desc' => 'O-'],
            ['desc' => 'O+'],
            ['desc' => 'A-'],
            ['desc' => 'A+'],
            ['desc' => 'B-'],
            ['desc' => 'B+'],
            ['desc' => 'AB-'],
            ['desc' => 'AB+'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blood_type');
    }
}
