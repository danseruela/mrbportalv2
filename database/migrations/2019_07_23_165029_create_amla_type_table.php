<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmlaTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amla_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        DB::table('amla_type')->insert([
            ['desc' => 'International, National & Local Watchlist'],
            ['desc' => 'Watchlist Countries'],
            ['desc' => 'Watchlist Corporation'],
            ['desc' => 'Government Agencies'],
            ['desc' => 'National Officials'],
            ['desc' => 'Local Officials'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amla_type');
    }
}
