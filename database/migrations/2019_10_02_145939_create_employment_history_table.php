<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employment_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->string('employer_name');
            $table->string('employer_address');
            $table->string('designation');
            $table->string('employment_id_no')->nullable();
            $table->date('date_start');
            $table->date('date_end');
            $table->string('tel_no')->nullable();
            $table->string('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employment_history');
    }
}
