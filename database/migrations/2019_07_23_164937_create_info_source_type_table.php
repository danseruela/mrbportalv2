<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoSourceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_source_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        DB::table('info_source_type')->insert([
            ['desc' => 'Media'],
            ['desc' => 'Common Knowledge/Reputation'],
            ['desc' => 'Intellegence Report/Authority'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_source_type');
    }
}
