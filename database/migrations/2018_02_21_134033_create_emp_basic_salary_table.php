<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpBasicSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_basic_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_code');
            $table->integer('pay_grd_code')->nullable();
            $table->decimal('basic_salary', 9, 2);
            $table->integer('pay_freq_code');
            $table->string('salary_component');
            $table->string('comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_basic_salary');
    }
}
