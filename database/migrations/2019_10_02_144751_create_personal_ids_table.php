<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id');
            $table->integer('type');
            $table->string('id_no');
            $table->date('date_issued')->nullable();
            $table->date('date_expiry')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_ids');
    }
}
