<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_level', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        App\EducationLevel::insert([
            ['desc' => 'Elementary'],
            ['desc' => 'High School'],
            ['desc' => 'High School Undergraduate'],
            ['desc' => 'College'],
            ['desc' => 'College Undergraduate'],
            ['desc' => 'Vocational'],
            ['desc' => 'Masters Degree'],
            ['desc' => 'Doctoral Degree'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_level');
    }
}
