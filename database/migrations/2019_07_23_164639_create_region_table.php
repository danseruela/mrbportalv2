<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        DB::table('region')->insert([
            ['desc' => 'Region I'],
            ['desc' => 'Region II'],
            ['desc' => 'Region III'],
            ['desc' => 'Region IV'],
            ['desc' => 'Region V'],
            ['desc' => 'Region VI'],
            ['desc' => 'Region VII'],
            ['desc' => 'Region VIII'],
            ['desc' => 'Region IX'],
            ['desc' => 'Region X'],
            ['desc' => 'Region XI'],
            ['desc' => 'Region XII'],
            ['desc' => 'National'],
            ['desc' => 'International'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('region');
    }
}
