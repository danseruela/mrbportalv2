<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('id_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc');
        });

        App\IDType::insert([
            ['desc' => 'SSS'],
            ['desc' => 'TIN'],
            ['desc' => 'PHILHEALTH'],
            ['desc' => 'HDMF'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('id_type');
    }
}
