<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ContactTypeTableSeeder::class,
            ContactCatTableSeeder::class,
            EmpEducTableSeeder::class,
            EmpSkillsTableSeeder::class,
            EmpStatusTableSeeder::class,
            JobTitleTableSeeder::class,
            JobCatTableSeeder::class,
            JobLevelTableSeeder::class,
            CountriesTableSeeder::class,
            CivilStatusTableSeeder::class,
            GenderTableSeeder::class,
            RelTypeTableSeeder::class,
            RelConsanguinityTableSeeder::class,
            UserRoleTableSeeder::class,
            EmployeeTableSeeder::class,
            OrganizationTableSeeder::class,
            LocationTableSeeder::class,
            UserTableSeeder::class,
        ]);
    }
}
