<?php

use Illuminate\Database\Seeder;

class JobCatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$job_categories = array(
    		array('desc'    =>  'Accounting'),
    		array('desc'    =>  'Admin & Clerical'),
    		array('desc'    =>  'Automotive'),
    		array('desc'    =>  'Banking'),
    		array('desc'    =>  'Biotech'),
    		array('desc'    =>  'Business Development'),
    		array('desc'    =>  'Construction'),
    		array('desc'    =>  'Consultant'),
    		array('desc'    =>  'Customer Service'),
    		array('desc'    =>  'Design'),
    		array('desc'    =>  'Engineering'),
    		array('desc'    =>  'Entry Level - New Grad'),
    		array('desc'    =>  'Executive'),
    		array('desc'    =>  'Finance'),
    		array('desc'    =>  'General Business'),
    		array('desc'    =>  'Health Care'),
    		array('desc'    =>  'Human Resources'),
    		array('desc'    =>  'Information Technology'),
    		array('desc'    =>  'Maintenance'),
    		array('desc'    =>  'Insurance'),
    		array('desc'    =>  'Inventory'),
    		array('desc'    =>  'Legal'),
    		array('desc'    =>  'Legal Admin'),
    		array('desc'    =>  'Management'),
    		array('desc'    =>  'Manufacturing'),
    		array('desc'    =>  'Marketing'),
    		array('desc'    =>  'Other'),
    		array('desc'    =>  'Quality Control'),
    		array('desc'    =>  'Real Estate'),
    		array('desc'    =>  'Research'),
    		array('desc'    =>  'Sales'),
    		array('desc'    =>  'Science'),
    		array('desc'    =>  'Skilled Labor'),
    		array('desc'    =>  'Strategy Planning'),
    		array('desc'    =>  'Telecommunications'),
    		array('desc'    =>  'Training'),
    		array('desc'    =>  'Transportation'),
    		array('desc'    =>  'Warehouse'),
    	);

        \App\JobCat::insert($job_categories);
    }
}
