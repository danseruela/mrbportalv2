<?php

use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee_admin = array(
        	[ 
        		'emp_no' 			=> 	'SysAd - 01', 
        		'lastname' 			=> 	'Administrator',
        		'firstname'			=>	'System',
        		'middlename'		=>	'Admin',
        		'nickname'			=>	'admin',
        		'birthdate'			=>	date('Y-m-d h:i:s'),
				'gender'			=>	1,
				'blood_type'		=>	7,
        		'civil_status'		=>	1,
        		'emp_status'		=>	5,
        		'emp_start_date'	=>	date('Y-m-d h:i:s'),
        		'org_code'			=>	1,
        		'location_code'		=>	1,
				'job_title'			=>	11,
				'job_level'			=>	6,
        		'job_cat'			=>	18,
        		'job_specifics'		=>	'System Administrator',
        		'address1'			=>	'Pajo',
        		'address2'			=>	'Pajo',
        		'city'				=>	'Lapu-Lapu City',
        		'province'			=>	'Cebu',
        		'postal_code'		=>	'6015',
        		'country_id'		=>	155,
        		'created_at'		=>	date('Y-m-d h:i:s'),
        		'created_by'		=>	1,
        	],
        );

        \App\Employee::insert($employee_admin);
    }
}
