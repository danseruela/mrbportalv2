<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = array(
    		array('org_code' => 1, 'desc'  =>  'Head Office', 'address' => 'MYFC Bldg., M. Patalinghog Ave., Pajo, Lapu-Lapu City 6015'),
            array('org_code' => 1, 'desc'  =>  'Main Branch', 'address' => 'MYFC Bldg., M. Patalinghog Ave., Pajo, Lapu-Lapu City 6015'),
            array('org_code' => 1, 'desc'  =>  'Bogo Branch', 'address' => 'Cogon, Bogo City, Cebu'),
    		array('org_code' => 1, 'desc'  =>  'Carcar Branch', 'address' => 'Valladolid, Carcar City, Cebu'),
            array('org_code' => 1, 'desc'  =>  'Cebu City Branch', 'address' => 'Pahina, San Nicolas, Cebu City, Cebu'),
            array('org_code' => 1, 'desc'  =>  'Liloan Branch', 'address' => 'Poblacion, Liloan, Cebu'),
            array('org_code' => 1, 'desc'  =>  'Toledo Branch', 'address' => 'Sangi, Toledo City, Cebu'),
    		array('org_code' => 1, 'desc'  =>  'Tacloban City Branch', 'address' => 'Maharlika Highway, Tacloban City, Leyte'),
    		array('org_code' => 1, 'desc'  =>  'Moalboal Branch', 'address' => 'Poblacion East, Moalboal, Cebu'),
    		array('org_code' => 1, 'desc'  =>  'Ormoc Branch', 'address' => 'Ormoc City, Leyte'),
    		array('org_code' => 1, 'desc'  =>  'Dumaguete Branch', 'address' => 'Taclobo, Dumaguete City, Negros Oriental'),
    	);

        \App\Location::insert($locations);
    }
}
