<?php

use Illuminate\Database\Seeder;

class EmpSkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$skills = array(
    		array('name' => 'Accounting', 'desc' => 'Accounting'),
    		array('name' => 'Active Listening', 'desc' => 'Active Listening'),
    		array('name' => 'Administrative', 'desc' => 'Administrative'),
    		array('name' => 'Analytical', 'desc' => 'Analytical'),
    		array('name' => 'Behavioral', 'desc' => 'Behavioral'),
    		array('name' => 'Blue Collar', 'desc' => 'Blue Collar'),
    		array('name' => 'Business Intelligence', 'desc' => 'Business Intelligence'),
    		array('name' => 'Business', 'desc' => 'Business'),
    		array('name' => 'Business Storytelling', 'desc' => 'Business Storytelling'),
    		array('name' => 'Clerical', 'desc' => 'Clerical'),
    		array('name' => 'Collboration', 'desc' => 'Collboration'),
    		array('name' => 'Communication', 'desc' => 'Communication'),
    		array('name' => 'Computer', 'desc' => 'Computer'),
    		array('name' => 'Conceptual', 'desc' => 'Conceptual'),
    		array('name' => 'Conflict Management', 'desc' => 'Conflict Management'),
    		array('name' => 'Conflict Resolution', 'desc' => 'Conflict Resolution'),
    		array('name' => 'Content Strategy', 'desc' => 'Content Strategy'),
    		array('name' => 'Creative Thinking', 'desc' => 'Creative Thinking'),
    		array('name' => 'Customer Service', 'desc' => 'Customer Service'),
    		array('name' => 'Decision Making', 'desc' => 'Decision Making'),
    		array('name' => 'Digital Marketing', 'desc' => 'Digital Marketing'),
    		array('name' => 'Entrepreneurial', 'desc' => 'Entrepreneurial'),
    		array('name' => 'Finance', 'desc' => 'Finance'),
    		array('name' => 'Information Technology', 'desc' => 'Information Technology'),
    		array('name' => 'Leadership', 'desc' => 'Leadership'),
    		array('name' => 'Logical Thinking', 'desc' => 'Logical Thinking'),
    		array('name' => 'Management', 'desc' => 'Management'),
    		array('name' => 'Marketing', 'desc' => 'Marketing'),
    		array('name' => 'MS Office Skills', 'desc' => 'MS Office Skills'),
    		array('name' => 'Multitasking', 'desc' => 'Multitasking'),
    		array('name' => 'Problem Solving', 'desc' => 'Problem Solving'),
    		array('name' => 'Public Speaking', 'desc' => 'Public Speaking'),
    		array('name' => 'Research', 'desc' => 'Research'),
    		array('name' => 'Sales', 'desc' => 'Sales'),
    		array('name' => 'Strategic Planning', 'desc' => 'Strategic Planning'),
    		array('name' => 'Teamwork', 'desc' => 'Teamwork'),
    		array('name' => 'Technical', 'desc' => 'Technical'),
    		array('name' => 'Writing', 'desc' => 'Writing'),
    	);

        \App\EmpSkills::insert($skills);
    }
}
