<?php

use Illuminate\Database\Seeder;

class EmpStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$emp_status = array(
    		array('desc'    =>  'Probationary'),
    		array('desc'    =>  'Part-time'),
    		array('desc'    =>  'Fixed-term'),
    		array('desc'    =>  'Casual'),
    		array('desc'    =>  'Regular'),
    	);

        \App\EmpStatus::insert($emp_status);
    }
}
