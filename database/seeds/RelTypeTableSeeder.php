<?php

use Illuminate\Database\Seeder;

class RelTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $relation_type = array(
    		array('desc'  =>  'Great Grand Father'),
    		array('desc'  =>  'Great Grand Mother'),
    		array('desc'  =>  'Grand Father'),
    		array('desc'  =>  'Grand Mother'),
            array('desc'  =>  'Grand Father-In-Law'),
            array('desc'  =>  'Grand Mother-In-Law'),
    		array('desc'  =>  'Father'),
    		array('desc'  =>  'Mother'),
    		array('desc'  =>  'Father-In-Law'),
    		array('desc'  =>  'Mother-In-Law'),
    		array('desc'  =>  'Husband'),
    		array('desc'  =>  'Wife'),
            array('desc'  =>  'Son'),
            array('desc'  =>  'Daughter'),
            array('desc'  =>  'Son-In-Law'),
            array('desc'  =>  'Daughter-In-Law'),
            array('desc'  =>  'Grand Son'),
            array('desc'  =>  'Grand Daughter'),
            array('desc'  =>  'Grand Son-In-Law'),
            array('desc'  =>  'Grand Daughter-In-Law'),
    		array('desc'  =>  'Brother'),
    		array('desc'  =>  'Sister'),
    		array('desc'  =>  'Brother-In-Law'),
    		array('desc'  =>  'Sister-In-Law'),
    		array('desc'  =>  'Uncle'),
    		array('desc'  =>  'Aunt'),
    		array('desc'  =>  'Cousin'),
    		array('desc'  =>  'Nephew'),
    		array('desc'  =>  'Niece'),
    	);

        \App\RelType::insert($relation_type);
    }
}
