<?php

use Illuminate\Database\Seeder;

class ContactTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$contact_type = array(
    		array('desc'	=> 'Telephone Number'),
    		array('desc'	=> 'Mobile Number'),
    		array('desc'	=> 'Address'),
    		array('desc'	=> 'Email Address'),
    		array('desc'	=> 'Others'),
    	);

        \App\ContactType::insert($contact_type);
    }
}
