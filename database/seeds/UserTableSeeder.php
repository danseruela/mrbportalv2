<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserRole;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_admin = UserRole::where('name', 'Administrator')->first();

        $user = new User();
        $user->emp_id = 1;
        $user->username = 'admin';
        $user->password = bcrypt('password');
        $user->deleted = 0;
        $user->is_first_login = 0;
        $user->created_at = date('Y-m-d h:i:s');
        $user->created_by = 1;
        $user->save();
        $user->roles()->attach($user_admin);
        
    }
}
