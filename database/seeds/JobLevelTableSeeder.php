<?php

use Illuminate\Database\Seeder;

class JobLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job_level = array(
            array('desc'    =>  'Directors'),
            array('desc'    =>  'Stockholders'),
            array('desc'    =>  'Executives'),
            array('desc'    =>  'Corporate Managers'),
            array('desc'    =>  'Branch Managers'),
            array('desc'    =>  'Supervisors'),
            array('desc'    =>  'Rank & File'),
        );

        \App\JobLevel::insert($job_level);
    }
}
