<?php

use Illuminate\Database\Seeder;

class RelConsanguinityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $consanguinity = array(
    		array('desc1'  =>  '1st degree', 'desc2'	=>	'1st degree'),
    		array('desc1'  =>  '2nd degree', 'desc2'	=>	'2nd degree'),
    		array('desc1'  =>  '3rd degree', 'desc2'	=>	'3rd degree'),
    		array('desc1'  =>  '4th degree', 'desc2'	=>	'4th degree'),
    		array('desc1'  =>  '5th degree', 'desc2'	=>	'5th degree'),
    		array('desc1'  =>  '6th degree', 'desc2'	=>	'6th degree'),
    		array('desc1'  =>  '7th degree', 'desc2'	=>	'7th degree'),
    	);

        \App\RelConsanguinity::insert($consanguinity);
    }
}
