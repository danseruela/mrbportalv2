<?php

use Illuminate\Database\Seeder;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $affiliates = array(
    		array('desc'  =>  'Mactanbank Inc.',),
            array('desc'  =>  'Mr. Yu Finance Corporation',),
            array('desc'  =>  'Citizens Lending Corporation',),
    		array('desc'  =>  'Asaynica Realty Development Corporation',),
            array('desc'  =>  'Ernesto Asay Yu Foundation',),
    	);

        \App\Organization::insert($affiliates);
    }
}
