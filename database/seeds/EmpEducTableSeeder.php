<?php

use Illuminate\Database\Seeder;

class EmpEducTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$education = array(
    		array('desc'	=>	'Primary Education'),
    		array('desc'	=>	'Secondary Education'),
    		array('desc'	=>	'Bachelor Degree'),
    		array('desc'	=>	'Graduate Diploma'),
    		array('desc'	=>	'Postgraduate Degree'),
    	);

        \App\EmpEduc::insert($education);
    }
}
