<?php

use Illuminate\Database\Seeder;

class PayFrequencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pay_frequency = array(
    		array('desc'  =>  'Bi Weekly'),
    		array('desc'  =>  'Hourly'),
    		array('desc'  =>  'Monthly'),
    		array('desc'  =>  'Monthly on 1st pay of month'),
    		array('desc'  =>  'Semi-Monthly'),
    		array('desc'  =>  'Weekly'),
    	);

        \App\PayFrequency::insert($pay_frequency);
    }
}
