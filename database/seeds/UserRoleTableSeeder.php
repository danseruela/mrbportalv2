<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
        	array('id' => 1, 'name' => 'User', 'description' => 'A normal user.'),
        	array('id' => 2, 'name' => 'Content Manager', 'description'  => 'Manages employee informations.'),
            array('id' => 3, 'name' => 'Reporting', 'description' => 'Can generate reports.'),
        	array('id' => 99, 'name' => 'Administrator', 'description' => 'A system administrator.'),
        );

        \App\UserRole::insert($roles);
    }
}
