<?php

use Illuminate\Database\Seeder;

class SalComponentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sal_component_type = array(
        	array('id' => 1, 'desc' => 'Basic'),
        	array('id' => 2, 'desc' => 'Benefits'),
        	array('id' => 3, 'desc' => 'Deductions'),
        );

        \App\SalComponentType::insert($sal_component_type);
    }
}
