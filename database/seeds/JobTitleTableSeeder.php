<?php

use Illuminate\Database\Seeder;

class JobTitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$job_titles = array(
    		array('cat_id' => 13, 'desc'  =>  'Director',),
            array('cat_id' => 13, 'desc'  =>  'Stockholder',),
            array('cat_id' => 13, 'desc'  =>  'President',),
    		array('cat_id' => 24, 'desc'  =>  'VP-Operation',),
            array('cat_id' => 24, 'desc'  =>  'Treasurer',),
            array('cat_id' => 17, 'desc'  =>  'HR Manager',),
            array('cat_id' => 17, 'desc'  =>  'HR Staff',),
    		array('cat_id' => 1, 'desc'  =>  'Accounting Manager',),
    		array('cat_id' => 1, 'desc'  =>  'Accounting Staff',),
    		array('cat_id' => 18, 'desc'  =>  'IT Manager',),
    		array('cat_id' => 18, 'desc'  =>  'IT Staff',),
    		array('cat_id' => 26, 'desc'  =>  'Marketing Manager',),
    		array('cat_id' => 14, 'desc'  =>  'Finance Manager',),
            array('cat_id' => 24, 'desc'  =>  'Audit Manager',),
            array('cat_id' => 24, 'desc'  =>  'Audit Staff',),
            array('cat_id' => 24, 'desc'  =>  'Compliance Officer',),
    		array('cat_id' => 4, 'desc'  =>  'Credit Manager',),
            array('cat_id' => 4, 'desc'  =>  'Credit Officer',),
            array('cat_id' => 24, 'desc'  =>  'Area Manager',),
    		array('cat_id' => 24, 'desc'  =>  'Branch Manager',),
    		array('cat_id' => 4, 'desc'  =>  'Bookkeepper',),
    		array('cat_id' => 4, 'desc'  =>  'Loans Officer',),
            array('cat_id' => 4, 'desc'  =>  'Loans Clerk',),
    		array('cat_id' => 4, 'desc'  =>  'Cashier',),
    		array('cat_id' => 4, 'desc'  =>  'Teller',),
    		array('cat_id' => 4, 'desc'  =>  'New Accounts',),
    	);

        \App\JobTitle::insert($job_titles);
    }
}
