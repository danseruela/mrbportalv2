<?php

use Illuminate\Database\Seeder;

class ContactCatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contact_category = array(
    		array('desc'	=> 'Personal'),
    		array('desc'	=> 'Work'),
    		array('desc'	=> 'Others'),
    	);

        \App\ContactCat::insert($contact_category);
    }
}
