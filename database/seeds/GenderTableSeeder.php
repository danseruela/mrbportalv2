<?php

use Illuminate\Database\Seeder;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gender = array(
    		array('desc'    =>  'Male'),
    		array('desc'    =>  'Female'),
    	);

        \App\Gender::insert($gender);
    }
}
