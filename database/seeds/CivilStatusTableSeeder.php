<?php

use Illuminate\Database\Seeder;

class CivilStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$civil_status = array(
    		['desc'	=>	'Single'],
    		['desc'	=>	'Married'],
    		['desc'	=>	'Widowed'],
    		['desc'	=>	'Others']
    	);

        \App\CivilStatus::insert($civil_status);
    }
}
