<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_roles = array(
            ['user_id' => 1, 'role_id' => 99],
            ['user_id' => 2, 'role_id' => 2],
            ['user_id' => 1, 'role_id' => 3],
        );

        DB::table('users_roles')->insert($user_roles);
    }
}
