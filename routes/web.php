<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/{any}', function(){
// 	return view('home');
// })->where('any', '.*');

Route::get('/', ['as' => 'login', 'uses' => 'HomeController@showLogin']);

Route::post('/', ['as' => 'login', 'uses' => 'HomeController@doLogin']);

Route::get('logout', ['uses' => 'HomeController@doLogout']);

Route::get('dashboard', function() {
		return view('dashboard')->with([
			'organizations'		=>	App\Organization::where('id', '<=', 5)->get(),
			'total_employees'	=>	App\Employee::where('emp_no', 'NOT LIKE', '%SysAd%')->count(),
			'total_users'		=>	App\User::count(),
			'dosri'				=>	App\Relations::leftJoin('employees', 'relations.emp_id', '=', 'employees.id')->whereIn('employees.job_level', [1,2,3,4,5,6])->count(),
			'era'				=>	App\Relations::leftJoin('employees', 'relations.emp_id', '=', 'employees.id')->where('job_level', '=', 7)->count(),
		]);
})->middleware('auth');

Route::post('dashboard', 'QuickEmailController@send')->middleware('auth');

Route::get('genderdata', 'ReportController@genderChart');
Route::get('empstatusdata', 'ReportController@empStatusChart');

Route::get('profile', function() {
	return view('profile')->with([
		'emp_details'		=>	App\Employee::where('id', auth()->user()->emp_id)->get(),
		'contacts'      	=>  App\Contacts::where('emp_id', '=', auth()->user()->emp_id)->orderBy('category')->get(),
		'relations'			=>	App\Relations::where('emp_id', '=', auth()->user()->emp_id)->orderBy('consanguinity')->get(),
		'personal_ids'		=>	DB::table('personal_ids')->select('personal_ids.*', 'id_type.desc AS id_type')
								->leftJoin('id_type', 'id_type.id', '=', 'personal_ids.type')
								->where('emp_id', auth()->user()->emp_id)
								->get(),
		'educ_attainment'	=>	DB::table('educational_attainment')->select('educational_attainment.*', 'education_level.desc AS educ_level')
								->leftJoin('education_level', 'education_level.id', '=', 'educational_attainment.level')
								->where('emp_id', auth()->user()->emp_id)
								->orderBy('educational_attainment.year_start')
								->get(),
		'emp_history'		=>	DB::table('employment_history')->select('employment_history.*')
								->where('emp_id', auth()->user()->emp_id)
								->orderBy('employment_history.date_start')
								->get(),
	]);
})->middleware('auth');

// Downloads
Route::get('era-form-download', ['as' => 'eraform.download', 'uses' => function() {
		return Response::download('downloads/ERA-Fillup-Form.xlsx', 'era-fillup-form.xlsx');
	}])->middleware('auth');
Route::get('rpt-form-download', ['as' => 'rptform.download', 'uses' => function() {
		return Response::download('downloads/RPT-Fillup-Form.xlsx', 'rpt-fillup-form.xlsx');
	}])->middleware('auth');
Route::get('it-request-form-download', ['as' => 'itrequestform.download', 'uses' => function() {
		return Response::download('downloads/IT-REQUEST-FORM.pdf', 'it-request-form.pdf');
	}])->middleware('auth');
Route::get('user-request-form-download', ['as' => 'userrequestform.download', 'uses' => function() {
		return Response::download('downloads/USER-REQUEST-FORM.pdf', 'user-request-form.pdf');
	}])->middleware('auth');

Route::resource('user', 'UserController');
Route::prefix('user')->group(function(){
	Route::put('/resetpassword/{id}', 'UserController@resetPassword');
	Route::put('/updatepassword/{id}', 'UserController@updatePassword');
});


Route::resource('employee', 'EmployeeController');
Route::prefix('employee')->group(function(){
	Route::post('/photo', 'EmpPhotoController@empImage');
	Route::delete('/removeContact/{id}', 'EmployeeController@removeContact');
	Route::delete('/relation/{id}', 'RelationController@destroy');
	Route::get('/locations/{id}', 'EmployeeController@orgLocationList');
	Route::get('/jobs/{id}', 'EmployeeController@catJobList');
	Route::resource('/relation', 'RelationController');
	Route::resource('/salary', 'EmpBasicSalaryController');
});

Route::get('amla', function(Request $request){
	return view('amla')->with([
		'page_title'	=>	'AMLA Watchlist',
		'api'			=>	'/api/amla/',
		'crime_types'	=>	App\CrimeType::select('crime_type.id', 'crime_type.desc AS text')->pluck('text', 'id'),
		'amla_types'	=>	App\AMLAType::pluck('desc', 'id'),
		'user_id'		=>	$request->user()->id,
		'user'			=>	$request->user(),
	]);
})->middleware('auth');

Route::get('amlagroup', function(Request $request){
	return view('amlagroup')->with([
		'page_title'		=>	'AMLA Group Watchlist',
		'api_countries'		=>	'/api/amlagroup/countries/type',
		'api_corporation'	=>	'/api/amlagroup/corporation/type',
		'user_id'			=>	$request->user()->id,
		'user'				=>	$request->user(),
	]);
})->middleware('auth');

Route::prefix('reports')->middleware('auth')->group(function(){
	Route::get('/employee-master', function(){
		return view('report_emp')->with([
			'report_title'		=>	'Employee Master List Report (All)',
			'companies'     	=>  App\Organization::whereBetween('organization.id', [1,5])->pluck('desc', 'id')
		]);
	});
	Route::get('/employee-master/{id}', function(Request $request){
		return view('report_emp')->with([
			'report_title'		=>	'Employee Master List Report (All)',
			'companies'     	=>  App\Organization::whereBetween('organization.id', [1,5])->pluck('desc', 'id'),
			'request'			=>	$request
		]);
	});
	Route::get('/employee/{id}', [
				'as'	=> 'reports.employee', 
				'uses'	=> 'ReportController@showEmpRelatives']);
	Route::get('/employee-related-accounts', function(){
		return view('report_era')->with(['report_title'=>'Employee Related Accounts Report (All)']);
	});
	Route::get('/employee-related-accounts/{group}', function(Request $request){
		return view('report_era')->with([
			'report_title'	=>	'Employee Related Accounts Report (All)',
			'request'		=>	$request
			]);
	});
	Route::get('/customer-uploads', function(Request $request){
		return view('report_customer_uploads')->with([
			'request'		=>	$request,
		]);
	});
	// Route::get('/attachments/{category}', function(Request $request){
	// 	return view('report_customer_uploads')->with([
	// 		'request'		=>	$request,
	// 	]);
	// });
	Route::get('/resigned', function(Request $request){
		return view('report_emp_resigned')->with([
			'report_title'	=>	'MRB - List of Resigned Employees',
			'request'		=>	$request
			]);
	});
	// API call
	Route::get('/emp-master/{id}', 'ReportController@employeesAPI');
	Route::get('/emp-master/resigned/{date_from}/{date_to}', 'ReportController@getResignedEmployees');
	Route::get('/emp-related-all', 'ReportController@employeeRelatedAllAPI');
	Route::get('/emp-related-dosri', 'ReportController@employeeRelatedDosriAPI');
	Route::get('/emp-related-accounts', 'ReportController@employeeRelatedAccountsAPI');
});

Route::prefix('job')->middleware('auth')->group(function(){
	Route::resource('/title', 'JobTitleController');
	Route::resource('/category', 'JobCategoryController');
	Route::resource('/level', 'JobLevelController');
	Route::resource('/status', 'EmpStatusController');
	Route::resource('/paygrade', 'PayGradeController');
});

Route::prefix('setup')->middleware('auth')->group(function(){
	//Route::resource('/skills', 'OrganizationController');
	//Route::resource('/education', 'OrganizationController');
	Route::resource('/organization', 'OrganizationController');
	Route::resource('/location', 'LocationController');
});

Route::prefix('setups')->middleware('auth')->group(function(){
	Route::get('/attachmentcategory', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	Route::get('/attachmenttype', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	Route::get('/amlatype', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	Route::get('/region', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	// Route::get('/postalcode', function(Request $request){
	// 	return view('setup_main')->with(['request' => $request]);
	// });
	Route::get('/crimetype', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	Route::get('/infosource', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	Route::get('/infosourcetype', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	Route::get('/idtype', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
	Route::get('/educlevel', function(Request $request){
		return view('setup_main')->with(['request' => $request]);
	});
});

Route::get('test', function(){
	$notifications = Auth::user()->unreadNotifications;
	foreach ($notifications as $notification) {
		dd($notification->data['reset']);
		$notification->markAsRead();
	}
});

Route::get('/markAsRead', function(){
	Auth::user()->unreadNotifications->markAsRead();
})->middleware('auth');


Route::get('json/{id}', [
		'as' 	=>	'json',
		'uses'	=>	'UserController@getRoles'
	]);

Route::get('auth', function (Request $request) {
        return $request->user()->id;
});	

// ICBS Connection
Route::get('icbs', function(){
	return response()->json([
		'data'	=>	App\Customer::select('customer_id', 'display_name')->limit(100)->get()
	]);
});

Route::get('customers', function(Request $request){
	return view('customers')->with([
		'page_title'	=>	'Customer List',
		'api'			=>	'/api/customer/',
		'user_id'		=>	$request->user()->id,
		'user'			=>	$request->user(),
	]);
})->middleware('auth');

Route::get('/customers/{id}', function(Request $request, $id){
	return view('customers-profile')->with([
		'page_title'	=>	'Customer Profile',
		'api'			=>	'/api/customer/' . $id,
		'api_docs'		=>	'/api/customer/' . $id . '/attachmenttype',
		'user_id'		=>	$request->user()->id,
		'user'			=>	$request->user(),
	]);
})->middleware('auth');


