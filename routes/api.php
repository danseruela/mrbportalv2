<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('related', 'ReportController@employeeRelatedAccountsAPI');

Route::get('amla/{amlatype}/group', 'AmlaController@grpByType');
Route::resource('amla', 'AmlaController');
Route::get('amlagroup/{amlatype}/type', 'AmlaGroupController@getGroupWatchlist');
Route::resource('amlagroup', 'AmlaGroupController');

Route::get('rel_type', 'AmlaRelationController@relTypes');
Route::get('rel_consanguinity', 'AmlaRelationController@relConsanguinity');
Route::get('amlarelations/{id}/show', 'AmlaRelationController@getRelation');
Route::resource('amlarelations', 'AmlaRelationController');

Route::post('personalid', 'EmpOtherInfoController@saveID');
Route::put('personalid/{id}', 'EmpOtherInfoController@updateID');
Route::get('personalid/{id}/show', 'EmpOtherInfoController@showID');
Route::delete('personalid/{id}', 'EmpOtherInfoController@removeID');
Route::get('personalid/{emp_id}', 'EmpOtherInfoController@getIDs');

Route::post('education', 'EmpOtherInfoController@saveEducation');
Route::put('education/{id}', 'EmpOtherInfoController@updateEducation');
Route::get('education/{id}/show', 'EmpOtherInfoController@showEducation');
Route::delete('education/{id}', 'EmpOtherInfoController@removeEducation');
Route::get('education/{emp_id}', 'EmpOtherInfoController@getEducations');

Route::post('emphistory', 'EmpOtherInfoController@saveEmpHistory');
Route::put('emphistory/{id}', 'EmpOtherInfoController@updateEmpHistory');
Route::get('emphistory/{id}/show', 'EmpOtherInfoController@showEmpHistory');
Route::delete('emphistory/{id}', 'EmpOtherInfoController@removeEmpHistory');
Route::get('emphistory/{emp_id}', 'EmpOtherInfoController@getEmpHistories');

// Setup Tables
Route::resource('amlatype', 'AMLATypeController');
Route::resource('region', 'RegionController');
Route::resource('postalcode', 'PostalCodeController');
Route::resource('crimetype', 'CrimeTypeController');
Route::resource('infosource', 'InfoSourceController');
Route::resource('infosourcetype', 'InfoSourceTypeController');
Route::resource('idtype', 'IDTypeController');
Route::resource('educlevel', 'EducationLevelController');
Route::get('organization', 'OrganizationController@list');

// ICBS API
// customers
Route::resource('customer', 'CustomerController');

// customer attachments
Route::get('customer/{cid}/attachmenttype/{category}', 'AttachmentTypeController@byCustomer');
Route::post('uploadFiles', 'AttachmentTypeController@upload');
Route::get('viewFiles/{id}/{file_cid}', 'AttachmentTypeController@view');
Route::delete('removeFiles/{id}', 'AttachmentTypeController@remove');
Route::get('downloadFile/{id}', 'AttachmentTypeController@download');
// Route::get('rptCustomerUploads', 'AttachmentTypeController@rptCustomerAttachments');
Route::get('rptCustomerUploads/{date_from}/{date_to}', 'AttachmentTypeController@rptCustomerAttachments');
Route::get('rptBranchUploads', 'AttachmentTypeController@rptBranchAttachments');
Route::get('customerphoto/{cid}/{mid}', 'AttachmentTypeController@customerPhoto'); // Customer Photo
Route::post('overrideAttachments', 'AttachmentTypeController@override');

Route::resource('attachmenttype', 'AttachmentTypeController');
Route::resource('attachmentcategory', 'AttachmentCategoryController');

